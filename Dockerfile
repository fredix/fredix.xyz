FROM alpine
# libc6-compat & libstdc++ are required for extended SASS libraries
# ca-certificates are required to fetch outside resources (like Twitter oEmbeds)
RUN apk update && \
    apk add --no-cache ca-certificates libc6-compat libstdc++ git

# Download and install hugo
ENV HUGO_VERSION 0.125.6
ENV HUGO_BINARY hugo_extended_${HUGO_VERSION}_Linux-64bit

ADD https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/${HUGO_BINARY}.tar.gz /usr/local/
RUN tar xzf /usr/local/${HUGO_BINARY}.tar.gz -C /tmp/ \
        && cp /tmp/hugo /usr/local/bin/hugo \
        && rm /usr/local/${HUGO_BINARY}.tar.gz

VOLUME /site
WORKDIR /site
COPY . /site

# Expose port for live server
EXPOSE 8080

ENV HUGO_BASE_URL https://fredix.xyz
CMD /usr/local/bin/hugo server -s /site --baseURL=${HUGO_BASE_URL} --port 8080 --appendPort=false --bind=0.0.0.0 -e production
