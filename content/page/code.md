+++
date = "2019-06-09T13:40:35+02:00"
draft = false
title = "code"
+++

## Dépôts git

* [Codeberg.org/fredix](https://codeberg.org/fredix/)
* [Framagit.org/fredix](https://framagit.org/fredix)
* [Radicle/fredix](https://app.radicle.xyz/nodes/seed.fredix.xyz)


Voici les quelques projets dont je suis l’auteur.

## Gobee

- status : en pause

[Gobee][17] est un logiciel qui permet de stocker ses médias et les consulter.

Environnement : Go, mongoDB, nomad, docker


## drone-gotify

- status : en sommeil

[drone-gotify](https://framagit.org/fredix/drone-gotify) est un plugin [Drone](https://drone.io/) pour [Gotify](https://gotify.net/).

Environnement : Go


## FRD

- status : en sommeil

[FRD][16] est un outil de suppression automatique de fichiers. A partir d'un fichier de configuration il surveille des répertoires et selon des règles établies par l'utilisateur (taille, date, extension, ...) supprime les fichiers.

Environnement : Golang

## Nodecast

- status : en sommeil

[Nodecast][4] est un projet de partage de fichiers en Qt/C++ qui utilise bittorrent et basé sur le code de [qBittorrent][5].

Environnements : Qt, C++, libVLC

## sensitbot

- status : abandonné

[sensitBot][1] est un bot Telegram qui permet de communiquer avec un appareil [sensit][2].  Le bot permet de recevoir les notifications envoyées par l’appareil via le réseau [sigfox][3]. Il permettra de modifier les paramètres de son appareil sans utiliser le site Internet de sensit, depuis Telegram.

Environnements : Golang, mongoDB

## graylog_http_push

- status : abandonné

[ graylog\_http\_push ][6] est un outil de transmission de logs vers un serveur [graylog][7]. Ce programme fonctionne sous forme de service essentiellement à destination de serveur Windows. Développé pour des besoins spécifiques, il est utilisé pour des logiciels qui écrivent leurs logs dans un nouveau fichier à chaque plantage ou exception. graylog\_http\_push surveille des répertoires, transmet chaque nouveau log et les archive.  
Dans la plupart des cas il est conseillé d’utiliser à la place [nxlog][8].

Environnements : Golang, Graylog

## NCS

- status : abandonné

[NCS][9] est un projet personnel opensource qui a pour objectif d’ordonnancer des jobs (workers) sur un ou plusieurs serveurs. NCS permet d’enchainer des tâches longues mais aussi de répartir les traitements vers un poll de workers. NCS expose une API HTTP qui permet de lui recevoir les données à traiter. La communication entre NCS et les workers utilise la librairie [zeromq][10].

- Utilisation de la base de données NoSQL, MongoDB. (administration, driver C++)
- Développement du backend en Qt C++ et zeroMQ
- Développement de l’API REST en Qt, Zeromq et ZeroGW
- Déploiement et exploitation en production pour la société Ubicmedia en 2012.

Environnements : QT, C++, MONGODB, ZEROMQ, ZEROGW, GIT, UBUNTU/DEBIAN LINUX

## geekast

- status : abandonné
- licence : GPL V3

[Geekast][11] est un client graphique à peercast.org en Ruby/GTK+

## iMotion

- statut : abandonné
- licence : GPL V3

[iMotion][13] est une interface graphique pour l’environnement de bureau GNOME qui utilise les effets visuels du logiciel [EffecTV][14].

Environnements : GTK+, GTKmm, C++, GStreamer

## resultset-autofilter

- statut : abandonné
- licence : MIT

[resultset-autofilter][15] est un ancien plugin pour Ruby on Rails qui permettait de générer la mise à jour en AJAX d’un tableau de données à partir d’un formulaire de filtre.

Environnements : Ruby, Rails

[1]:    https://framagit.org/fredix/sensitbot
[2]:    https://www.sensit.io "sensit"
[3]:    http://www.sigfox.com "sigfox"
[4]:    https://framagit.org/fredix/nodecast
[5]:    http://www.qbittorrent.org
[6]:    https://framagit.org/fredix/graylog_http_push "graylog_http_push"
[7]:    https://www.graylog.org "graylog"
[8]:    http://nxlog.org/ "nxlog"
[9]:    https://framagit.org/fredix/ncs
[10]:    http://zeromq.org
[11]:    https://framagit.org/fredix/geekast
[13]:    https://framagit.org/fredix/imotion
[14]:    https://www.effectv.dev
[15]:    http://code.google.com/p/resultset-autofilter/ "resultset-autofilter"
[16]:    https://framagit.org/fredix/frd
[17]:    https://codeberg.org/fredix/gobee
