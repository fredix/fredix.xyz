+++
date = "2016-01-03T12:24:12+01:00"
draft = false
title = "à propos"
slug = "about"
+++

Passionné par mon métier, je suis toujours à l’affut des nouvelles technologies de développement et administration système.  
Je n’hésite pas à m’auto-former dans le but d’étudier et mettre en place des architectures modernes et efficaces.

Dernièrement j’étudie et utilise le langage Go ([golang][1]) qui permet de mettre en oeuvre des services très performant, multi-plaformes (Linux, Windows, Mac) dans un binaire exécutable autonome. Cela peut aussi bien être un service Windows effectuant une tâche métier spécifique, qu’un site web sous Linux.

Je préfère de loin un environnement de travail avec un minimum de rigueur, de professionnalisme et de bonne humeur. Cela entend les matériels, les outils logiciels, les méthodes et documentations que la communication interne et l’ambiance.

[1]:	https://go.dev "golang"