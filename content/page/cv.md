+++
date = "2024-05-03T20:30:00+02:00"
draft = false
title = "CV"
+++

## Frédéric Logier
- Lyon
- fredix (chez) protonmail (point) com

Développeur système & backend. Administrateur système Linux.

### Stack technique

- Linux, Ubuntu, Debian, [Go][1], [Nomad](https://www.nomadproject.io), [Consul](https://www.consul.io), [Vault](https://www.vaultproject.io), [semaphore UI](https://www.semui.co), [Rundeck](https://www.rundeck.com), [docker](/tags/docker/), docker swarm, [drone.io](https://drone.io), ansible, [caddy](https://caddyserver.com), Bitwarden, [Minio](https://min.io), [restic](https://restic.net), Ruby, mongoDB, PostgreSQL, Graylog

### Expériences professionnelles

### depuis décembre 2023 : [Gama (Gaussin Macnica Mobility)](https://www.navya.tech)

Administrateur systèmes et réseaux Linux

- mise en place de l'orchestrateur de conteneurs [Nomad](https://www.nomadproject.io) ([Hashicorp](https://local.hashicorp.com/fr)), de
[Consul](https://www.consul.io) et [Vault](https://www.vaultproject.io) sur une infra hybride Azure - on premise
- migration d'applications web dans l'orchestrateur Nomad (HCL) et
protection des secrets dans Vault
- mise en place du reverse proxy [Caddy](https://caddyserver.com) dans l'infra Nomad
- migration de AWX vers [SemaphoreUI](https://www.semui.co)
- développement de scripts Ansible (backup, upgrade, ...) planifiés et exécutées par Semaphore
- mise en place de [Rundeck](https://www.rundeck.com) pour la gestion des mises à jours mensuelle
des serveurs Linux (planification et suivi)
- gestion de l'inventaire des postes de travail Linux / Windows avec [Kace](https://www.quest.com/fr-fr/kace/). Déploiement journalier ou à la demande de scripts Ansible via l'agent Kace
- maintenance de la construction d'une ISO Linux Ubuntu LTS 20 et 22 personnalisées pour des postes de travail : disque chiffré avec luks et login vers un ActiveDirectory
- déploiement de VM Linux sur Azure et Proxmox
- maintenance Gitlab

Environnements : nomad, consul, vault, docker, ansible, semaphoreUI, git,
gitlab, kace, prtg, Ubuntu serveur, Ubuntu desktop, phpipam, proxmox,
Azure, PFsense, Active Directory, CodeBeamer, Artifactory, Sonarqube

### juin 2019 - juin 2023 :  [BeeBryte](https://www.beebryte.com) 
Administrateur systèmes

- mise en place d'une solution de conteneurs avec docker et docker swarm.
- mise en place d'une solution de déploiement automatisée avec [Drone](https://drone.io/)
- migration d'un service en python vers [Golang](https://golang.org/)
- mise en place du serveur git [gitea](https://gitea.io)
- mise en place d’une solution de sauvegarde avec le serveur de stockage object [Minio](https://min.io) et l’outil de backup [restic](https://restic.net).

Environnements : git, gitea, ldap, golang, docker, docker swarm, drone, minio, restic


### avril 2019 - juin 2019 :  [WorldLine](https://fr.worldline.com) 
Responsable application 


### avril 2017 - mars 2019 :  [Sogelink](https://www.sogelink.fr) 
Ingénieur systèmes

- Développement de scripts ansible afin d’automatiser le déploiement d’application métiers;
- Installation/configuration de AWX (ansible tower) dans des conteneurs docker pour déclencher les déploiements via une interface web;
- Utilisation de puppet et git;
- Développeur du logiciel open source [frd](https://framagit.org/fredix/frd) un service en Go (golang) de suppression de fichiers;
- Suivi du monitoring (centreon), gestion des serveurs Linux (centOS) et vmware (vsphere).

Environnements :  centOS, vmware, puppet, ansible, git, Go, docker, redmine, postgresql, centreon, graylog, gitlab



### juin 2015 - 2017 : [Université de Lyon][2]
Administrateur systèmes Linux / Windows

- Administration d'une application web métier en ASP / SQL Server. Gestion du projet avec le prestataire en charge du développement. Suivi des bugs, des évolutions, mise en production.
- Installation configuration de l'outil de supervision [Zabbix][3]. Déploiement des agents, développement de scripts en Ruby.
- Installation de l'outil de centralisation et analyse de log [Graylog][4]. Cluster mongoDB (3 noeuds), Elasticsearch (2 noeuds), 2 serveurs Graylogs, 2 loadbalancer nginx (TCP/UDP) + keepalived
- Graylog : Mise en place d'alertes email en fonction de la sévérité des logs systèmes et applicatifs. Déploiement de l'agent [nxlog][5] sur les serveurs Windows et Linux.
- Mise en place d'un file système distribué [GlusterFS](https://www.gluster.org/) 
- Maintenance et mise à jour de VM Linux (debian,centos) avec [ansible](https://www.ansible.com/)
- Création de VM Linux/Windows, VMware vsphere
- gestion des tickets redmine, glpi
- développement d’un site web en Golang permettant de consulter les informations de personnels de l’UDL depuis un serveur Active Directory

Environnements : Windows 2008 R2, Ubuntu server, Debian, CentOS, Postgresql, SQL Server, Graylog, mongoDB, Elasticsearch, nxlog, nginx, keepalived, Redmine, VMWare vsphere, Docker, glusterFS, ansible, Golang, Ruby


### février 2014 : [Nodecast][6]

Développement d'un outil opensource de partage de fichier en P2P et multiplateforme. L’application est développée en Qt / C++. Utilisation de la bibliothèque [libtorrent](http://www.rasterbar.com/products/libtorrent/)  et de la bibliothèque [qxmpp](https://github.com/qxmpp-project/qxmpp/)  (XMPP). 

Environnements : C++, Qt, bittorrent, XMPP, Mac OSX, Linux


### décembre 2011 – 2014 : Responsable Exploitation et développeur système backend chez Ubicmedia

Administration système Linux, développeur système

Dans le cadre de l’exploitation et de l’évolution de l’infrastructure d'un produit de protection et vente de vidéos sur Internet.

- Développement du logiciel opensource [ncs][7] pour le pilotage et l’automatisation d’un backend asynchrone de traitement des fichiers vidéos.
- Étude et développement d’une architecture hautement scalable basée sur nodecast avec Amazon EC2
- Mise en œuvre et administration d’une architecture de serveurs virtualisés chez OVH avec [KVM][8] pour les environnements de dev et test
- Mise en place de l’outil de supervision Zabbix. Supervision système des plateformes hébergées et supervision applicative. Gestion des escalades d’alertes email, SMS, IM. Graphiques de statistiques
- VPN avec OpenVPN et Freelan
- Administration, configuration et exploitation en production des services Amazon : EC2, ELB, S3, ROUTE53

Environnements : C++, Qt, MONGODB, ZEROMQ, NCS, GIT, DEBIAN, UBUNTU SERVEUR, KVM, ZABBIX, OPENVPN, AWS

### 2010 – 2011 : Responsable Exploitation chez Nirva Software

Administration système Linux

Dans le cadre de l’évolution du produit Nirva en mode SaaS, Post Green

- Étude, mise en œuvre et administration d’une architecture répartie, chez OVH
- Mise en place de l’outil de supervision Zabbix. Supervision système des plateformes hébergées et supervision applicative. Gestion des escalades d’alertes email, SMS, IM. Graphiques de statistiques
- Virtualisation de serveurs Linux et Windows 2008 avec KVM
- VPN avec OpenVPN
- Développement et intégration d’une QA en Ruby / RSPEC, pilotée par Zabbix et Selenium RC
- Écriture et tests des workflow utilisateur
- Génération de statistiques du temps de réponse des scénarios utilisateur sur le site web de production
- Développement en Java et Playframework

Environnements : UBUNTU SERVEUR, KVM, ZABBIX, REDMINE, MYSQL, WINDOWS SERVER 2008 R2, OPENVPN, SELENIUM RC, RUBY, JAVA

### 2008 – 2009 : Chef de projet intégrateur à la SNCF via AXIALOG
Ingénieur intégration et support niveau 3 en environnement Red Hat Enterprise Linux à la SNCF (DSIT-XIA).

- Coordination entre la MOA MOE et la production
- Gestion d’incidents, support de niveau 3
- Développement de scripts KSH
- Gestion de scripts avec subversion
- Réalisation de documentation d’exploitation
- Gestion de planning
- Mise en place d’un gestionnaire de tickets via une interface web pour faciliter les échanges (Redmine)
- développement de schémas avec l’ordonnanceur XOS (Synchrony Automator)
- Formation de base et formation avancée sur Synchrony Automator (5 jours chez Axway)

Environnements : KSH, XOS, RED HAT LINUX, REDMINE, SUBVERSION

### 2007 – 2008 : lead developer chez AF83
Développement en Ruby on Rails sur le site web communautaire Noumba

- recherche et développement
- implémentation d’un mécanisme de distribution de tâches asynchrones en Ruby (beanstalkd et XMPP)
- réception et envoi de messages SMS et Jabber
- serveur de push d’envoi des nouveaux messages aux navigateurs via une socket flash (juggernaut) Poste occupé en télétravail (IRC, Trac, subversion, …)

Environnements : RUBY, RUBY ON RAILS, JAVASCRIPT, XMPP, MYSQL, SUBVERSION

### 2006 – 2007 : Analyste Programmeur chez Cartaix group (Uniteam)

- Développement d’un FrontOffice et d’un BackOffice en Ruby on Rails : Avantages presse
- Migration d’un BackOffice métier PHP en Ruby on Rails : Viseopharma:

	- Utilisation du framework AJAX scriptaculous
	- Installation et utilisation du gestionnaire de code source subversion
	- Migration d’un BackOffice PHP en Ruby on Rails
	- Mise en place d’outils de développement en groupe :
		- Ubuntu Serveur
		- Subversion
		- Redmine
		- PostgreSQL
	- Mise à niveau de l’architecture des serveurs applicatifs (Debian / Ubuntu) :
		- sécurité (netfilter)
		- monitoring (munin)
		- VPN (openvpn)
		\- 
Environnements : RUBY, RUBY ON RAILS, MYSQL, JAVASCRIPT, SUBVERSION, MUNIN

### 2003 – novembre 2005 : Associé de la Société de Services en Logiciels Libres Taonix

- Développement PHP/MySQL
- Préparation à la création de la Société de Services en Logiciels Libres, Taonix
- Formation à la création d’entreprise (AFPA)

Environnements : PHP, MYSQL, APACHE, DEBIAN LINUX

### 2001 – 2003 : Développeur et Administrateur système Linux chez Az Informatique
développeur web et base de données

- développement de sites web en php avec PostgreSQL et MySQL
- utilisation du moteur de templates Smarty
- administrateur système et réseau GNU/Linux (Debian)
- refonte du réseau interne de l’entreprise, création d’un Intranet, d’une DMZ et d’un firewall
- solution de backup sécurisé via Internet (rsync over SSL)
- installation et administration de réseaux VPN (FreeS/Wan)
- administration mails (postfix/amavis/sophos), serveur d’impression (lpr, rlp), serveur de fichiers (samba), apache, bind
- gestion en réseau d’un onduleur APC via NUT

Environnements : PHP, POSTGRESQL, DEBIAN LINUX, FIREWALL, DNS, POSTFIX, TCP/IP

### 2000 – 2001 : Analyste Programmeur chez Accelance
Création du premier fournisseur d’accès Internet gratuit, OREKA :

- développement en PHP4 et Sql avec Postgresql 7.0.3, Oracle 8i et MySQL.
- création d’extensions spécifiques PHP en C * CGI en Pro*C, création et appel de procédures stockées ORACLE
- scripts Perl
- développement de modules Apache avec l’API en C
- authentification sur base Postgresql
- modification du module PUT d’APACHE
- redirection des requêtes HTTP contenant les informations du navigateur client vers une librairie C++
- Administration d’un serveur de développement Linux sous Debian 2.2 :
	- serveur de gestion de source CVS
	- serveur web Apache / PHP
	- sgbd Postgresql 7.0.3

Environnements : C, PHP, APACHE, ORACLE, MYSQL

### 1998 – 2000 : Analyste Programmeur à la SSII Transiciel
- mission chez ABEL Guillemot (jan. 1998 – 31 juil. 1998, à Bron) : · développement avec Centura en POO * mission chez Socara (centrale d’achat Leclerc) : · développement pour un backoffice de modules en Pro*C sur ORACLE 7.
- mission au CIRRA : · programmes en Pro*Cobol sur ORACLE 7. * mission Mutuel : · développement sur un projet Client-Serveur en Visual Basic 5 et Oracle 7.
- inter contrat Transiciel : · formation interne au PL/SQL.

Environnements : CENTURA, C, PL/SQL

### Formations
15 mai 2004 – 31 juillet 2004

- Gestion pour Repreneur d’Entreprise et Créateur (AFPA Lyon)

novembre 1994 – novembre 1995

- Analyste programmeur niveau III, BTS (AFPA Lyon)

septembre 1993 – juillet 1994

- Technicien en informatique de gestion niveau IV, BAC (AFPA Marseille)

### Compétences
systèmes d’exploitation Linux : Debian, Ubuntu, Fedora, Manjaro

- Installation
- Configuration
- Administration
- Développement

### Développement

- Go
- C++, Qt, C
- Ruby
- SQL
- shell

### Administration

- Ubuntu / Debian
- Apache
- Bind
- Graylog
- Git
- iptable
- mongoDB
- Nginx
- OpenVPN
- PostgreSQL



[1]:	https://go.dev "golang"
[2]:	http://www.universite-lyon.fr
[3]:	http://www.zabbix.com
[4]:	https://www.graylog.org
[5]:	http://nxlog.org
[6]:	https://github.com/nodecast/nodecast
[7]:	https://github.com/fredix/ncs "ncs"
[8]:	http://www.linux-kvm.org/page/Main_Page "KVM"
