+++
date = "2012-12-14 14:40:07+00:00"
draft = false
slug = "le-monde-selon-le-libre"
title = "le monde selon le Libre"
+++

Ce texte tente d'être une réponse à [le monde selon Google][1] de [ploum][2].

En 2020 l’hégémonie du Triumvirat Google-Apple-Amazon est à son comble. Pour les consommateurs il ne s'agit plus de choisir une télévision, une voiture, un ordinateur, un smartphone ou une tablette, il s'agit de choisir un écosystème dans lequel son expérience sera comblée.

A cette époque le logiciel libre sur les terminaux utilisateurs est réduit à une peau de chagrin, celle de certains informaticiens et libertaires qui préfèrent sacrifier les facilités que procurent les écosystèmes fermés sur l'autel de leur indépendance.. Après des années de dénis, de dénigrement et de mauvaise fois, une frange de cette communauté décida de développer les fondations amenant à la naissance d'un écosystème libre respectant l’étique du Libre et l'architecture décentralisée d'Internet.

Le projet Avalon fut créé, non sans difficultés et heurts. Il avait pour principe de fournir des outils et protocoles permettant à n'importe qui de développer un logiciel sans se préoccuper de la confidentialité, de la scalabilité, du stockage, du partage des données et de leur redondance. Ce fut une avancée majeure pour le libre, ainsi il pouvait proposer des fonctionnalités présentes depuis des décennies dans les écosystèmes propriétaires.

Avalon fut rapidement intégré dans les distributions Linux, lequelles offrirent également un service en ligne qui permettait d'associer le compte de l'utilisateur Linux à leur propre instance. De plus en plus de logiciels natifs intégrèrent l'API d'Avalon ce qui permit de sauvegarder les préférences de configuration dans Avalon mais aussi les données de ces logiciels. Cela commença par de simple éditeur de texte via des plugins, puis lorsque les retours utilisateurs enthousiastes fusaient de toute part, des poids lourds comme OpenOffice intégrèrent l'API nativement ainsi que des logiciels de chat, musique, mail, et même des sites web.

Les utilisateurs découvrirent les avantages de la fusion du natifs et d'Internet, puisque leurs documents étaient sauvegardés sur plusieurs noeuds, mais l'API permettait aussi l'édition à plusieurs. Les avantages du web sans les limitations de ses interfaces de programmation.

Avalon était tellement optimisé que les utilisateurs avancés pouvaient l'installer dans un device léger au fond d'un placard. Avalon intégrait nativement une réplication en P2P avec ses contacts. Les données étaient chiffrées et répliqués, une simple commande permettaient de les récupérer en cas de crash de son instance locale. Avalon fut tellement adopté que toutes les distributions l'avaient intégré au point qu'un utilisateur pouvait installer à nouveau une machine, l'associer à son compte Avalon puis en quelques minutes retrouver les logiciels déjà installé par ailleurs avec leurs données de configuration et les données utilisateurs correspondantes pour chacun d'eux.

Avalon atteignit son apogée dans le Libre, lorsque [cyanogenmod][3] intégra les API d'Avalon par défaut. Les tablettes, smartphone et télévisions sous Android étaient synchronisable avec ses desktops Linux. De nombreuses applications Android utilisèrent son API plutôt que celle de Google. [HaikuOS][4] fit de même ainsi que[ Jolla Mobile][5].

Avalon devint un standard du Libre et de tous les écosystèmes alternatifs. L'intégration avec l'OS était telle que les services de réseaux sociaux historique n'avaient plus d'intérêt, en effet on avait son réseau social intégré dans toutes les applications de son système. Il était devenu complètement désué de devoir aller sur un site web dédié ou de lancer une application dédié pour communiquer avec son réseau.

Les développeurs d'Amazon, Mac, et même du vieux système Windows encore sur le marché malgré sa chute libre depuis 2017,  souhaitèrent à ce moment là utiliser l'API d'Avalon plutôt que celles de leurs OS éponyme. Cela leur économisait des coûts de licence vers les clouds et surtout la demande de leurs utilisateurs étaient de plus en plus tenace. A ce moment là les développeur d'Avalon ajoutèrent le service [Bitcoin][6] nativement. Cette monnaie était de plus en plus utilisée et la demande était forte pour vendre et acheter du contenu numérique à travers Avalon.

Le Triumvirat ne supporta pas l'arrivée sur leur plate-bande d'Avalon, qui permettait aux développeurs et surtout à leurs consommateurs d'échapper à leurs contrôles et à leurs taxes. Bitcoin fut la goutte de trop et ils firent pression auprès des gouvernements qui n'attendaient que ça pour rendre illégal Avalon.

Malgré une mobilisation mondiale et une résistance farouche d'hacktivistes, le point d'Achille fut rapidement trouvé en stoppant les DNS d'Avalon.

Aujourd'hui, 2022. Internet est splitté. Avalon a comblé sa dernière faille, son dernier point central, en basculant sur [Namecoin][7], un DNS en P2P basé sur Bitcoin. La bataille est loin d'être terminée, mais Avalon attirant de plus en plus d'utilisateurs et une économie florissante il obtient chaque jour plus de soutiens. Les FAIs ont de plus en plus de pressions via des lois pour scruter ce réseau parallèle et le bloquer. En contrepartie des FAIs locaux alternatifs poussent partout, ils ne supportent qu'Avalon et son DNS en P2P.

La bataille est loin d'être terminée, mais l'économie numérique basée sur Avalon étant de plus en plus florissante, il devient difficile pour les gouvernements de justifier son éradication. Hier Google a du faire un premier pas en se rendant compatible avec Avalon, ils ne pouvaient continuer à rester isolé d'un nombre de plus en plus grand d'utilisateurs. Google n'est plus qu'un noeud, mais il est certainement trop tard pour eux, sur Avalon tout le monde utilise [seeks][8]...

[1]:	http://ploum.net/post/le-monde-selon-Google
[2]:	http://ploum.net/
[3]:	http://www.cyanogenmod.org/
[4]:	https://www.haiku-os.org/
[5]:	http://jolla.com/
[6]:	http://bitcoin.org/
[7]:	http://dot-bit.org/Main_Page
[8]:	http://www.seeks-project.info "seeks"