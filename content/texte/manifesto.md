+++
date = "2014-10-09"
draft = false
title = "manifesto"
+++


Quel est le bien le plus précieux de l’Homme ? Sans nul doute la Connaissance. Sans celle-ci l’Homme ne peut survivre à son environnement et s’adapter. Or le savoir devient numérique. Qui détient le contrôle de l’Information numérique détient le pouvoir.

Petit à petit les visions d’anticipation de [William Gibson][2] deviennent réalité. Dans cet avenir inquiétant ne survivront que ceux qui voient à travers le miroir.

Ces dernières années voient la naissance du contrôle de la Culture et du Savoir de l’Humanité. La Bête prend forme. L’Humanité a oublié qu’elle a pu atteindre ce stade de l’évolution grâce à l’échange et au partage.

Pour protéger les intérêts de quelques uns, elle a décidé de se brider. Cependant ceci n’étant pas naturel, elle a engendré également, avec les mêmes outils, une culture du partage.  
Il ne pouvait en être autrement. Tout dans le monde a besoin d’un équilibre, et si celui-ci vient à être perturbé, il sera tôt ou tard rétablit.

Les années qui viennent vont voir la lutte constante entre ces deux antinomies.

Bienvenue dans ce monde numérique, et, choisissez votre camp …

FL 17/03/2006

[1]:	/texte/manifesteduhacker/
[2]:	http://www.williamgibsonbooks.com/ "William Gibson"
