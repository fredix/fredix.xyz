+++
date = "2016-02-26T21:15:20+01:00"
draft = false
title = "alternatives aux applications web"
tags            = ["mac", "desktop"]
+++

Pour mes besoins professionnels et personnels j’ai la nécessité d’utiliser deux types d’outils, un gestionnaire de tâche et un outil de prise de notes.
Autant le premier ne nécessite pas d’explication (gestion des courses, gestion de projets/tâches, …) , le deuxième un peu plus.

Lorsqu’on travaille depuis de nombreuses années dans des domaines aussi variés et complexes que l’administration système ou le développement, on acquiert une immense collection de connaissances sur des détails de configuration de tel service que l’on aborde parfois une seule fois (commandes systèmes, web, email, dns, ..) ou de tel langage. De même à l’échelle d’une entreprise, celle-ci possède également un grand nombre de savoirs spécifiques à son métier, ses données et méthodes.

Ces connaissances, quelles soient à l’échelle d’un individu ou d’une entreprise, obligent à les péréniser en les partageant et en les mettant à jour. Ceci est la base pour toute innovation, maintient de la motivation et de la productivité. A défaut, chacun devra rechercher la même information mainte et mainte fois dans son coin sans profiter de l’expérience acquise par les autres. Ces évidences, qui sont loin de l’être sur le terrain, sont à la base de la [gestion des connaissances][1] (ou Knowledge Management).

Pour ma part je plutôt fan de [Redmine][2], un outil de gestion de projets qui permet de gérer des tâches, mais aussi intègre un wiki, une roadmap, etc. Il répond à tous les besoins précités, mais pour sa propre gestion personnelle il est un peu « overkill » il faut bien avouer.  
De part sa nature web, il oblige à administrer un serveur Linux, une base de données, un nom de domaine, à faire les mises à jour et les sauvegardes. J’en étais donc venue à titre personnel à utiliser simplement des fichiers dans un répertoire Dropbox pour stocker mes connaissances et pour les retrouver à lancer un grep…

J’ai de très loin une préférence pour les applications desktop, l’une d’elle convient très bien pour cette gestion du savoir, [zim wiki][3]. Elle a juste le mauvais goût d’utiliser un grand nombre de bibliothèques (python, gtk, ..) qui complexifie horriblement son installation sur un Mac. Dans le monde propriétaire le leader est sans aucun doute [Evernote][4] qui propose une assez bonne interface desktop, mais le rend inéligible pour noter du code source car ne gère pas la coloration syntaxique.

Puis on m’a fait découvrir [Quiver][5] et j’ai enfin trouvé le Graal. C’est un outil qui permet de regrouper des notes dans des notebook. Chaque note peut être constituée de cellules de type texte, markdown, code, latex et diagram ( [sequence][6] / [flowchart][7] ). Donc que du texte brut qui sera mis en forme par Quiver.  
La coloration syntaxique gère un [très grand nombre de langage][8], exporte en PDF/HTML/Text, possède un mode présentation plein écran, stocke les notebooks dans un format ouvert en json ([Quiver Data Format][9]), permet de partager un notebook vers un répertoire dédié (dropbox, iCloud, google drive, ..) en lecture ou écriture partagé, et propose une interface simple légère avec des thèmes. Bien entendu un moteur de recherche et une gestion des notes par tags.  
Sa base de données peut être stockées dans un répertoire partagé afin de la synchroniser entre ses Mac ou ses collègues.

![][image-1]
  
[Un exemple de note][10] exportée en PDF.

Bref, de quoi donner envie de maintenir et enrichir sa base de connaissance. Mon seul regret est qu’il n’y a pas encore d’outils windows/linux sachant lire son format.  
J’utilise pour l’instant la version gratuite qui n’impose qu’une bannière free trial discrète, mais je compte bien m’acquitter des 10$ demandés sur l’app store pour soutenir ce développeur talentueux.

Pour la gestion de tâches le choix est beaucoup plus pléthorique. Pour être efficace il faut un outil desktop et smartphone qui se synchronise, propose des alertes, des récurrences de tâches, etc. [Todoist][11] fait partie des meilleurs pour cela. A noter sa version premium à 29$ / an qui peut sembler excessive mais qui propose d’attacher des fichiers à des tâches, ajouter des notes et des rappels (même selon sa géolocalisation). La version gratuite est cependant suffisament complète pour un usage basique.

Voici donc deux excellentes applications bien meilleures que celles uniquement en web et qui peuvent faire partie de la boite à outils indispensable à tout administrateur/développeur.

ps : article écrit avec [Ulysses][12], synchronisé avec [syncthing][13] et publié avec [Hugo][14].

[1]:	https://fr.wikipedia.org/wiki/Gestion_des_connaissances "Gestion_des_connaissances"
[2]:	http://www.redmine.org "redmine"
[3]:	http://zim-wiki.org/ "zim wiki"
[4]:	https://evernote.com "evernote"
[5]:	http://happenapps.com/#quiver "quiver"
[6]:	http://bramp.github.io/js-sequence-diagrams/ "sequence"
[7]:	http://flowchart.js.org/ "flowchart"
[8]:	https://github.com/HappenApps/Quiver/wiki/Syntax-Highlighting-Supported-Languages "Syntax-Highlighting-Supported-Languages"
[9]:	https://github.com/HappenApps/Quiver/wiki/Quiver-Data-Format "Quiver-Data-Format"
[10]:	/pdf/quiver_cell_types.pdf "quiver_cell_types.pdf"
[11]:	https://todoist.com/ "todoist"
[12]:	http://www.ulyssesapp.com "ulysses"
[13]:	https://syncthing.net "syncthing"
[14]:	https://gohugo.io/ "hugo"

[image-1]:	/images/quiver_sync.png "quiver_sync"