---
title: "Gobee BitTorrent"
date: 2023-05-28T19:33:00+02:00
draft: false
tags: ["gobee"]
---

Voici du nouveau depuis le billet sur [Gobee](/2021/11/gobee/). J'ai récemment migré le projet sur [codeberg](https://codeberg.org/fredix/gobee) qui utilise [gitea](https://gitea.io), la raison est que je suis plus habitué à gitea qu'à gitlab utilisé par [framagit](https://framagit.org).

En fonctionnalité on peut enfin avancer et reculer dans le lecteur vidéo (mp4 ou mov), pour cela j'ai du intégrer un [serveur de fichier http ](https://codeberg.org/fredix/gobee/src/branch/master/gobee.go#L215) qui fonctionne dans une go routine. Cela permet au lecteur de faire des requêtes HTTP sur des ranges pour obtenir les morceaux de vidéos. Un grand merci à Radhian Amri pour cet article, [Video Streaming using HTTP 206 Partial Content in Go](https://medium.com/@radhian.amri/video-streaming-using-http-206-partial-content-in-go-4e89d96abdd0) qui m'a permis cela.

Ajout également de [Redis](https://redis.io), l'objectif est de poser un lock lorsqu'on extrait une vidéo de mongoDB gridFS. En effet cela prend un certain temps or si un autre utilisateur demande en même temps à lire la même vidéo cela aurait pu écraser le fichier en cours d'extraction.

La dernière grosse fonctionnalité est l'ajout de [BitTorrent](https://fr.wikipedia.org/wiki/BitTorrent). Pour chaque fichier dans gobee un bouton Torrent est disponible (bien entendu surtout utile pour les gros fichiers) et va générer un fichier Torrent qu'il faudra ouvrir avec un client comme [transmission](https://transmissionbt.com). Le torrent va utiliser 2 trackers (serveur qui met en relation les clients torrent), celui de https://privtracker.com et celui embarqué dans gobee dont le code est celui de [privtracker](https://github.com/meehow/privtracker). Ce tracker embarqué tourne dans une go routine et pour l'utiliser il est nécessaire de configurer sa box Internet pour forwarder le port 6969 vers l'IP du PC qui fait tourner gobee. Ceci n'étant pas toujours possible ou trop compliqué, le tracker de privtracker fera le job.

L'usage est de partager les torrents avec vos proches de manière privée comme l'explique le site de [privtracker](https://privtracker.com/).

Pour finir j'ai écris un [docker-compose.yml ](https://codeberg.org/fredix/gobee/src/branch/master/docker-compose.yml) qui permet avec une commande docker d'installer et de lancer gobee:

```sh
# installer docker, docker-compose et git

git clone https://codeberg.org/fredix/gobee.git
cd gobee
docker compose up -d
```

cliquez sur  [http://localhost:8080](http://localhost:8080) le login/pass est gobee.
