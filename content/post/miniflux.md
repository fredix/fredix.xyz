---
title: "Miniflux"
date: 2024-03-13T18:05:35+01:00
draft: false
tags: ["linux","rss","miniflux"]
---

J'avais parlé de [Miniflux](https://miniflux.app) en 2019, [Miniflux un serveur RSS](/2019/06/miniflux-un-serveur-rss/), alors quoi de neuf en 2024 ?

Tout d'abord fini l'autohébergement pour ce service. En effet mon infra est ras-la-gueule et même si miniflux est lui même léger (en Golang) il nécessite une base de données postgresql conséquente. A l'époque j'avais une base de plusieurs giga pour stocker les flux RSS ; mais peut être que l'application était mal configurée.

Mais ce que je souhaite maintenant c'est utiliser et payer la version en cloud. On peut [s'inscrire ici](https://miniflux.app/hosting.html) et tester gratuitement 15 jours. Le service coûte 15$/an ce qui est largement raisonnable d'autant plus que cela finance un logiciel libre et me libère du temps d'administration 🤝.  
Mais cela ne me suffirait pas s'il n'y avait pas un bon client de bureau natif ainsi qu'un client mobile. Or c'est le cas j'ai testé avec succès l'excellent [NewsFlash](https://apps.gnome.org/fr/NewsFlash/) (codé en Rust/GTK4) et il se connecte à divers fournisseurs de flux RSS dont miniflux.

![newflash_compte](/images/newsflash_compte.png)

NewsFlash est disponible sur la majorité des distributions Linux, bien sûr Manjaro/Arch via un `sudo pacman -S newsflash` mais aussi part [snap](https://snapcraft.io/newsflash) pour les Ubuntu, et [flatpak](https://flathub.org/apps/io.gitlab.news_flash.NewsFlash).

![newsflash](/images/newsflash.png)


Pour connecter Newsflash il faut d'abord aller sur [https://reader.miniflux.app](https://reader.miniflux.app) créer une clé API pour obtenir un jeton.

![miniflux token](/images/miniflux_token.png)

Ensuite dans NewFlash lors de l'ajout d'un compte il faudra indiquer une URL de connexion, **attention** pas celle du point de terminaison de l'API qui fini par /v1/, elle ne fonctionne pas, mais juste `https://reader.miniflux.app`, puis fournir la token (jeton) et voilà.  
Pour finir il faut alimenter en liens RSS le service. Personnellement j'avais un compte sur Feedly, j'ai pu faire un export de mes flux dans un fichier OPML ([export OPML](https://feedly.com/i/opml)) et l'importer dans l'interface web miniflux.

Côté mobile, il y a une liste ici [d'applications tierces](https://miniflux.app/docs/apps.html), j'utilise [miniflutt](https://play.google.com/store/apps/details?id=be.martinelli.miniflutt) qui fonctionne bien (avec la même configuration que newsflash pour la connexion). Évidement j'ai testé la synchronisation entre le bureau et le mobile et en effet un article lu sur newsflash et bien marqué lu sur mobile, de même pour l'ajout/suppression de flux.

Alors pourquoi encore le RSS en 2024 ?  
Pour choisir ses sources d'informations. Parce que news.google.com (Google Actualité sur mobile) n'est pas forcément judicieux si l'on souhaite se détourner des infos toxiques et si l'on ne veut pas être profilé. Parce que les réseaux sociaux ne sont pas pertinant pour s'informer mais plutôt un complément (évidemment je ne parle pas de MuskX, metaFace, TikTruk, 💩).  
Parce que je maitrise l'information que j'avale au même titre que la nourriture. Le RSS c'est bon mangez-en ! 🍲

(Ce texte a été écrit avec [Ghostwriter](https://ghostwriter.kde.org/fr/))