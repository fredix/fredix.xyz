---
title: "Manjaro"
date: 2022-08-07T13:23:12+02:00
draft: false
---

Comme l'indique le logo à gauche je suis passé sur la distribution [Manjaro](https://manjaro.org) sur mon laptop perso. Pour changer d'Ubuntu je souhaitais une distrib en rolling release et une solution basée sur [Arch](https://archlinux.org) me semble idéal. J'ai testé cette dernière et d'ailleurs grâce au script archinstall fourni de base dans l'iso, l'installation est automatisée.  
Cependant lorsqu'on installe GNOME, c'est la version brut de celui-ci. Au contraire, l'iso GNOME de Manjaro préconfigure GNOME avec des extensions qui vont bien pour avoir une interface léchée. De même le shell zsh est par défaut avec un thème Manjaro. Enfin Manjaro fourni un outil graphique (ou cli) de gestion du kernel, on peut ainsi choisir plusieurs versions du kernel pour pouvoir booter sur celui de son choix.  

![parametres](/images/parametres_manjaro.png)

![kernel](/images/kernel.png)

Ici j'ai installé en plus la version 5.10.133 pour corriger un freeze en sortie de veille de la 5.15.57. En espérant que la prochaine 5.19 ne freeze pas non plus.  
En installant le programme yay (sudo pacman -S yay) on accède au dépot AUR ( https://aur.archlinux.org ) et donc à des milliers de packages supplémentaires.

liens:

* https://linuxfr.org/news/bien-debuter-avec-manjaro-linux
* https://www.nextinpact.com/article/47796/linux-manjaro-reine-distributions-rolling-release-grand-public
* https://websetnet.net/fr/6-essential-things-to-do-after-installing-manjaro-linux