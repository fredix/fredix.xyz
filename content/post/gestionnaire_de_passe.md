---
title: "Gestionnaires de mots de passe"
date: 2022-09-27T19:46:17+02:00
draft: false
---

* [update 29.09.2022]: ajout de [pass](https://www.passwordstore.org)
* [update 03.24] corrections diverses

Ça fait un moment que je voulais écrire un billet sur les gestionnaire de mots de passe. Qui de nos jours en 2022 n'utilise toujours pas un tel outil ? Cela me parait une abération, avec l'augmentation des services numériques et des piratages il n'est pas possible de se souvenir de tous ses mots de passe, à moins d'utiliser toujours le même partout ......  
Bref soyons sérieux, l'utilisation d'un gestionnaire de mots de passe est maintenant indispensable que cela soit à titre personnel ou professionnel. Et si vous en doutez posez-vous une seule question :  
**Quelle valeur donnez-vous à vos mots de passe ?** 🤔

Il y a quelques années j'utilisais [keepassxc](https://keepassxc.org) sur le desktop et [keepassdx](https://www.keepassdx.com) sur le mobile le tout synchronisé par [syncthing](https://syncthing.net). Mais c'était une vrai usine à gaz à maintenir et j'ai eu des conflits de fichier.  
Le cloud (sécurisé) a ses avantages, voici donc 6 gestionnaires de mots de passe qui correspondent à mes critères :

* proposer une application mobile
* proposer une application desktop graphique Linux et pas uniquement une extension navigateur web

## 1password

ici on est dans le lourd, [1password](https://1password.com/fr/downloads/linux/) est un des leaders des gestionnaires de mots de passe en tout cas dans l'écosystème Apple. Il s'ouvre cependant à Linux car il y propose depuis quelques temps une interface graphique. ~~C'est d'ailleurs un des seuls à offrir une application native (GTK3) pas en electron~~  
**[update]** Hé bien c'est un joli fail puisque que 1password Linux est bien en electron .. un simple `ls -alh /opt/1Password/` montre bien les fichiers classique d'une application [Electron](https://www.electronjs.org) (chrome_100_percent.pak, etc) 🤡.  
Il faut avouer que l'application est parfaitement bien conçue et fait illusion.


## Enpass

[Enpass](https://www.enpass.io) propose une interface graphique native (~~GTK3~~ Qt). Par contre il n'y a pas d'offre en cloud, il faudra se reposer sur un [drive](https://support.enpass.io/app/sync/sync_and_access_enpass_data_on_all_devices.htm) ou sur une synchro en local par wifi entre l'application bureau et mobile. Plusieurs offres avec abonnement.


## Bitwarden

[bitwarden](https://bitwarden.com) est l'application que j'utilise actuellement. L'application en electron est fournie dans un fichier AppImage. C'est une des offres la moins chère. Gratuite ou 10$/an pour avoir le 2FA. 

## pCloud Pass

Sorti il y a quelques jours, [pcloud pass](https://www.pcloud.com/fr/pass/download) est également en electron et AppImage. Gratuit avec 1 seul device, payant pour plus.

## Nordpass

je n'ai pas pu tester [nordpass](https://nordpass.com/fr/download/linux/) sur Manjaro. Application desktop en electron via un snap.

## Pass

On m'a suggéré [pass](https://www.passwordstore.org) qui stocke les passes dans un dépôt git. Utile pour ceux qui souhaitent s'autohéberger. Il existe une interface desktop en Qt, [QtPass](https://qtpass.org) et des applications [Android](https://github.com/zeapo/Android-Password-Store#readme) et [iOS](https://mssun.github.io/passforios/).


