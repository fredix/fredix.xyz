+++
date = "2008-10-07 13:02:00+00:00"
draft = false
slug = "the-man-from-earth"
title = "The Man from Earth"
tags            = ["cinema"]
+++

  

Je viens de voir un [extraordinaire film][1] difficile à cataloguer. [IMDb][2] le classe comme science-fiction / dramatique, mais je le mettrais plutôt dans la catégorie philosophie en compagnie d’autres trop rares films, tel que le Cercle des poètes disparus ou Adam’s Apples. Ce conte se passe en vase clos dans une petite maison, ce qui met en valeur le premier rôle du cinéma trop souvent oublié : le scénario. Pour résumer sans rien déflorer, il parle d’un homme très particulier qui raconte son histoire à ses amis. Je n’en dirais pas plus car je conseille vraiment de le voir.


L’histoire de ce film méconnu aurait pu s’arrêter là mais sa page [WikiPedia][3] nous en apprend plus. Ayant été tourné avec très peu de moyen et diffusé uniquement aux USA en DVD, il a connu une plus large diffusion et un grand succès grâce à son “piratage” et son sous-titrage sur des sites de Torrent. Mais ce n’est pas tout. Le réalisateur et le production ont remercié les sites “illégaux” et encouragent les Internautes à faire un [don][4], chose que je me suis empressé de faire.


Je leur ai malgré tout conseillé de passer leur film sous licence Creative Commons, si tant est que cela soit possible, ce qui éviterait de transformer leurs généreux donateurs et diffuseurs en dangereux criminels terroristes spolieurs de multinationales.

[1]:	http://manfromearth.com/
[2]:	http://www.imdb.com/title/tt0756683/
[3]:	http://fr.wikipedia.org/wiki/The_Man_from_Earth
[4]:	http://www.rlslog.net/piracy-isnt-that-bad-and-they-know-it/comment-page-2/#comment-150581 "piracy-isnt-that-bad-and-they-know-it"