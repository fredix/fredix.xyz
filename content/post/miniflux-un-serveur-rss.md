---
tags: ["miniflux","docker","arm","postgresql"]
categories: ["devops"]
title: "Miniflux un serveur RSS"
slug: "miniflux-un-serveur-rss"
date: 2019-06-05T19:49:36+02:00
draft: false
---

Suite à [fathom](/2019/06/stats-web-avec-fathom/), un autre un autre service me manquait, un serveur de RSS proposant une API pour un client mobile. En opensource il en existe plusieurs comme [tt-rss](https://tt-rss.org/) ou [freshrss](https://www.freshrss.org/) mais comme toujours l'usinerie PHP/SGBD me rebute. C'est alors (encore :) que j'ai découvert [miniflux](https://miniflux.app/) un serveur en Go (anciennement en PHP). Malheureusement il demande comme les autres un SGBD ; comme je ne peux y échapper voici mes différentes étapes d'installation avec PostgreSQL sur ma stack docker swarm / [glusterfs](/2019/06/glusterfs-docker/).

## postgresql

Miniflux propose dans sa documentation un [docker compose clé en main](https://miniflux.app/docs/installation.html#docker), de plus nul besoin de builder une image pour ARM64 puisque le développeur en propose déjà une : [arm64v8-latest](https://hub.docker.com/r/miniflux/miniflux/tags). Le problème est que le conteneur postgresql est défini dans le docker compose de miniflux, il sera donc spécifique à lui. Je préfère lancer un unique conteneur postgresql qui pourra être utilisé par d'autres applications.

Créons le répertoire de travail de postgresql dans glusterfs

```sh
mkdir -p /swarm/volumes/postgresql/data/
```

puis un réseau docker lui est dédié

```sh
docker network create -d overlay postgresql
```

ensuite on créé un [secret docker](https://blog.adminrezo.fr/2019/05/gestion-des-secrets-sous-docker/) qui servira à initialiser le password postgres, cela évitera de publier le mot de passe dans le docker compose.

```sh
echo "PASS" |docker secret create postgres_db_password -
```

le mot de pass est ainsi accessible depuis tout le swarm

```sh
docker secret ls
ID                          NAME                   DRIVER              CREATED             UPDATED
f2cdvbje7mxw289js4eigyn58   postgres_db_password                       28 hours ago        28 hours ago
```

le mot de passe sera monté dans le conteneur dans le fichier `/run/secrets/postgres_db_password`  
Le docker compose

cat postgresql-arm64v8.yml 
```yaml
version: "3.7"
services:
  postgres:
    image: arm64v8/postgres:alpine
    volumes:
      - /swarm/volumes/postgresql/data:/var/lib/postgresql/data
    environment: 
      - POSTGRES_PASSWORD_FILE=/run/secrets/postgres_db_password
    networks:
      - postgresql
    ports:
      - "5432:5432"
    secrets:
      - postgres_db_password

networks:
  postgresql:
    external: true

secrets:
  postgres_db_password:
    external: true
```

il reste à instancier le conteneur

```sh
docker stack deploy --compose-file=postgresql-arm64v8.yml postgresql-arm64
```

```sh
docker stack ps postgresql-arm64 
ID                  NAME                          IMAGE                     NODE                DESIRED STATE       CURRENT STATE          ERROR               PORTS
qnbu4intrbfb        postgresql-arm64_postgres.1   arm64v8/postgres:alpine   node3               Running             Running 23 hours ago                       
```

sur node3

```sh
docker ps
CONTAINER ID        IMAGE                             COMMAND                  CREATED             STATUS              PORTS               NAMES
ce7796551ad2        arm64v8/postgres:alpine           "docker-entrypoint.s…"   23 hours ago        Up 23 hours         5432/tcp            postgresql-arm64_postgres.1.qnbu4intrbfbq6caj0qbwi5dq
```

```sh
docker exec -it ce7796551ad2 bash
```
puis on suit la doc d'installation de miniflux : [Database Configuration](https://miniflux.app/docs/installation.html#database)

```sh
su - postgres
createuser -P miniflux
(renseigner un mot de passe)
createdb -O miniflux miniflux
psql miniflux -c 'create extension hstore'
```

## miniflux

le SGBD étant prêt on peut s'atteler à miniflux. On créé un fichier des variables d'environnements de miniflux

cat .env.production-server
```env
DATABASE_URL=postgres://miniflux:PASS@postgres/miniflux?sslmode=disable
BASE_URL=https://miniflux.fredix.xyz/
POCKET_CONSUMER_KEY=TOKEN
```
PASS est le password renseigné à la création de l'utilisateur miniflux (createuser -P miniflux). *@postgres* est le nom DNS du conteneur éponyme publié dans tout le network postgresql.  
La dernière clé sert à connecter miniflux à l'[API de pocket](https://miniflux.app/docs/services.html#pocket) pour sauvegarder depuis miniflux un article vers Pocket. Voir l'ensemble des variables d'environnement : [https://miniflux.app/docs/configuration.html](https://miniflux.app/docs/configuration.html).  
Le docker compose

cat miniflux-arm64v8.yml
```yaml
version: "3"
services:
  miniflux:
    image: miniflux/miniflux:arm64v8-latest
    env_file: .env.production-server
    networks:
      - postgresql
      - traefik-net
    ports:
      - 8080
    deploy:
      placement:
        constraints:
          - node.labels.location == cloud-arm64
      labels:
        - "traefik.port=8080"
        - "traefik.docker.network=traefik-net"
        - "traefik.frontend.rule=Host:miniflux.fredix.xyz"


networks:
  traefik-net:
    external: true
  postgresql:
    external: true
```
A savoir que ce conteneur sera attaché à 2 réseaux, celui de traefik pour être accessible par lui et celui de postgresql pour qu'il puisse se connecter au conteneur sgbd. En effet il suffit que 2 conteneurs soient dans le même réseau pour que le DNS interne de docker publie leur nom respectif.  
On lance le conteneur

```sh
docker stack deploy --compose-file=miniflux-arm64v8.yml miniflux-arm64
```

Il reste à initialiser la base de données de miniflux. On se connecte dans son conteneur

```sh
docker stack ps miniflux-arm64 
ID                  NAME                        IMAGE                              NODE                DESIRED STATE       CURRENT STATE         ERROR               PORTS
y3zealy2bimx        miniflux-arm64_miniflux.1   miniflux/miniflux:arm64v8-latest   proxy               Running             Running 3 hours ago    

ssh root@proxy

docker ps|grep miniflux
4b4e4c05df6c        miniflux/miniflux:arm64v8-latest   "/usr/bin/miniflux"      3 hours ago         Up 3 hours            8080/tcp            miniflux-arm64_miniflux.1.y3zealy2bimxr3y9n4iu2gcwa


docker exec -it 4b4e4c05df6c /usr/bin/miniflux -migrate
```
et on créé un compte admin comme indiqué dans la [doc](https://miniflux.app/docs/installation.html#docker
)

```sh
docker exec -it 4b4e4c05df6c /usr/bin/miniflux -create-admin
```

## client RSS

on accède à l'interface web pour importer un fichier opml par exemple. La dernière étape est d'aller dans dans les settings / Integrations pour activer l'API fever. Grâce à cette dernière on pourra utiliser un client RSS compatible fever. Pour cela la documentation est explicite, [Integration with External Services](https://miniflux.app/docs/services.html). Malheureusement je n'ai pas trouvé de client RSS dans f-droid compatible API fever, j'ai du me rabattre sur [Readably](https://play.google.com/store/apps/details?id=com.isaiasmatewos.readably) via l'installeur [Aurora store](https://f-droid.org/fr/packages/com.aurora.store/). 

## Hébergement payant

Pour ceux qui ne souhaitent pas s'auto-héberger, le développeur propose une offre payante hébergé par lui : [Miniflux Hosting](https://miniflux.app/hosting.html). Ou on peut simplement lui faire un [don](https://miniflux.app/#donations).

## Sources

[NextInpact](https://www.nextinpact.com/news/106030-avec-sa-nouvelle-version-lecteur-rss-libre-miniflux-joue-carte-long-terme.htm)  
[Miniflux with docker-compose](https://meta.caspershire.net/miniflux-with-docker-compose/)  
[Miniflux, lightweight self-hosted rss reader](https://www.funkypenguin.co.nz/review/miniflux-lightweight-self-hosted-rss-reader/)

