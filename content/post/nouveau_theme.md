---
title: "Nouveau theme"
date: 2023-12-09T23:33:02+01:00
draft: false
tags: ["blog"]
---

update : le thème dark-theme-editor n'étant pas compatible sur mobile je viens de passer à [hugo-theme-tailwind](https://github.com/tomowang/hugo-theme-tailwind).

Je viens de mettre à jour [hugo](https://gohugo.io) dans sa dernière version mais le thème [m10c](https://themes.gohugo.io/themes/hugo-theme-m10c/) ne fonctionne plus. J'avoue avoir la flème de chercher le pourquoi du comment, aussi c'est l'occasion de changer et ~~d'essayer [dark-theme-editor](https://themes.gohugo.io/themes/dark-theme-editor/).~~

Également je viens de migrer mon "infra" (2 serveurs, lol) de [docker swarm](https://dockerswarm.rocks) vers [nomad](https://www.nomadproject.io) d'Hashicorp. Je devrais écrire sur ce sujet d'ici peu.  