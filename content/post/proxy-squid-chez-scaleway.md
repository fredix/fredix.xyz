---
tags: ["squid","docker","scaleway"]
categories: ["devops"]
title: "Proxy Squid chez Scaleway"
slug: "proxy-squid-chez-scaleway"
date: 2019-06-10T15:20:58+02:00
draft: false
---


Pour rappel j'utilise un proxy http squid sur un serveur chez Scaleway afin que les machine n'ayant pas d'IP publique puisse accéder à Internet, voir l'article [Docker swarm chez Scaleway](/2018/12/docker-swarm-chez-scaleway/).

Un oubli dans cet article concerne le passage des variables d'environnement dans les conteneurs. En effet des conteneurs peuvent avoir besoin d'accéder à une URL externe ou à un autre conteneur via une URL publique (exemple le drone agent vers le drone serveur). Pour cela le plus simple est de configurer le client Docker comme indiqué dans cette documentation : [Configure Docker to use a proxy server](https://docs.docker.com/network/proxy/).  

cat ~/.docker/config.json 
```json
{
	"auths": {
		"https://index.docker.io/v1/": {
			"auth": "TOKEN"
		}
	},
	"HttpHeaders": {
		"User-Agent": "Docker-Client/18.05.0-ce (linux)"
	},
 	"proxies":
 		{
   			"default":
   			{
     				"httpProxy": "http://fredix:PASS@192.168.254.10:3128",
     				"httpsProxy": "http://fredix:PASS@192.168.254.10:3128",
     				"noProxy": "localhost,127.0.0.1,192.168.254.1,192.168.254.2,192.168.254.3"
   }
 }
}
```


Ainsi à chaque lancement d'un conteneur docker, il positionne les variables d'environnement *http_proxy* et *https_proxy* dans le conteneur. Malheureusement j'ai constaté que ce n'était pas suffisant à la contruction de l'image du [plugin drone Gotify](/2019/06/notification-drone-avec-gotify/). Revoici son Dockerfile

cat Dockerfile.alpine.arm64v8
```dockerfile
FROM arm64v8/golang:1.12.5-alpine3.9
MAINTAINER Frederic Logier <fredix@protonmail.com>

RUN apk add -U --no-cache ca-certificates git build-base
RUN go get framagit.org/fredix/drone-gotify/

ENTRYPOINT ["drone-gotify"]
```

Si l'on build l'image sur un serveur n'ayant pas d'IP publique (et donc sans sortie vers Internet) cela échoue 

```sh
docker build -f Dockerfile.alpine.arm64v8 -t fredix/arm64v8-alpine-drone-gotify .

Sending build context to Docker daemon    193kB
Step 1/5 : FROM arm64v8/golang:1.12.5-alpine3.9
 ---> 306636bcc7d9
Step 2/5 : MAINTAINER Frederic Logier <fredix@protonmail.com>
 ---> Using cache
 ---> 1d614d43f225
Step 3/5 : RUN apk add -U --no-cache ca-certificates git build-base
 ---> Using cache
 ---> d8006c5e52eb
Step 4/5 : RUN go get framagit.org/fredix/drone-gotify/
 ---> Running in b2667e8d7bd7
# cd /go/src/github.com/aymerick/raymond; git submodule update --init --recursive
Submodule 'mustache' (git://github.com/mustache/spec.git) registered for path 'mustache'
Cloning into '/go/src/github.com/aymerick/raymond/mustache'...
fatal: unable to look up github.com (port 9418) (Try again)
fatal: clone of 'git://github.com/mustache/spec.git' into submodule path '/go/src/github.com/aymerick/raymond/mustache' failed
Failed to clone 'mustache'. Retry scheduled
Cloning into '/go/src/github.com/aymerick/raymond/mustache'...
fatal: unable to look up github.com (port 9418) (Try again)
fatal: clone of 'git://github.com/mustache/spec.git' into submodule path '/go/src/github.com/aymerick/raymond/mustache' failed
Failed to clone 'mustache' a second time, aborting
package github.com/aymerick/raymond: exit status 1
The command '/bin/sh -c go get framagit.org/fredix/drone-gotify/' returned a non-zero code: 1
```

le `go get framagit.org/fredix/drone-gotify/` est bien passé par squid mais les git submodule non ! Car ils utilisent le protocole git:// et non pas http://

On pourrait forcer cela en ajoutant dans le Dockerfile cette directive

```sh
git config --global https.proxy http://fredix:PASS@192.168.254.10:3128
```

Cependant cela ne change rien car git utilise déjà la variable d'environnement *https_proxy* mais le git submodule ne la prend pas en compte.. on voit bien qu'il essaye de cloner *git://github.com/mustache/spec.git*. La solution trouvée ici [how-to-convert-git-urls-to-http-urls](https://stackoverflow.com/questions/1722807/how-to-convert-git-urls-to-http-urls), est de forcer le comportement global de git en remplacant les urls en git:// par https://, faisons le de manière générique 

```sh
git config --global url.https://.insteadOf git://
```

Dernier point, le log du proxy squid contenait ce type d'erreur lors des go get (pas bloquant mais moche)

```
TCP_DENIED/407 3943 CONNECT github.com:443 - HIER_NONE/- text/html
```

il suffit de forcer l'authentification à basic comme ceci

```sh
git config --global http.proxyAuthMethod basic
```

Ou comme ceci selon la [doc git](https://git-scm.com/docs/git-config#Documentation/git-config.txt-httpproxyAuthMethod)

```env
ENV GIT_HTTP_PROXY_AUTHMETHOD=basic
```

Voici le Dockerfile final 

cat Dockerfile.alpine.arm64v8
```dockerfile
FROM arm64v8/golang:1.12.5-alpine3.9
MAINTAINER Frederic Logier <fredix@protonmail.com>

RUN apk add -U --no-cache ca-certificates git build-base
ENV GIT_HTTP_PROXY_AUTHMETHOD=basic
RUN git config --global url.https://.insteadOf git:// && \
    go get framagit.org/fredix/drone-gotify/

ENTRYPOINT ["drone-gotify"]
```

On peut maintenant builder une image utilisant des git submodule depuis n'importe quel serveur qu'il ait une IP publique ou non.



