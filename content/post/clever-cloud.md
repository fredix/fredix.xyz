---
title: "clever cloud"
date: 2022-04-04T23:55:42+02:00
draft: false
tags: ["docker"]
---


je suis en train de tester le service de [clever cloud](https://www.clever-cloud.com) et j'avoue être assez bluffé. Pour celles et ceux qui connaissent, clever cloud est un [heroku](https://www.heroku.com) français.

Ils proposent d'héberger des applications mais aussi des conteneurs Docker. Pour l'instant je ne viens de tester que ce dernier et la mise en oeuvre est assez simple.  
Il suffit d'ajouter dans son git config une url de déploiement (via un git remote add clever url), ajouter sa clé ssh publique dans la console d'administration et bien sûr avec un Dockerfile à la racine du dépôt. Ensuite un simple git push -u clever master va builder le conteneur chez clever cloud et le déployer avec le certificat let's encrypt qui va bien. Ce blog tourne d'ailleurs chez eux de cette manière.


Alors pourquoi infogérer un conteneur docker alors que j'ai une VM avec drone, docker swarm et traefik ? Justement pour ne pas avoir à me préoccuper de tout cela !
Avec mon conteneur docker je suis agnostic et je peux aller chez un autre fournisseur ou bien revenir sur une infra VM type ovh/scaleway.


Cependant cela a un prix, en l'occurence 6€/mois sur une instance nano (1 vcpu et 512Mo de RAM). Alors certes pour un blog c'est un service sans doute overkill, mais pour une application avec des utilisateurs cela devient tout de suite plus intéressant, surtout avec la possibilité d'[auto-scaler](https://www.clever-cloud.com/doc/administrate/scalability/) en fonction de la charge CPU et mémoire.

Pour les applications il suffit de choisir son langage et de pusher son code, nul besoin de faire un conteneur. Pour une [application en Go](https://www.clever-cloud.com/doc/deploy/application/golang/go/) on pourra d'ailleur descendre le tarif à 4,50€/mois avec une instance pico (256Mo de RAM). Voir les tarifs : https://www.clever-cloud.com/fr/tarification/ 

A savoir que [Scalingo](https://scalingo.com) propose les mêmes prestations hormis les conteneurs docker.
