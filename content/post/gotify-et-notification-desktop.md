---
title: "Gotify et notification desktop"
date: 2023-03-25T15:00:38+01:00
draft: false
---


[Gotify](https://gotify.net) est un outil très puissant et associé à [Beehive](/2023/01/beehive-et-gotify/) permet entre autre de recevoir des liens de flux RSS. L'[application android](https://play.google.com/store/apps/details?id=com.github.gotify&hl=fr&gl=US) est certes pratique mais lorsqu'on est sur son PC on aimerait bien recevoir les alertes sur le desktop. Une solution est d'ouvrir un onglet sur son instance gotify et autoriser les notifications du navigateur. Cependant il serait plus agréable de pouvoir utiliser les notifications du desktop.

J'ai trouvé ce projet en rust, [gotify-desktop](https://github.com/desbma/gotify-desktop), qui fait exactement cela. C'est un service qui se connecte à son instance gotify et utilise les notifications du bureau (GNOME, KDE, ...). Pour l'installation sur ArchLinux ou Manjaro, un simple `yay -S aur/gotify-desktop` suffit, pour les autres il faudra sans doute suivre la doc pour compiler le code source.

Comme indiqué dans la documentation il faut créer un répertoire et un fichier dans ~/.config/gotify-desktop/config.toml. Après avoir indiqué l'url de son instance gotify il faut fournir une token. Il faut donc créer un client dans l'interface de gotify, l'appeler par exemple "gotify-desktop" et copier-coller la token générée dans le fichier de config. Pour tester on peut lancer le programme /usr/bin/gotify-desktop dans un terminal et voir les notifications (s'il y en a) s'afficher sur le desktop.

Finalement on va créer un service systemd pour lancer le programme au démarrage de la session utilisateur. On créé un répertoire et fichier dans ~/.config/systemd/user/gotify-desktop.service dans lequel il y a :

```toml
[Unit]
Description=Small Gotify daemon to send messages as desktop notifications

[Service]
ExecStart=/usr/bin/gotify-desktop

[Install]
WantedBy=default.target
```

On active le service `systemctl --user enable gotify-desktop` et on le lance `systemctl --user start gotify-desktop` ; on peut ouvrir le journal du service pour voir si tout se passe bien `journalctl --user -u gotify-desktop -f`

Et voici ce que cela donne sur GNOME :

![gotify-desktop](/images/gotify-desktop.png)
