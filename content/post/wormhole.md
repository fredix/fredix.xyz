---
title: "Wormhole"
date: 2020-07-27T20:03:35+02:00
draft: false
---

wormhole est un outil dont le nom porte à croire que l'on a à faire à un virus. Bien sûr il n'en est rien, il s'agit ici d'un excellent outil d'envoi sécurisé en P2P de fichier ou de texte. A l'origine il s'appelle [magic-wormhole](https://github.com/warner/magic-wormhole) , c'est un outil en python et en ligne de commande qui a été publié il y a quelques années.  
Depuis une version en Go a été créé, [wormhole-william](https://github.com/psanford/wormhole-william) , qui a elle même donné naissance à une version graphique : [wormholynee-gui](https://github.com/Jacalz/wormhole-gui)


![wormhole](/images/wormhole.png)


Wormhole génère un code lorsqu'on partage un fichier, il suffit de fournir ce code par un moyen sécurisé au destinataire pour qu'il le télécharge. Wormehole passe par un serveur relay qui va mettre en lien l'émetteur et le récepteur, si on le souhaite on pourra héberger son propre serveur : https://github.com/warner/magic-wormhole-mailbox-server

A noter que wormhole-gui utilise la bibliothèque [fyne.io](https://fyne.io/) qui fourni des widgets graphique à Go pour linux/mac/win. Le binaire est disponible pour les 3 plateformes ici : https://github.com/Jacalz/wormhole-gui/releases