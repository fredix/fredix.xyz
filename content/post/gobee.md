---
title: "Gobee"
date: 2021-11-28T19:20:36+01:00
draft: false
tags: ["gobee"]
---


Depuis quelques temps je me remets au développement d'un outil de stockage de médias appelé [Gobee](https://framagit.org/fredix/gobee). C'est un serveur en Go qui utilise mongoDB (et son [gridfs](https://docs.mongodb.com/manual/core/gridfs/)) pour stocker les fichiers avec une interface web.  L'objectif de Gobee est de stocker en masse ses médias en les classant par catégories et par tags. Gobee utilise les fonctionnalités du navigateur web pour afficher le document s'il le peut.  
Lorsque j'ai repris le développement j'ai du migrer du driver [mgo](https://github.com/go-mgo/mgo), qui fût malheureusement abandonné par son développeur, vers le [driver officiel](https://docs.mongodb.com/drivers/go/current/).  C'est chose faite, voici une capture de l'interface qui reste basique pour le moment :

![cli](/images/gobee.png)

Il reste à implémenter une recherche plus élaborée, une gestion des tags plus visuelle, une interface web plus jolie et un lecteur vidéo adapté pour les gros fichiers.