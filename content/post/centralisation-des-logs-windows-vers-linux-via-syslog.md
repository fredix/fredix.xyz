+++
date = "2012-01-12T20:10:27+00:00"
draft = false
slug = "centralisation-des-logs-windows-vers-linux-via-syslog"
title = "Centralisation des logs Windows vers Linux via syslog"
tags            = ["graylog2", "syslog", "windows"]
+++

[[Article publié sur linuxfr.org][1]]


#### Intro


Lorsqu'on gère un parc de serveur qui comprend malheureusement des serveurs Windows, on souhaite centraliser leurs logs, voir même recevoir des alertes en fonction du contenu. C'est simple à faire sous Linux avec rsyslog, avec lequel je transmets les syslogs vers l'excellent [Graylog2][2]. Par contre dans le monde Windows il n'y a pas de serveur syslog natif. J'avais donc trouvé [MonitorWare Agent][3] un impressionnant logiciel propriétaire qui permet de définir des services qui vont lire n'importe quel type de logs, puis associer à ces services des ruleset qui vont déclencher une action comme faire suivre les logs vers un serveur syslog. L'interface graphique permet de créer un nombre de services et de rulesets impressionnants pour enchaîner des actions les plus tordus.
Le seul problème est le coût de la licence à environ 200€ par serveur, ce qui peut être bloquant.


#### NXLOG


Prêt à laisser tomber, mon collègue et voisin de bureau [@ponteilla][4] m'a judicieusement déniché le logiciel libre [nxlog][5] qui permet de faire exactement la même chose, sans interface graphique mais ça nous fait pas peur. Après de nombreux tâtonnement et quelques mails sur la liste j'ai pu transmettre les logs d'une application qui rotate elle meme ses logs en donnant un nouveau nom au log en cours (gruik)... Cependant nxlog permet d'utiliser des wildcard sur les noms des fichiers (\*.log par ex) et même d'être récursif en allant lire tous les logs dans chaque sous répertoire. Un processor transformer permet de formater le log selon la RFC syslog voulue.
[La doc][6] montre le workflow très poussé de nxlog qui permet des enchaînements vraiment tordu, genre intégrer un schéduler dans un [module input][7], surveiller un fichier de log et le convertir en CVS vers un autre fichier [xm\_csv\_example][8]. Il intègre également un [langage][9] qui permet par exemple de tester une expression régulière sur un flux en entrée :

```
if $Message =~ /^Test (\S+)/ log_info("captured: " + $1); 
```

#### Workflow

En entrée on choisi un input module (fichier, sgbd, syslog, tcp/udp, ..), puis un processor module qui va transformer ou filtrer les datas, puis un output module (sgbd, syslog, tcp/udp, file, ..) enfin via la directive route on enchaîne ces modules entre eux. Je n'ai que survolé les possibilités du soft, mais au vu de sa richesse et de sa licence GPL/LGPL je suis plutôt surpris de ne pas le trouver dans les dépôts debian/ubuntu.


#### Config basique fichiers de logs (\*.log) -\> serveur Graylog2 Linux (syslog)

Voici ma config qui fonctionne. Le serveur Windows communique avec Graylog2 sous Linux via OpenVPN. Pour aider au paramétrage j'ai lancé sur le Linux un tcpdump pour vérifier que les paquets arrivaient bien, avec la facility voulue :

```sh
tcpdump -i tap0 "udp port 514"
```

Le fichier de conf nxlog.conf

```
## This is a sample configuration file. See the nxlog reference manual about the
## configuration options. It should be installed locally and is also available
## online at http://nxlog.org/nxlog-docs/en/nxlog-reference-manual.html
	
## Please set the ROOT to the folder your nxlog was installed into,
## otherwise it will not start.

define ROOT C:\Program Files\nxlog
# define ROOT C:\Program Files (x86)\nxlog

define CERTDIR %ROOT%\cert
define CONFDIR %ROOT%\conf
define LOGDIR %ROOT%\data

Moduledir %ROOT%\modules
CacheDir %ROOT%\data
Pidfile %ROOT%\data\nxlog.pid
SpoolDir %ROOT%\data
LogFile %ROOT%\data\nxlog.log
LogLevel INFO
	
	
<Extension syslog>
    Module  xm_syslog
</Extension>
	
<Input in>
    Module  im_file
    File    "C:\monapp\log\\*.log" # lit tous les .log dans tous les sous répertoires !
    Exec    $Message = $raw_event;
    SavePos TRUE
    Recursive TRUE
</Input>
	
<Processor transformer>
    Module  pm_transformer        
    Exec        $SyslogFacilityValue = syslog_facility_value("local2");
    OutputFormat syslog_rfc3164
</Processor>
	
<Output out>
    Module      om_udp
    Host        10.8.0.1 # IP du serveur Linux sur lequel écoute graylog2 server
    Port        514
</Output>

<Route 1>
    Path        in => transformer => out
</Route>
```

[1]:	http://linuxfr.org/users/fredix/journaux/centralisation-des-logs-windows-vers-linux-via-syslog
[2]:	http://graylog2.org/
[3]:	http://www.mwagent.com/
[4]:	https://twitter.com/ponteilla
[5]:	http://nxlog-ce.sourceforge.net/
[6]:	http://nxlog-ce.sourceforge.net/nxlog-docs/en/nxlog-reference-manual.html
[7]:	http://nxlog-ce.sourceforge.net/nxlog-docs/en/nxlog-reference-manual.html#config_module_schedule
[8]:	http://nxlog-ce.sourceforge.net/nxlog-docs/en/nxlog-reference-manual.html#xm_csv_example
[9]:	http://nxlog-ce.sourceforge.net/nxlog-docs/en/nxlog-reference-manual.html#lang "nxlog-reference-manual.html#lang"
