---
title: "MuskX"
date: 2024-03-21T21:10:45+01:00
draft: false
tags: ["nostr","social","radicle"]
---

Aujourd'hui je viens de quitter "MuskX" (à ne pas confondre avec la crypto, quoique). Ca fait un peu bizarre car j'avais créé mon compte en 2007 soit 1 an après la création de Twitter. J'avais ce compte car j'avais été recruté pour développer un site web plus ou moins concurrent mais dédié aux 16-25 ans. J'étais développeur Ruby on [Rails](https://rubyonrails.org), framework web super tendance à l'époque, et il y avait tout à faire c'était passionnant mais la cible visé était pour moi sans intérêt. Il devait y avoir quelques "stars" de la variété française de l'époque sur le site mais ça n'a jamais pris.  
J'avais eu l'idée d'utiliser un serveur XMPP afin d'y faire transiter les posts vers des bots Ruby qui faisaient des traitements et ainsi de rendre le site web asynchrone, c'était bien sûr avant la création de Redis. J'étais un peu fier de cette architecture et ça fonctionnait bien 😁

Aujourd'hui je lis ce texte de Ploum, [La nouvelle informatique](https://ploum.net/2024-03-21-nouvelle-informatique.html) et je suis dubitatif. Comment peut-on se dire être passionné par l'informatique et utiliser une machine à écrire ? Si j'ai besoin de ne pas être dérangé, je clique sur le bouton de GNOME "Ne pas être dérangé", je bascule Ghostwriter en plein écran et il a même un mode « Hemingway ». Et au pire GNOME a un bouton mode avion 😑

En 2007, c'était aussi la sortie de l'iPhone. Dans les bureaux un des développeurs en avait acheté un et le faisait tester. J'ai jeté un oeil mais sans plus d'intérêt que cela (genre le même oeil quand j'ai découvert Bitcoin en 2011 😭). Et pourtant c'était le début d'un nouveau monde qui allait changer le mode de vie de tout le monde. Il y a clairement un avant et un après smartphone et à moins d'être un rentier il est difficile d'y échapper. Mais cela ne veut pas dire subir !

L'informatique est passionnante, mais pas parce qu'on utilise vim et une machine à écrire. Elle est passionnante car elle évolue sans cesse. Chaque année voit son nouveau "buzz word". Mais est-ce que cela veut dire que cela disparait ensuite ? non, le big data est toujours là, on l'appelle data lake ou autres termes, on a parlé de systèmes experts, de machine learning, puis d'IA et ils seront toujours là. La blockchain ? elle sera toujours là.  
Ce qui est passionnant c'est que ce qui était révolutionnaire en 2007 est devenu ensuite un usage courant. Et ce qui est révolutionnaire aujourd'hui sera courant plus tard. La technologie pose ses assises et c'est à nous de faire des choix pour ne pas la subir et pour lui donner la bonne direction. La fuir c'est ne pas faire de choix, c'est donner raison à ceux qui veulent qu'on la subisse. En d'autres termes cela voudrait dire que la technologie est comme ça et pas autrement. Or c'est faux.

Aujourd'hui j'ai supprimé mon compte Twitter car je me suis rendu compte que je passais plus de temps sur [Nostr](/2024/03/nostr/), [Bluesky](https://bsky.app) et [Mastodon](https://joinmastodon.org/fr) et les idées prônées par son propriétaire ne sont pas les miennes. Ces alternatives n'en sont plus, car elles sont dans mon quotidien, comme Linux l'est. J'ai essayé et j'ai choisi.

De même grâce à mes compétences, j'ai choisi de participer à ce mouvement en mettant en place un noeud Nostr : [Nostromo](https://nostromo.social).  
Je viens de mettre également en place un noeud [Radicle](https://radicle.xyz). Radicle est une forge décentralisée en P2P, Korben en parlait i ly a 5 ans, [Radicle – Le Github décentralisé](https://korben.info/radicle-le-github-decentralise.html). Depuis ils ont [viré](https://radicle.xyz/faq) la guignolerie IPFS et devraient sortir une version 1.0 fin mars.

Ce mouvement est celui d'une informatique différente, avec des valeurs, construite par et pour ses utilisateurs. Il parait que Linux vient de [dépasser 4% sur le poste de travail](https://linuxfr.org/users/jerome_misc/liens/linux-sur-le-bureau-vient-de-depasser-la-barre-des-4), cela semble très peu mais c'est énorme pour un système sans marketing qu'on ne peut quasiment pas acheter pré-installé sur un PC, développé en grande partie par des gus en slipbar chez eux 🤙


Twitter est né un 21 mars 2006 et par une étrange coincidence ce 21 mars je viens d'y mettre fin. Twitter n'existe plus dans mon monde, remplacé par autre chose qui représente pour moi l'avenir. Dans les années 2000 on a permis à des monstres d'émerger, mais les logiciels libres et réseaux physiques n'étaient pas ceux d'aujourd'hui. Il est temps de réparer cela.

## Liens

[Radicle : le projet open-source alternatif à GitHub vise à faciliter la collaboration sur le code en mode peer-to-peer](https://alm.developpez.com/actu/354980/Radicle-le-projet-open-source-alternatif-a-GitHub-vise-a-faciliter-la-collaboration-sur-le-code-en-mode-peer-to-peer-sans-dependre-d-un-serveur-centralise/)