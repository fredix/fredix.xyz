---
title: "Les applications sur Linux"
date: 2024-01-13T14:26:50+01:00
draft: false
tags: ["linux","app"]
---

Voici un billet sur les applications que j'utilise le plus souvent sur mon bureau. Ma priorité est d'utiliser au maximum des applications natives (compilées) pour la rapidité et l'intégration à l'OS, mais ce n'est malheureusement pas toujours le cas.

[update 01.24] : 
* ajout dans la section [Telegram](/2024/01/les-applications-sur-linux/#telegram) et correction des liens.
* ajout de [Machines](/2024/01/les-applications-sur-linux/#machines)

[update 02.24]
* ajout de [Pamac](/2024/01/les-applications-sur-linux/#pamac)

[update 03.24]
* ajout de [NewsFlash](/2024/01/les-applications-sur-linux/#newsflash)
* ajout de [Tuba](/2024/01/les-applications-sur-linux/#tuba)
 
## Manjaro Linux

Avant de parler applications, l'OS est la brique indispensable d'autant plus que nous avons le choix avec Linux. Je suis passé par de nombreuses distributions comme slackware, debian, mandrake, suse, ubuntu, fedora ... Mais à ce jour [Manjaro](https://manjaro.org) est sans nul doute la meilleur distribution de Linux que j'ai utilisé. Manjaro est basée sur Arch, donc en rolling release c'est à dire en mise à jour continue. Elle utilise ses propres dépôts qu'elle synchronise avec ceux de Arch. Elle est très stable et dès l'installation préconfigurée contrairement à Arch.  

Sur ce sujet, j'avais fais un détour du côté de Mac en 2014 et j'avais écris ce billet [J’ai switché sur Mac et j’en suis ravi](/2014/10/jai-switche-sur-mac-et-jen-suis-ravi/) plutôt provocateur je l'avoue. En 2016 je suis revenu sur Linux [rollback to Linux Fedora](/2016/11/rollback-to-linux-fedora/). En tant que fan de tech je suis content d'avoir connu l'univers d'Apple qui est de très loin supérieur à celui de Microsoft. 

Mais au final il y a 2 univers, le monde du libre et les autres et je fais parti du premier dans mon ADN. Je ne suis pas contre utiliser du proprio s'il n'y a pas d'alternative satisfaisante à mes goûts.  
L'univers du bureau Linux s'est grandement amélioré en qualité, et grâce à Valve et Steam je peux jouer à des jeux AAA sans problème. Et contrairement à un Mac je pourrais mettre à jour mon Linux indéfiniment sur mon PC.  

Apple fourni cependant un écosystème aux developpeurs qui leur permet de vivre. Dans le libre il faudrait sans nul doute participer plus à des initiatives comme [open collective](https://opencollective.com), [Ko-fi](https://ko-fi.com), [Liberapay](https://fr.liberapay.com), c'est en tout cas mon objectif.

## Pamac

[Pamac](https://gitlab.manjaro.org/applications/pamac) est un outil graphique sur Manjaro qui permet l'installation de logiciel et la mise à jour du système d'exploitation. Toutes les distributions Linux proposent un équivalent graphique et celui de Manjaro est plutôt joli comme on peut le voir :

![pamac](/images/apps/pamac.png)

## Steam

Dans la Tech [Valve](https://www.valvesoftware.com/fr/) font parti des dieux vivants. Ils ont travaillé durement pour pousser [Steam](https://store.steampowered.com/) sur Linux et développé [proton](https://github.com/ValveSoftware/Proton) pour améliorer la compatibilité des jeux Windows. La base de données [protondb](https://www.protondb.com) permet de vérifier que de nombreux jeux tournent sans problème sur Linux et le [Steam Deck](https://store.steampowered.com/steamdeck). J'ai pour ma part joué au chef d'oeuvre [Cyberpunk 2077](https://store.steampowered.com/app/1091500/Cyberpunk_2077/?l=french) sur Manjaro, avec du matériel AMD bien mieux supporté que NVidia.

## Spotify

A l'occasion j'écoute de la musique via [Spotify](https://www.spotify.com/). Leur application bureau fonctionne bien (**[update]** même si c'est une application [Electron](https://www.electronjs.org) 🤢).  
Une pub pour la BO de [Cyberpunk 2007](https://open.spotify.com/playlist/37i9dQZF1DWYMokBiQj5qF?si=aaec2082a34e438c) qui est légendaire 🎸.

## Telegram

[Telegram](https://telegram.org) est à mon avis la meilleur application de tchat tous OS confondu. La version [desktop](https://desktop.telegram.org) est juste fantastique : fluide, native (pas en mode web desktop moisi façon electron) et sobre. Elle possède toutes les fonctionnalités de la version mobile dont les appels audio et vidéos.  
Je rajoute que Telegram (bureau et mobile) permet de streamer une caméra dans un canal ou un groupe, ce qui en fait une alternative sérieuse à Twitch : [Diffusion illimitée en direct](https://telegram.org/blog/live-streams-forwarding-next-channel/fr?ln=a#diffusion-illimitee-en-direct) ; de plus avec la version desktop on peut obtenir un lien vers un serveur rtmps de Telegram afin de streamer avec OBS Studio.

[Jami](https://jami.net) pourrait être une alternative mais je ne connais personne qui l'utilise :( Mais j'y ai un compte.

Je regrette l'arrêt du développement de [qTox](https://qtox.github.io) qui aurait du devenir le standard dans le libre.


## Vivaldi

J'ai utilisé de nombreuses années Firefox et un peu Chrome. Mais les fonctionnalités de [Vivaldi](https://vivaldi.com/fr/desktop/) sont excellentes, il intègre notamment un client mail, agenda et RSS. Contrairement à Chrome il respecte la vie privée et compatible avec les extensions Chrome. Et bien sûr très rapide, il utilise le moteur de rendu web [Blink](https://fr.wikipedia.org/wiki/Blink_(moteur_de_rendu)).

## Firefox

Parce que le web est un pilier d'Internet il est dangereux de l'attacher à un seul moteur web piloté par une entreprise (Google), j'utilise encore un peu [Firefox](https://www.mozilla.org/fr/firefox/) qui a son propre moteur. La politique de Mozilla qui gère le projet est cependant bien étrange et tente de se diversifier au lieu de se concentrer sur le navigateur. On verra bien ce que cela donne...

## Ghostwriter


Après avoir testé de nombreux éditeurs markdown, mon choix s'est arrété sur cet éditeur natif qui fait simplement le boulot, [Ghostwriter](https://ghostwriter.kde.org/fr/)

## Obsidian

Je fais ici une entorse au natif car [Obsidian](https://obsidian.md) est une application electron (techno web sur desktop). Toutefois il est excellent pour écrire et organiser ses documentations localement et en markdown. Il propose un grand nombre de plugins pour étendre ses capacités : [https://obsidian.md/plugins](https://obsidian.md/plugins). La version mobile permet d'accéder à ses documents, soit en utilisant leur offre [Sync](https://obsidian.md/sync) un peu cher, soit en utilisant une synchro desktop/mobile comme pCloud.

## GSConnect

10 ans après tout le monde je découvre cette extension GNOME, [GSConnect](https://extensions.gnome.org/extension/1319/gsconnect/), qui permet de connecter son mobile au desktop. Cela permet entre autres de recevoir ses notifications mobile sur le desktop, lire et envoyer des SMS, partager des fichiers, piloter son desktop depuis le mobile, etc.. La version pour KDE est [KDE Connect](https://kdeconnect.kde.org). Indispensable !

## System-monitor-next

On reste sur les extensions GNOME ; celle-ci, [system-monitor-next](https://extensions.gnome.org/extension/3010/system-monitor-next/), permet d'afficher les consommations CPU, mémoire et réseau. Une autre est [Astra Monitor](https://extensions.gnome.org/extension/6682/astra-monitor/) qui vient lui faire de l'ombre.


## Proton Bridge

J'utilise [Proton mail](https://proton.me/fr/mail) comme service email. [Proton bridge](https://proton.me/fr/mail/bridge) (client natif en Go) me permet d'utiliser mon client mail bureau, [Evolution](https://wiki.gnome.org/Apps/Evolution/), au lieux d'ouvrir un énième onglet web...

## Proton VPN

Pour rester sur Proton, ils proposent une [version Linux](https://protonvpn.com/fr/download-linux) de leur client VPN. Très joli il fait le boulot.

## pCloud

[pCloud](https://www.pcloud.com/fr/eu) est un service de stockage en ligne à la Dropbox. Il propose bien sûr un client Linux (appImage electron.. ). Il permet de monter un disque distant en local ou bien de synchroniser un répertoire. Sur option on peut également chiffrer ses fichiers. J'attend la sortie de la version Linux de [Proton Drive](https://proton.me/fr/drive) pour comparer.


## LiteIDE

Pour coder j'utilise [LiteIDE](https://github.com/visualfc/liteide), éditeur dédié à Go. Bien sûr natif (Qt/C++) il est très rapide contrairement à un Visual Studio Code webeu.  
Je rajoute au passage le grand [Qt Creator](https://www.qt.io/product/development-tools) que j'utilisais lorsque je faisais du C++, une référence des IDE.

## Bitwarden

[Bitwarden](https://bitwarden.com/download/) est un excellent gestionnaire de coffre, gratuit ou très peu cher si l'on veut des options. C'est ma troisième exception au natif car l'application est une appImage en Electron :( Voir mon billet dédié aux [gestionnaires de mots de passe](/2022/09/gestionnaires-de-mots-de-passe/)

## Shortwave

[Shortwave](https://gitlab.gnome.org/World/Shortwave) est une application d'écoute de radios. Très rapide et jolie (codée en Rust) elle utilise la base de donnée de [radio-browser.info](https://www.radio-browser.info). Je conseille les radios de soma fm (voir plus bas) ou [Couleur3](https://www.rts.ch/couleur3) par exemple. On y trouve aussi les radios nationales type France Info.

## somafm-qt

Petite application dédiée aux excellentes radios de [soma fm](https://somafm.com) j'ai d'ailleurs fait un petit patch :)  
[somafm-qt](https://github.com/josefbehr/somafm-qt)

## restic

Une des rares application en ligne de commande que j'utilise. [Restic](https://restic.net) me permet de faire un backup de certains répertoires importants. Restic peut faire des backups chiffrés vers divers services tel quel Amazon S3, Wasabi, Blackblaze, etc ([voir ici](https://restic.readthedocs.io/en/stable/030_preparing_a_new_repo.html)) et d'autres en passant par [rclone](https://rclone.org/). J'utilise l'offre de Scaleway [Object Storage](https://www.scaleway.com/fr/object-storage/) compatible S3 et gratuit jusqu'à 65Go, ce qui est pour le moment suffisant pour mes répertoires **code** et **Documents**.  
Si vous voulez gérer toute la chaine, vous pouvez utiliser [MinIO](https://min.io) comme alternative à un fournisseur S3.

En application graphique, GNOME propose l'application [DejaDup](https://apps.gnome.org/fr/DejaDup/) qui peut sauvegarder vers Google Drive ou OneDrive. 

## LibreOffice

Impossible de parler d'applications Linux sans parler de ce monument qui fait suite à OpenOffice qui faisait suite à StarOffice. Pour ma part j'évite au maximum ce genre d'application bureautique (vive le markdown). A savoir que Manjaro fourni également l'application [FreeOffice](https://www.freeoffice.com/fr/).  
[LibreOffice](https://fr.libreoffice.org)

## OBS Studio

J'ai testé rapidement [OBS Studio](https://obsproject.com/fr) avec Twitch pour streamer quelques scéances de cyberpunk :) Ca fonctionne nickel, ça fait le boulot, [bravo à eux](https://obsproject.com/contribute).


## Warp

[Warp](https://apps.gnome.org/fr/Warp/) est une application GNOME (en Rust) qui permet de s'envoyer des fichiers de manière sécurisé en réseau local ou par Internet. Elle utilise le protocole de [Magic Wormhole](https://github.com/magic-wormhole/magic-wormhole) donc compatible avec [croc](https://github.com/schollz/croc) et [Rymdport](https://rymdport.github.io) dont j'ai parlé ici : [Wormhole](/2020/07/wormhole/)


## Molotov

Il m'arrive de lancer cette app (appImage) pour regarder certaines chaines TV.  
[Molotov](https://www.molotov.tv/download)

## qBittorrent

Bittorrent est loin d'être mort, j'utilise parfois ce client très complet à égalité avec [Transmission](https://transmissionbt.com)  
[qBittorrent](https://www.qbittorrent.org)

## Sublime Text

Un des meilleurs éditeur de texte à ce jour et customisable via un store de packages, [Package Control](https://packagecontrol.io)  
[Sublime Text](https://www.sublimetext.com)

## Authy

J'utilisais [Authy](https://authy.com) qui proposait une application bureau de gestion des codes 2FA. "Proposait" car ils viennent d'annoncer la fin du développement de la version bureau : [Authy for Desktop End of Life (EOL)](https://support.authy.com/hc/en-us/articles/17592416719003-Authy-for-Desktop-End-of-Life-EOL-) en août 2024. C'est l'occasion de les migrer sur Bitwarden (version payante) qui les gère ([Bitwarden Authenticator](https://bitwarden.com/help/authenticator-keys/)).

## Machines

[Machines](https://apps.gnome.org/fr/Boxes/) est aussi connu sous le nom de GNOME Boxes. Elle permet de manière très simple de créer des machines virtuelles. Très pratique pour tester une distribution, [Haiku OS](https://www.haiku-os.org) ou Windows. Si vous voulez quelque chose de plus avancé et pouvoir gérer des VMs à distance il existe [virt-manager](https://virt-manager.org)

## NewsFlash

[NewsFlash](https://apps.gnome.org/fr/NewsFlash/) est 	un lecteur de flux RSS pour le bureau. Plus de détails sur cet article, [Miniflux](/2024/03/miniflux/).

## Tuba

[Tuba](https://tuba.geopjr.dev) est un magnifique client [Mastodon](https://joinmastodon.org/fr), en GTK/Vala. Très rapide ça vous fera un onglet web en moins.

## Les autres 

J'ai fais le tour de mes apps du quotidien, on peut ajouter celles que je n'utilise pas ou peu comme [Blender](https://www.blender.org), [GIMP](https://www.gimp.org), [darktable](https://www.darktable.org), [Audacity](https://www.audacityteam.org), [Ardour](https://ardour.org), [VLC](https://www.videolan.org/vlc/), [Scribus](https://www.scribus.net), [Inkscape](https://inkscape.org/fr/), [Kdenlive](https://kdenlive.org/fr/) et j'en oublie. Le bureau Linux est chaque jour plus complet, robuste et riche.


(Ce texte a été écrit avec [Ghostwriter](https://ghostwriter.kde.org/fr/))