---
tags: ["docker"]
title: "Dnote"
date: 2019-12-16T06:22:10+01:00
draft: false
---

[Dnote](https://www.getdnote.com/) est un outil de gestion de notes bien différent des autres. Il est composé d'un serveur (en Go) qui propose une interface web, ainsi qu'un client en ligne de commande. Les deux outils permettent la saisie et la recherche de notes, le serveur les enregistre  dans une base postgresql et le client dans une base locale sqlite. Un simple `dnote sync` permet de synchroniser les 2 bases.

L'intérêt de la ligne de commande est de limiter le context switching ; quand on code / admin on est le plus souvent dans un terminal dans lequel on pourra  rechercher rapidement une information via le cli dnote. Voici une démontration issue du [github](https://github.com/dnote/dnote) :

![cli](/images/cli.gif)

L'interface web permet quand à elle de consulter ses notes depuis l'exterieur ou de les [publier](https://dnote.fredix.xyz/notes/51ed0a93-269a-4793-a05b-043c9f59cb9f). 

![dnote_web](/images/dnote_web.png)


A savoir qu'il est possible de sélectionner son éditeur favori lors de l'édition en  ligne de commande :

```sh
cat ~/.dnote/dnoterc 
editor: emacs
apiEndpoint: https://dnote.fredix.xyz/api
```
La dernière ligne force le client dnote à utiliser son instance plutôt que celle du développeur. Une section de la documentation est dédiée au [self hosting](https://github.com/dnote/dnote/blob/master/SELF_HOSTING.md) et reprend en détails les éléments ci-dessus.

## Docker

Le développeur de Dnote a publié une image docker amd64 que j'utilise ici via ce docker compose : [dnote-amd64.yml](https://framagit.org/fredix/swarm/blob/master/dnote/dnote-amd64.yml)

```yaml
version: "3.3"
services:
  dnote:
    image: dnote/dnote:0.3.2
    env_file: .env.server.scaleway-postgresql
    networks:
      - traefik-net
    ports:
      - 3000
    deploy:
      placement:
        constraints:
#          - node.labels.location == cloud-arm64
          - node.hostname == nuc
      labels:
        - "traefik.port=3000"
        - "traefik.docker.network=traefik-net"
        - "traefik.frontend.rule=Host:dnote.fredix.xyz"
        - "traefik.frontend.entryPoints=http,https"
        - "traefik.frontend.redirect.entryPoint=https"
        - "traefik.frontend.redirect.permanent=true"

networks:
  traefik-net:
    external: true
```
le env_file pointe vers un fichier de variables d'environnement pour l'accès à postgresql, il est de cette forme :

```env
GO_ENV=PRODUCTION
DBHost=IP
DBPort=PORT
DBName=dbname
DBUser=dbuser
DBPassword=PASS
WebURL=https://dnote.fredix.xyz
```


Dnote a été libéré depuis peu ([Open Sourcing Dnote](https://www.getdnote.com/blog/open-sourcing-dnote/)), aussi il fonctionne sur le même modèle que [miniflux](/2019/06/miniflux-un-serveur-rss/) c'est à dire qu'une instance payante vous permettra d'éviter de gérer la partie serveur : https://www.getdnote.com/pricing/
