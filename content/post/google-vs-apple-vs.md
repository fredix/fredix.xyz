+++
date = "2014-06-07T22:59:21+00:00"
draft = false
slug = "google-vs-apple-vs"
title = "Google vs Apple vs ... ?"
tags            = ["apple", "google"]
+++

Je viens de tomber sur cet article [Google vs Apple, des visions très différentes][1] c'est l'occasion pour moi de rebondir sur ce sujet qui me titille.

La dernière [WWDC][2] d'Apple a été l'occasion de démontrer les dernières avancées autour de l'intégration entre leurs devices. On pourra ainsi commencer l'écriture d'un email sur son iPhone ou iPad et le finir sur son Mac. De même on pourra répondre à un appel téléphonique depuis son Mac. Apple mise en force sur le matériel et le logiciel natif le tout lié par iCloud. Google, semble en effet différer puisqu'il mise tout sur ses services web, bien qu'il propose également des smartphones et tablettes nexus il est absent du desktop, chromeOS étant tout sauf un succès.

En fait tout découle de leur business model. Celui d'Apple est basé sur la vente de matériels, il est donc condamné à innover régulièrement sur ce secteur. Celui de Google est basé sur son moteur de recherche et la publicité qui est leur pompe à fric, il est donc condamné à conserver son leadership dans le search en s'appuyant sur leurs services web annexes, et tous les matériels sous android.

Qui a raison, qui peut gagner, impossible à dire, le vrai problème est que les deux peuvent gagner indéfiniment... On entre dans une nouvelle ère où l'informatique s'étend vers des matériels jusque là isolés ; les véhicules, les lunettes, les montres, les maisons, etc. or même si les premiers utilisateurs seront vu comme des passionnés frimeurs fortunés, nul doute que le coût d'acquisition baissera et sera accessible au plus grand nombre. Le choix du consommateur ne sera plus quel matériel lui fait envie mais à quel univers il souhaite se lier, et de fait la question sera également imposée à tous les développeurs...

Rien de surprenant dans tout cela pour qui s'intéresse à ces sujets, mais les derniers scandales autour de la vie privée repose la question sur des alternatives à ces services propriétaires. Il s'agit bien de services et non de logiciels, par exemple l’interaction de plus en plus forte des iPhone avec iCloud va rendre ce dernier obligatoire. De même pour android avec un compte Google...

La seule alternative du libre, qui peut paraître crédible est celle de Mozilla avec [Firefox OS][3]. Je ne parlerai pas de celle de [Ubuntu][4], qui prend bien trop son temps depuis des années malheureusement. Un des problèmes de Mozilla à mes yeux est de réduire l'informatique et Internet au web, d'en faire le dénominateur commun... Mettre à la trappe de nombreux outils, langages, et technologies pour se baser uniquement sur la moins pertinente techniquement est une grossière erreur, pour rester poli...

Il est à noter malgré tout qu'après des années d'extrémismes anti-cloud, mozilla se sent obligé de proposer des services, un [market][5] et [account][6] ; quand la réalité du monde percute les idéaux les plus stupide il ne reste pas grand chose... Je suis pour ma part convaincu depuis des années que le libre se doit de proposer des services, mais il n'y a pas malheureusement un libre mais des libres ... Le libre étant constitué de communautés qui travaillent souvent sur des solutions proches voir concurrentes des noyaux aux desktops, ce qui faisait notre force il y a des années, nous réduit en simple fournisseur de briques logiciels pour des fournisseurs de services ou pour les quelques utilisateurs que nous auront pu conserver.

Aussi j'ai bien peur qu'une alternative libre et respectueuse de la vie privée, fournissant des services voir aussi des matériels tels que ceux proposés par Google et Apple ne voit jamais le jour. Il est impossible que le libre puisse s'unifier afin de faire naître cela de manière homogène, cependant il y a quelques lueurs d'espoirs de faire autrement. Lorsque je regarde un produit 100% hacker tel que [bitcoin][7] obtenir un si gros succès en si peu de temps je pense que cela peu nous montrer la voie vers des services complètement décentralisés et spécialisés. Un de ces nouveaux service spécialisé basé sur le moteur de bitcoin est [twister][8] qui est à mes yeux une alternative hautement crédible à twitter, même s'il est infiniment loin d'espérer atteindre la même base d'utilisateurs avant des années. Le libre arrive parfois à accoucher de pépites, or pourquoi pas imaginer qu'un jour ces pépites puissent arriver à communiquer entre elles afin de fournir un service homogène pour les utilisateurs ? Entre temps il faudra que les libristes, les utilisent, les promeus, les hacks, au lieu de les ignorer tout en utilisant des services propriétaires centralisés... et pour les autres qui n'utilisent ni l'un ni l'autre, il s'isoleront dans leur grotte chaque jour plus seul ; numériquement bien sûr :)

[1]:	http://www.ginjfo.com/actualites/politique-et-economie/google-vs-apple-visions-tres-differentes-20140607
[2]:	https://developer.apple.com/wwdc/
[3]:	http://www.mozilla.org/fr/firefox/os/
[4]:	http://www.ubuntu.com/
[5]:	https://marketplace.firefox.com
[6]:	https://account.services.mozilla.com/
[7]:	https://bitcoin.org
[8]:	http://twister.net.co/ "twister"