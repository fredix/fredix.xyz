+++
date = "2014-10-13T00:30:07+02:00"
draft = false
title = "hugo, syncthing"
slug = "hugo-syncthing"
tags            = ["hugo", "syncthing", "opensource"]
+++

Suite à mon [switch sur Mac][1] et la découverte de l'excellent éditeur markdown [Ulysses][2][^1] j'ai décidé de l'utiliser le plus souvent possible, notamment pour écrire sur mon blog.

L'idée est de pouvoir écrire en offline et depuis un éditeur natif, n'utiliser le web uniquement pour la publication ; ce pour quoi il a été conçu à la base ...  
Il existe depuis longtemps [jekyll][3] qui fait le boulot, puis je découvre au détour d'un [commentaire chez korben][4] qui présente [JustWriting][5], le fameux [Hugo][6].  
Je ne désire plus utiliser de PHPerie ni de Rubyerie, or Hugo est exactement ce que je souhaite utiliser le plus souvent, un service en [Golang][7], et qui dit Go dit léger rapide, ultra portable.

Il tourne sans aucun problème sur OSX ce qui permet de tester et d'écrire en local afin de prévisualiser à la volée ; en effet avec l'option `--watch` Hugo rafraichi automatiquement la page à mesure que le fichier markdown est sauvegardé, inutile avec Ulysses qui le fait au bout de quelques secondes.

L'installation sur un serveur Linux est plutôt simple grâce à Go version manager ([gvm][8]) il permet d'installer la dernière version de Go et d'éviter ainsi celle un peu moisie de la distribution.  
Reste à synchroniser les documents markdown entre son desktop et son serveur. J'ai découvert dans la foulée le service de synchronisation [syncthing][9] (maintenant [Pulse][10]) lui même en Go :) L'occasion de tester et d'éviter d'installer sur un server un client dropbox, même si certains préfèreront utiliser git.

A nouveau une très bonne surprise car il permet de synchroniser des répertoires qui sont nommés différemment entre les noeuds, les répertoires à synchroniser sont simplement associés par un ID unique. Pour que les noeuds se retrouvent ils utilisent le service de recherche announce.syncthing.net si vous avez spécifié un adresse dynamique dans l'ajout d'un périphérique, sinon il vous faudra préciser l'ip et le port du noeud à relier.  
De plus contrairement à Dropbox syncthing permet de définir une [liste d'exclusion][11] de fichiers et répertoire, on peut définir un master, ici le noeud sur le desktop, afin que le serveur ne soit qu'un client qui recoit les données. L'interface web d'administration est plutôt agréable et elle permet d'imposer un login/pass indispensable pour l'admin du client sur le serveur.  
Enfin la connexion entre les 2 noeuds se fait sans problème à la Dropbox.

Dans la liste d'exclusion j'exclue le répertoire public afin que le paramétrage et la génération des pages de rendues en local ne viennent pas perturber l'instance sur le serveur. Le repertoire `content` possède les fichiers markdown à synchroniser et la synchro de `themes` permet de modifier en local son thème et impacter immédiatement sur le serveur, ou d'en ajouter.

A savoir qu'il vaut mieux lancer Hugo en local avec l'option `--buildDrafts` afin qu'il affiche à la volée les documents en cours d'écriture, mais sans cette option sur le serveur pour qu'il ne les publie pas. Par exemple ce fichier a pour en-tête :

```markdown
+++
date = "2014-10-13T00:30:07+02:00"
draft = true
title = "hugo, syncthing"
tags            = \["hugo", "syncthing"]
+++
```

le \ est nécessaire dans Ulysses sous peine qu'il transforme les tags en URL et fasse du coup planter Hugo... (personne est parfait). Une fois terminé il suffira que je remplace true par false, pour ce que texte soit publié, **magique**. 

Par défaut Hugo écoute sur le port 1313, il faudra donc sur le serveur configurer un serveur web en reverse proxy. La prochaine étape sera d'importer tous mes articles de wordpress, je ferais peut être un article du setup complet.  
En attendant à lire un article d'un utilisateur ravi, [Hugo Is Friggin' Awesome][12], et le [showcase][13] des sites web qui l'utilisent, on appréciera certains sites très classieux pour les fans du CSS.  
Bien entendu Hugo n'utilise pas de base de données, pour la gestion des commentaires il suffira d'utiliser [Disqus][14].

Je dois dire que ce workflow me parait extrèment efficace, productif, rapide et donc agréable. Ecrire avec un logiciel natif un texte en markdown qui est publié dans la foulée est à des années lumière de la lourdeur du web... responsive ou pas.

Ma liste d'exclusion syncthing :

```
*.swp
.DS_Store
.Ulysses-Group.plist
public
```

Le lancement en local :

```sh
hugo server --watch --theme=journal --buildDrafts
```

sur le serveur :

```sh
hugo server --baseUrl="http://fredix.xyz/" --theme=journal --watch --appendPort=false
```

One more thing : Hugo gère les tags, les catégories et les flux RSS.  

Ulysses étant spécifique Mac et plutôt cher on pourra le remplacer par [Marko Editor][15] qui gère tous les OS.

[^1]:	Ulysses étant spécifique Mac et plutôt cher on pourra le remplacer par Marko Editor http://marko-editor.com qui gère tous les OS.

[1]:	http://fredix.xyz/2014/10/Jai-switche-sur-Mac-et-jen-suis-ravi/
[2]:	http://ulyssesapp.com/
[3]:	http://jekyllrb.com/
[4]:	http://korben.info/justwriting-blog-100-markdown-base-donnees.html?utm_source=feedburner&utm_medium=feed&utm_campaign=Feed%3A+KorbensBlog-UpgradeYourMind+%28Korben%27s+Blog+-+Upgrade+Your+Mind%29#comment-1615190677
[5]:	https://github.com/hjue/JustWriting
[6]:	http://gohugo.io/
[7]:	http://golang.org/
[8]:	https://github.com/moovweb/gvm
[9]:	http://syncthing.net/
[10]:	https://ind.ie/pulse/
[11]:	https://discourse.syncthing.net/t/excluding-files-from-synchronization-ignoring/80
[12]:	http://npf.io/2014/08/hugo-is-awesome/
[13]:	http://gohugo.io/showcase/
[14]:	http://gohugo.io/extras/comments/
[15]:	http://marko-editor.com
