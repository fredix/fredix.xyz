+++
tags = [
  "linux",
  "fedora",
  "opensource",
]
date = "2016-11-27T18:34:51+01:00"
title = "rollback to Linux Fedora"
draft = false
slug = "rollback-to-Linux-Fedora"
+++

Cet article est une suite à [jai-switche-sur-mac-et-jen-suis-ravi](/2014/10/jai-switche-sur-mac-et-jen-suis-ravi/).

En 2013 j'étais passé dans un environnement Mac pour des raisons professionnelles ce qui m'avait mené à écrire cet article 1 an après. J'ai continué à être satisfait de mon MacBook 13" mais je songeais sérieusement à le renouveller, la résolution de 1 280 x 800 étant trop basse pour coder.  
Cependant en septembre 2016 Apple a annoncé sa nouvelle game de Macbook pro, au prix de 2700€ pour le 15"... Ce tarif stratosphérique est bien au delà de ceux pratiqués auparavant par Apple. Or il est aisé de trouver des configuration similaire entre 1200€ et 1500€.  
Même si MacOS a de nombreux avantages, parmis ceux énumérés dans mon précédent article, ces nouveaux tarifs parviennent à les effacer complétement à mes yeux et ce n'est pas une touch bar gogo-gadget qui va changer ça, il y a des limites à ne pas franchir et Apple vient de prendre une catapulte pour le faire. Je conçois qu'un matériel de grande qualité se paye, et je vais sans nul doute avoir du mal à retrouver un PC avec par exemple un trackpad qui s'approche de celui des Mac, mais pour environ 1500€ de moins il est possible de trouver un Dell 17" avec SSD/HDD et 16Go de RAM. Bref, visiblement Apple se satisfait de la vente de ses iPhone, et le Mac ne devient qu'un matériel toujours plus ciblé pour un public financièrement élitiste.

**mise à jour** : l'autre problème et pas des moindres  est qu'après la batterie maintenant [le SSD est également soudé](http://www.lemondeinformatique.fr/actualites/lire-mauvaise-surprise-le-ssd-est-soude-sur-le-macbook-pro-touch-bar-66534.html)  ... A ce titre on peut définivement dire que les Mac ne sont plus des ordinateurs en tant que tel, mais des matériels fermés au même titre qu'un iPhone. Mon aventure avec Apple se termine donc ici et a malgré tout était très enrichissante pour connaitre une autre facette de l'informatique qui brasse des milliards et qui impose sa vision a ses partenaires et ses clients.

Bref, ceci posé et le Mac vendu, quid des logiciels ?  

### Fedora
Pour l'OS j'ai redécouvert récemment la distribution [Fedora](https://getfedora.org/fr/) et j'avoue avoir été énormément surpris par sa qualité et sa finition. La version 25 sortie récemment utilise d'ailleurs le serveur graphique Wayland, on est donc bien à la pointe de ce que propose le libre. L'ensemble est très stable, et les petits bugs que je pouvais avoir avec une Ubuntu sont absent (genre le virt-manager qui ne se connecte pas dans une vm avec le driver graphique spice).  
ps : big up à [Patrice Ferlet](https://www.metal3d.org/) pour m'avoir poussé vers Fedora.

## Logiciels
J'utilisais sur Mac une "stack" de logiciels qui me convenait parfaitement, voici mes alternatives.

### Ulysses
J'utilisais cet excellent éditeur Markdown  [Ulysses](https://ulyssesapp.com/) pour éditer mon blog et quelques textes. J'écris cet article avec le non moins excellent [remarkable](http://remarkableapp.github.io/) qui même s'il ne fourni pas toutes les fonctionnalités d'Ulysses, apporte le live preview, la coloration syntaxique du code, des thèmes, etc. Le minimum vital mais suffisant pour se passer d'Ulysses.

## 1password
Ce logiciel est une pépite pour gérer convenablement ses mots de passe, il s'intègre dans les navigateurs via une extension et propose une version smartphone. Comme Ulysses il n'a pas de version Linux et son tarif est plutôt élevé. [Enpass](https://www.enpass.io/) fonctionne sur Linux et est gratuit sur les desktops. Contrairement à 1password il ne peut gérer qu'un seul coffre, mais est similaire sur les autres fonctionnalités, quant à la version smartphone elle est très abordable.


### Dropbox
Dropbox fonctionne lui sur Linux, il n'y avait pas de raison liée à mon départ de Mac pour ne plus l'utiliser. Cependant j'ai découvert [syncthing](https://syncthing.net/) qui est un superbe binaire autonome (vive golang) avec une interface web pour gérer le partage de répertoire entre ses devices.  
Associé avec fuse / [encfs](http://www.arg0.net/encfs) et [cryptkeeper](http://tom.noflag.org.uk/cryptkeeper.html) on peut chiffrer des répertoires critiques, pour par exemple accéder rapidement à ses fichiers perso depuis son travail, un simple clic dans cryptkeeper permettant de démonter le montage fuse et de rendre le répertoire illisible ; l'inverse demande bien sûr le mot de passe associé au coffre.  
On peut d'ailleurs envisager d'utiliser cette technique pour partager ses données dans un cercle de contacts, où chacun réplique mutuellement ses données chiffrées, plus besoin de serveur/cloud centralisés chez un tiers de non confiance.  
A ce sujet, l'élection de Trump risque de mettre en dernier coup dans la confiance que l'on pouvait accorder aux clouds et services américains, mais à chacun juger...

Dernier point, syncthing est disponible sur Android, cela permettra de synchroniser son fichier enpass et de le consulter avec la version mobile de enpass.

### Quiver
Aaaah.... [Quiver](http://happenapps.com/#quiver), sans doute l'application qui aurait pu freiner mon départ de MacOS. J'utilisais cet outil purement magnifique comme base de connaissance sur tout ce que je dois aborder. Rien de plus énervant de devoir consulter une énième fois la doc de tel langage ou tel logiciel que l'on avait péniblement décortiqué il y a plusieurs semaines, mois ou années. Quiver permet de classer son savoir, ses bouts de code, le rechercher, le synchroniser via le service cloud de son choix ou simplement par un répertoire partagé. Nul besoin d'installer un wiki sur un serveur et tout ce qui va avec en administration et sauvegarde.  
L'auteur ne souhaite pas faire de version en dehors du Mac, ce qui peut se comprendre vu que quasiment seul les utilisateurs de Mac sont prêt à payer, mais il est ouvert à tout développement sur d'autres plateformes ( [cross-platform-support](https://github.com/HappenApps/Quiver/wiki#cross-platform-support) )  et décrit son format de fichier : [Quiver-Data-Format](https://github.com/HappenApps/Quiver/wiki/Quiver-Data-Format)  

En attendant qu'une bonne âme se penche sur une version Qt, j'ai finalement pu trouver ce qui se rapproche le plus avec [cherrytree](http://www.giuspen.com/cherrytree/). Il ne propose pas autant de colloration syntaxique du code, (pas de golang :'( ), mais gère l'équivalent des Cells sous la forme de Codebox. De plus, pouvoir classer son savoir sous forme de noeud est intéressant. Il sauvegarde tout sous la forme d'un fichier sqlite ou xml au choix, que je synchonise avec syncthing.

### Dash
[Dash](https://kapeli.com/dash) est un complément à Quiver, il permet d'accéder à la documentation complète d'une palanquée de langage. [Zeal](https://zealdocs.org/) permet de lire les docsets de dash, même s'il manque l'intégration via des app tierce telle que [Alfred](https://www.alfredapp.com/).

### Alfred
[Alfred](https://www.alfredapp.com/) est un application qui permet de lancer rapidement au clavier des applications, mais aussi de déclencher des workflows définis par l'utilisateur. Elle permet d'exploser sa productivité et de ne quasiment plus toucher à la souris/trackpad.
Autant dire qu'on va être loin de trouver un équivalent, mais [omgubuntu](http://www.omgubuntu.co.uk/2016/10/your-favourite-alfred-app-launcher-linux) compare [synapse](https://launchpad.net/synapse-project) synapse et [albert](https://github.com/ManuelSchneid3r/albert) qui permettent au moins de lancer des apps. Les 2 sont en code natif (vala, C++/Qt) mais le premier plante sur ma Fedora et le 2ème ne s'y trouve pas, mais j'ai pu le compiler à partir des sources et le tester ; très rapide à l'usage il permet de lancer des apps, retrouver un fichier ou lancer une recherche dans un moteur de recherche.

[Mutate](https://github.com/qdore/Mutate) semblait intéressant également mais n'est plus mis à jour depuis plus d'un an.

### Skitch
J'utilisais [skitch](https://evernote.com/intl/fr/skitch/)  pour rapidement faire une capture d'écran d'un bureau, d'une fenêtre ou d'une zone, pour de la doc ou la partager rapidement. [shutter](http://shutter-project.org/) le remplace sans problème à 100%.

**mise à jour** : en fait shutter ne fonctionne pas du tout avec Fedora 25. En effet cette version utilise wayland à la place de Xorg, or pour des raisons de sécurité les applications graphique n'ont plus accès directement autres applications. La solution est soit de démarrer Fedora en mode Xorg à la place de wayland, soit d'utiliser gnome-screenshot (capture d'écran) qui est compatible avec wayland. Il est loin de proposer les mêmes fonctionnalités que shutter mais en attendant que ce dernier supporte wayland..

### Twitter
L'alternative au client Twitter pour Mac se nomme [corebird](https://corebird.baedert.org/). Mais l'alternative à Twitter se nomme [Twister](http://twister.net.co/).

### telegram, sublimetext ...
Ces logiciels fonctionnent et sont à jour sur Mac,Win,Linux, pas de soucis donc si on les utilisait sur Mac.

### Les electrons
Pour finir se sont les applications qui utilisent le framework [electron](http://electron.atom.io/) en js,html,css. Cela permet de proposer des applications desktop qui ressemble à des applications natives. Conçu par github pour l'éditeur de texte [atom](https://atom.io/), Linux en profite souvent, par exemple [molotov](https://www.molotov.tv/) distribue une version Linux de leur application.
A mes yeux c'est un piège, car cette techno très lourde consomme énormement de ressources, par exemple atom peut rapidement dépasser 1go de mémoire, contrairement à sublimetext qui ne consomme rien.


Avec du recul, je pense qu'il est possible que le desktop Linux ait de moins en moins à envier au Mac, et peut-être qu'avec le déploiement de [flatpak](http://flatpak.org/) ou de [snapcraft](http://snapcraft.io/) , cela facilitera la vie aux développeurs et aux utilisateurs et donc développer une économie où tout le monde y trouve son compte. 

