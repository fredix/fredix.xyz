---
title: "Obsidian"
date: 2021-11-17T21:21:13+01:00
draft: false
tags: ["linux"]
---

J'utilise depuis peu l'outil de gestion de notes en markdown [Obsidian](https://obsidian.md). [Dnote](/2019/12/dnote/) ne bouge malheureusement plus beaucoup et il lui manque la possibilité d'écrire des notes dans des dossiers et sous-dossiers.  
J'avoue que l'essayer a été une vrai surprise tant ce logiciel possède de nombreuses qualités. Mais tout d'abord évacuons les "défauts" : c'est une application desktop en [Electron](https://fr.wikipedia.org/wiki/Electron_(framework)) et qui est propriétaire (ouille). 

Pour le vérifier j'ai fais un simple mount de l'appimage (type 2) dans un répertoire comme expliqué ici https://docs.appimage.org/user-guide/run-appimages.html. Or il s'avère quà l'usage l'application est très fluide, ce qui me fait changer d'avis sur les applications en Electron je veux bien le reconnaitre ou du moins certaines.    
Concernant l'aspect propriétaire l'application écrit les fichiers en texte brut donc pas de format propriétaire ici. A choisir je préfère cela à une application libre qui utilise un format à la con type sqlite..

Pour les avantages, elle est gratuite pour un usage personnel et stocke les .md en local. Les développeurs proprosent également des versions iOS et Android.  
Donc si l'on souhaite synchroniser ses documents entre son PC et l'application mobile il suffira de les stocker dans un [pcloud](https://www.pcloud.com) par exemple ou bien de choisir l'outil de [sync intégré à Obsidian](https://obsidian.md/sync).

Outre l'édition en markdown Obsidian permet de relier chaque note entre elles par des liens, et la fameuse vue graphique permet de représenter ces liaisons.

![obsidian](/images/obsidian.png)

Enfin il propose un store intégré de plugins communautaires, 393 à ce jour, ainsi qu'un store de thèmes.  
Pour en savoir plus je vous conseille cette chaine youtube dédiée à Obsidian : https://www.youtube.com/channel/UCOCnktovglBI6c4hK_42tQg/videos