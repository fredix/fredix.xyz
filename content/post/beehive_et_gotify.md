---
title: "Beehive et gotify"
date: 2023-01-20T19:33:23+01:00
draft: false
---

Voici 2 services web opensource en Go (oui j'ai une appétence pour les logiciels léger et rapide) qui vont permettre de déclencher une action sur un évènement et de recevoir une alerte.  


#### [Beehive](https://github.com/muesli/beehive)

Beehive est un IFTTT basique mais libre. Il possède près de 55 *bees* (connecteurs event et action) qu'il peut enchainer par 2. C'est donc assez limité mais suffisant pour des besoins simples, si on veut un outil plus complet mais complexe [n8n](https://n8n.io) semble faire le job.

Voici quelques *bees* :

![beehive_hives](/images/beehive_hives.png)



Dans cet exemple je souhaite recevoir une notification sur mon smartphone dès qu'un nouveau article est publié sur mes sites d'actualités, [LinuxFR](https://linuxfr.org) et le [journalduhacker](https://www.journalduhacker.net). Pour cela je créé autant de connecteur RSS que de flux que je souhaite interroger, exemple pour le JDH :

![beehive_jdh](/images/beehive_jdh.png)

L'ensemble de mes *bees* :

![beehive](/images/beehive.png)

Outre les flux RSS on voit ici un *bee* gotify. Lui sera en sortie des nouveaux article RSS, pour cela on créé un chain. On sélectionne un event bee que l'on associe avec un action bee. On a donc créé auparavant un action bee Gotify en indiquant l'url de son gotify et la token. Voici le chain pour le JDH, on voit que je passe en paramètre à Gotify le `{{.title}}` et le `{{.links}}` qui proviennent du RSS.

![beehive_chain_rss_gotify](/images/beehive_chain_rss_gotify.png)

Et voici mes *Chains* :

![beehive_chains](/images/beehive_chains.png)


A noter qu'il n'y a pas eu de nouvelle version depuis 2020, mais le projet semble en cours de réécriture donc à voir sur la durée. Beehive ne propose pas d'authentification, donc si l'on souhaite l'utiliser sur un serveur exposé à Internet il faudra le protéger derrière un basic auth par exemple.


#### [Gotify](https://gotify.net)

Gotify est un simple serveur qui reçoit et envoie des messages. Son intérêt est qu'il propose une application android ce qui est bien pratique pour recevoir des alertes de toutes sorte ou simplement des notifications, plus efficace et moins intrusif qu'un SMS. Dans l'interface web de Gotify on créé une application beehive, on obtient une token qu'il faudra fournir dans le *bee* de Beehive.

Voici mes différentes applications ; Gotify ne sert pas uniquement à Beehive, je reçois une alerte lors d'une connexion ssh sur mon serveur, le nombre de paquets à mettre à jour et des alertes de [uptime kuma](https://github.com/louislam/uptime-kuma). A noter que l'application [clever cloud](https://www.clever-cloud.com) ne sert à rien car ils ne gèrent pas les notification vers Gotify :'(


![gotify_app](/images/gotify_app.png)


Et au final voici ce que le smartphone reçoit :

![gotify](/images/gotify.jpg)


#### [ntfy](https://ntfy.sh)

A noter que j'ai découvert il y a peu ce service opensource de notification (en Go également). Je n'ai pas vraiment trouvé de différence avec Gotify si ce n'est qu'il propose en plus d'android une application iOS. Il n'est par contre pas supporté par Beehive.

sources:

* https://www.scrample.xyz/gotify-service-de-notifications/
* https://blog.wirelessmoves.com/2022/07/get-notified-of-ssh-logins.html
* https://www.maison-et-domotique.com/140356-serveur-notification-jeedom-ntfy-synology-docker/
* https://domopi.eu/huginn-et-n8n-des-alternatives-a-ifttt-auto-hebergees-et-open-source/
* https://korben.info/notifications-push-telephone.html