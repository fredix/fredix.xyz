+++
date = "2016-05-01T12:59:20+02:00"
draft = false
slug = "gestion-de-la-mise-a-jour-de-serveurs-Linux"
title = "gestion de la mise à jour de serveurs Linux"
tags            = ["linux"]
+++

>[update 05.2024]
>Je vois régulièrement des lectures de cette page qui est obsolète. A ce jour pour la gestion de vaos mises à jour je vous conseille l'usage d'un outil comme [Rundeck](https://rundeck.com)


post basé sur : 

[http://korben.info/apt-dater.html][1]  
[https://www.abyssproject.net/2014/09/apt-dater-mettre-jour-vos-serveurs/][2]  
[http://irrational.ca/remote-debian-upgrades-using-apt-dater][3]

L’objectif de cet article est de présenter l’outil [apt-dater][4] afin de permettre l’assistance du suivi des mises à jour de ses serveurs qui utilisent apt-get/yum/rug (Debian, Centos/Fedora, SuSe).  
En effet il est parfois souhaitable de ne pas automatiser entièrement les mises à jour via un cron. apt-dater laisse le choix à l’administrateur de mettre à jour tous les paquets d’un serveur ou que certains. Il permet de le faire également sur un groupe de serveurs, ainsi que d’afficher les serveurs non à jour et sur quels paquets. In fine il déclenche uniquement les mises à jour via des raccourcis clavier. Enfin il permet d’afficher un rapport des mises à jour, le tout sans devoir installer un outil web mais via un outil en console (Linux/OSX) sur son poste de travail.

On créé une clef ssh dédiée sur la machine de l’admin. Ma méthode est d’utiliser une clef dédiée, une autre sera d’utiliser un compte utilisateur dédié sur la machine de l’admin ; dans tous les cas il n’est pas conseillé d’utiliser une clef associée au compte root des serveurs.

```sh
ssh-keygen -f .ssh/id_rsa_apt-dater
```

Ensuite, installez apt-dater pour Linux :

```sh
apt-get -y install apt-dater screen
```

Pour OSX

```sh
brew install apt-dater
```

#### **Pour un nombre conséquent de serveurs il vaut mieux automatiser les tâches suivantes via puppet/chef/ansible/saltstack/cfengine/…**  
On créé un compte apt-dater sur tous les serveurs à administrer.

```sh
sudo adduser --system --group apt-dater && sudo chsh -s /bin/bash apt-dater && sudo mkdir /home/apt-dater/.ssh && sudo chown apt-dater:apt-dater /home/apt-dater/.ssh && sudo passwd apt-dater
```

 Copie la clef publique. 

```sh
ssh-copy-id -i .ssh/id_rsa_apt-dater.pub apt-dater@IP
```

Suppression du mot de passe du compte apt-dater.

```sh
sudo passwd -dl apt-dater
```

Lancer visudo pour ajouter cette ligne. Elle fourni les droits root à apt-dater sur la commande apt.  (indiquer /usr/bin/yum ou /usr/bin/rug pour une CentOS ou une SuSe)

```sh
apt-dater ALL=NOPASSWD: /usr/bin/apt-get, /usr/bin/aptitude
```

Installer le client apt-dater-host.

```
sudo apt-get -y install apt-dater-host screen
```

Sur le poste administrateur après avoir lancé une fois apt-dater, éditer le fichier _.config/apt-dater/hosts.conf _ et regrouper les serveurs par thèmes ou localisation

```toml
[Backup] 
Hosts=apt-dater@node1.backup.com;apt-dater@node2.backup.com
[Monitoring]
Hosts=apt-dater@monitoring.com
[Webservers] 
Hosts=apt-dater@web.com;apt-dater@web2.com
```

Editer le fichier .config/apt-dater/apt-dater.conf
pour lui indiquer la clef ssh à utiliser

```toml
[SSH]
SSH binary
Cmd=/usr/bin/ssh
OptionalCmdFlags=-t
SpawnAgent=true
AddKeys=/Users/fredix/.ssh/id_rsa_apt-dater
	
[SFTP binary]
SFTPCmd=/usr/bin/sftp -i .ssh/id_rsa_apt-dater
```

Lancer api-dater.  la touche « ? » affiche les raccourcis disponible.   
On se déplace avec les flèches directionnelles vers le serveur cible , puis :

- u : met à jour un serveur entier
- c : lance une connexion ssh vers le serveur
- C: lance une connexion sftp
- e : affiche les problèmes de mise à jour
- i : installe un paquet sur le serveur sélectionné, ou les serveurs tagués

Les flags à gauche de chaque serveur indique son status. Il ne manque que l’uptime et la charge cpu/mémoire :/

![][image-1] 

![][image-2]

Il ne reste qu’à distribuer la clef privé id_rsa_apt-dater à l’équipe admin.

[1]:	http://korben.info/apt-dater.html "http://korben.info/apt-dater.html"
[2]:	https://www.abyssproject.net/2014/09/apt-dater-mettre-jour-vos-serveurs/ "https://www.abyssproject.net/2014/09/apt-dater-mettre-jour-vos-serveurs/"
[3]:	http://irrational.ca/remote-debian-upgrades-using-apt-dater "http://irrational.ca/remote-debian-upgrades-using-apt-dater"
[4]:	https://www.ibh.de/apt-dater/ "apt-dater"

[image-1]:	/images/apt-dater1.png "apt-dater1.png"
[image-2]:	/images/apt-dater-2.png "apt-dater-2.png"
