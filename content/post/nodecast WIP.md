+++
date = "2015-05-11T13:17:49+02:00"
draft = false
title = "nodecast WIP"
tags            = ["nodecast", "opensource"]
+++

### Work in Progress
Il est temps de faire une présentation du projet opensource sur lequel je travaille depuis quelques mois.

### Pourquoi
L'idée qui m'a poussé à me lancer dans ce projet est lorsque j'ai fait le constat qu'en 2014 (et 2015) il est encore très complexe de partager des médias avec des utilisateurs novices tout en protégeant leur vie privée.  
Le leader en la matière est Dropbox, cependant il nécessite de faire confiance à un cloud étranger, service qui a de plus un accès au mot de passe de l'utilisateur... Il faut également comprendre les notions de fichiers / répertoires puiqu'il les synchronise.  
Quid de ma mère si je souhaite lui envoyer des Giga-octets de photos/vidéos familiales dans ces conditions ?  
Si l'on souhaite éviter les clouds propriétaires, iCloud, Google Drive, Dropbox, il ne reste pas grand chose d'abordable techniquement dans le libre. Or nous possédons 2 fondations technique qui permettraient de résoudre en grande partie ce problème. La plus grand difficulté étant de rendre simple à l'usage des technologies complexes.

Ma réponse est [nodecast][1], un projet de partage de médias qui utilise les protocoles bittorrent et XMPP.  

 Il est en Qt/C++ et fonctionnera sur Mac/Windows/Linux.

Nodecast n'est pas "simplement" un client bittorrent, car ces derniers mettent en avant toutes les possibilités du protocole avec la complexité qui va avec pour l'utilisateur.  De plus il est à la charge de l'utilisateur de récupérer un fichier .torrent ou bien de le créer.  
Nodecast masque cette complexité à l'utilisateur, même si les options bittorrent restent accessible dans les préférences.

[![][image-1]][2] [![][image-2]][3]


### XMPP
L'intégration de XMPP permet à nodecast d'organiser ses partages sous forme de sphères dans lesquelles on regroupe ses contacts. Une sphère représente un répertoire sur le disque dur mais également une chat room (MUC XMPP).  
La chat room n'est accessible que sur une invitation du créateur. Elle permet de discuter de manière privée qu'entre les contacts présents dans celle-ci.  
nodecast permettra également de discuter entre 2 utilisateurs comme un client XMPP classique.

[![][image-3]][4]

### bittorrent et tracker
Lorsqu'un utilisateur glisse un fichier ou un répertoire dans une sphère, nodecast génère un .torrent qu'il transmet par XMPP à tous les contacts présents dans la sphère. Le tracker intégré permet la mise en relation en P2P de tous les contacts. Le tracker est facultatif si les contacts sont sur le même réseau local, nodecast utilise alors le [Local Peer Discovery][5] de la [libtorrent][6].  
Si l'utilisateur glisse un .torrent dans une sphère alors il est transmit tel quel à ses contacts, nodecast fait alors office de client bittorrent standard (connexion aux trackers du .torrent et P2P avec tous les peers).

nodecast utilise [UPnP et NATPMP][7] pour demander au routeur de l'utilisateur (la Box) d'ouvrir les ports utilisés par bittorrent et le tracker, et les forwarder vers sa machine. Un routeur qui ne gère ni l'un ni l'autre ne permettra pas à nodecast de fonctionner, et probablement de même si l'utilisateur est derrière un switch.

Pour le moment lorsqu'un utilisateur partage une vidéo, si un contact double clic sur celle-ci nodecast appelle le lecteur vidéo présent et par défaut dans le système. A charge à l'utilisateur d'avoir par exemple installé VLC et associé les extensions vidéos (.avi, etc) à VLC. Cependant je prévois d'intégrer la libVLC afin de rendre nodecast moins dépendant. De même pour les images, PDF et sons.  
Nodecast utilise le téléchargement en ordre séquentiel ce qui permet de visualiser une vidéo pendant son téléchargement.

[![][image-4]][8]

### Vie privée
nodecast nécessite un compte XMPP. A priori n'importe quel compte personnel (hormis gmail.com) devrait fonctionner. Cependant il est possible que le partage de fichier par XMPP fonctionne mal selon les serveurs XMPP. nodecast proposera la création d'un compte XMPP sur son instance afin de palier à cela.  
Il intégrera [OTR][9] pour chiffrer les communications et garantir la vie privée des utilisateurs.  
Il communique avec son serveur XMPP via un chiffrement TLS et un certificat SSL signé.

Pour la protection des médias je prévois de chiffrer les torrents (petits fichiers très rapide à chiffrer) et éventuellement sur option de chiffrer les médias avant de générer le torrent. Cependant cela posera certains problèmes sur de gros répertoires qu'il faudra dupliquer en local puis longuement chiffrer.

Actuellement lorsque l'utilisateur glisse un média dans une sphère, un simple lien est créé dans le répertoire de la sphère vers la donnée.

### Décentralisation
nodecast est en parti décentralisé avec bittorrent mais nécessite un serveur XMPP. N'importe qui doit pouvoir utiliser son propre serveur XMPP, tel que [prosody][10]. Dans ce cas à charge de l'utilisateur d'activer les modules MUC et file transfert du serveur.

### Futur
Je prévois de proposer sur option des fonctionnalités propres à l'instance officielle de nodecast.  
Un bot coté serveur permettra au client nodecast d'envoyer au serveur un média par FTP, cela afin de contourner les éventuelles bridages des routeurs/FAI.  
Ce bot partagera en permamence sur le serveur les données vers les contacts de la sphère l'utilisateur (seed). L'intérêt est de lui permettre d'éteindre sa machine tout en permettant à ses contacts d'obtenir les médias.  
Enfin une fonctionnalité lointaine permettra de créer des sphères publiques.

### code
nodecast est fortement basé sur le code de [qBittorrent][11], un des meilleurs client bittorrent opensource. De nombreux reliquats de code inutiles pour nodecast disparaitront.  
J'utilise l'excellent bibliothèque [qxmpp][12] qui gère de nombreuses XEP.

Nodecast est encore en version alpha, de nombreux bugs subsistent et des fonctionnalités importantes manquent encore mais les retours sont bienvenus.

[1]:	https://github.com/nodecast/nodecast
[2]:	/images/nodecast_wip.png
[3]:	/images/nodecast_wip_transfert.png
[4]:	/images/nodecast_xmpp.png
[5]:	http://en.wikipedia.org/wiki/Local_Peer_Discovery
[6]:	http://www.rasterbar.com/products/libtorrent/manual.html#start-lsd-stop-lsd
[7]:	http://www.rasterbar.com/products/libtorrent/manual.html#upnp-and-nat-pmp
[8]:	/images/nodecast_video.png
[9]:	http://fr.wikipedia.org/wiki/Off-the-Record_Messaging
[10]:	https://prosody.im
[11]:	http://www.qbittorrent.org
[12]:	https://github.com/qxmpp-project/qxmpp

[image-1]:	/images/nodecast_wip_thumbnail.png "nodecast wip"
[image-2]:	/images/nodecast_wip_transfert_thumbnail.png "nodecast wip transfert"
[image-3]:	/images/nodecast_xmpp_thumbnail.jpg "nodecast xmpp"
[image-4]:	/images/nodecast_video_thumbnail.png "nodecast video"