+++
date = "2012-03-21T22:28:06+00:00"
draft = false
slug = "pourquoi-linux-ne-percera-pas-sur-le-desktop"
title = "Pourquoi Linux ne percera pas sur le desktop"
tags            = ["linux", "desktop"]
+++

### Disclamer


Voici un billet pourrait sembler aller à  contre sens de mes convictions, cependant je n'ai pas honte de dire que des convictions sont parfois amenées à évoluer en fonction de la réalité que l'on veut bien accepter. Dans le cas où l'on refuse d'admettre les faits, il faut accepter l'idée de vivre avec sa propre réalité dans sa tête et au final sans doute plus subir que jouir de ses convictions...

Ce billet n'est pas une tentative d'excuse, n'ayant rien à justifier à personne, mais un simple billet d'humeur pour pourrait intéresser quelques geeks.


### Contexte


J'ai donc eu l'occasion de tester un week end complet un vieux macbook blanc. Même si j'avais pu lorgner du coin de l'oeil un OS X, cela n'a rien à voir avec un test complet dans les mains. La première chose qui me vient à l'esprit c'est OS X est un OS taillé au laser. Il possède une finition remarquable, que cela soit dans les polices, dans les petits détails comme dans l'écosystème de store remarquablement bien présenté. La dernière version de l''OS (Lion) ne semble subir d'aucun lag même sur ce vieux macbook de 2006. Le matériel est du même niveau avec un trackpad très agréable et beaucoup plus utilisable que les versions PC...

Je ne vais pas m'étendre mais l'OS possède une cohérence et une finition qu'aucun desktop libre n'arrive à approcher, que cela soit GNOME ou KDE. Ubuntu essaye bien de ressembler à OS X, mais il subit la stack Linux/Xorg/GTK ce qui ne lui permettra pas d'atteindre ce même niveau d'excellence.


### Théorie


Ma théorie est donc la suivante :

1. Linux est un noyau généraliste, qui essaye tant bien que mal de supporter une multitude de matériels.

2. Xorg est un serveur graphique en userspace qui possède une architecture très adaptée dans un ancien temps mais est archaïque de nos jours.

3. Les projets GNOME et KDE se concentrent uniquement sur les logiciels, ils ignorent sciemment la demande de services en ligne. Ils ne travaillent sur aucune API qui permettrait à un développeur d'un logiciel natif desktop de faciliter l'utilisation d'Internet. Chaque développeur doit donc redévelopper la roue de son côté et s'occuper de la partie développement et hébergement côté serveur s'il souhaite intégrer des fonctionnalités type cloud (synchronisation des préférences et des données en ligne, ...).

A l'inverse Mozilla essaye d'adapter le web au desktop (sic) avec le projet [WebAPI][1]. Il est "amusant" de voir que ceux qui auraient le moins de boulot pour fournir un desktop libre qui exploite Internet ne sont pas ceux qu'on aurait pu imaginer..............


### Faits


Chez Apple on trouve :



 
  1. Un noyau (micro-noyau peu importe) optimisé pour une gamme de matériel

	 
  2. Une stack graphique moderne et intégrée au plus profond de l'OS

	 
  3. Une plateforme de développement cohérente, moderne et dernièrement des API pour exploiter les fonctionnalités d'Internet (cloud Apple), afin de proposer des services de synchronisation (datas et apps), backup et partage de données.


Les 2 premiers points me rappellent furieusement les Amiga et Atari. Pour rappel pour les plus jeunes, ça été deux énormes succès, les premiers ordinateurs grand public (années 80-90). Ils ont malheureusement fini par échouer plus pour des raisons de stratégies commerciales et d'égo des dirigeants que de techniques. Cependant leur réussite a été du à un OS 100% dédié a un type de matériel (et en grande partie en langage assembleur) ce qui permettait des optimisations de folies. Les développeurs d'applications tierces connaissaient la plateforme sur le bout des doigts, ils ne perdaient pas leur temps à gérer des configuration matériel tordues.

Face au melon qu'ont pris les dirigeants d'Atari et Amiga ce qui a bridé le renouvellement de la gamme, les PC à bas coûts fait de bric et de broc ont su séduire  malgré des interfaces MS-DOS bien inférieures...

Or, ce qui a fait le succès d'Atari et Amiga, a été clairement repris par Steve Jobs : Un OS conçu pour le desktop et dédié à un type de matériel. Cette recette est, à mon avis, la seule valable pour rendre l'informatique sexy, agréable, intuitive, ergonomique, moderne. Elle a certes le "petit" défaut de lier l'utilisateur à cet écosystème logiciel et à des types de matériels et périphériques. Mais c'est le prix à payer pour ceux qui souhaite bénéficier de ces avantages, et ce prix étaient d'ailleurs  à l'époque pleinement accepté par les Ataristes et Amigaiste.


### Conclusion


Le calcul personnel à faire est donc mettre sur la balance le prix d'être lié à une marque et ses technologies proprios face au prix de subir une informatique dépassée qui n'apporte plus aucun plaisir. J'ai personnellement fait mon choix, car j'estime qu'en 2012 il est temps que l'informatique de bureau devienne un plaisir, quelque chose d'intuitif et transparent. Quelle liberté y a t-il a subir ses convictions ?

Pour ces raisons le desktop Linux ne pourra pas arriver aux genoux de ce que propose Apple avec OS X. Seule une entreprise qui ferait le choix de modifier profondément la stack Linux/Xorg pour l'optimiser à un matériel spécifique pourrait peut-être commencer à y arriver, encore que l'architecture de [Haiku OS][2] est à mon sens plus adapté par design.

Bien entendu le bureau Linux aura toujours son public, mais un public restreint, différent de la foule toujours plus grande qui souhaite se faire plaisir à utiliser un environnement sexy qui rend de plus en plus de services.  Si des communautés du libre pensent prétendre un jour opposer une alternative, il faudra autre chose que des paroles qui exigent au grand public de devenir informaticien pour "s'auto-héberger". Il faudra arrêter de troller, sortir le doigt de son derrière et tapoter sur son clavier pour coder une alternative sexy qui rend des services.

J'ai d'ailleurs testé récemment le très sexy Enlightenment ([E17][3]). Le problème est que même si le bureau après de pénibles paramétrages est à tomber (KDE ET GNOME sont juste laids à côté), il n'y a aucune application en EFL... Utiliser des applications en Qt ou GTK sous E17 n'apporte donc aucune cohérence malheureusement, sauf à gruger mollement avec des thèmes...

BREF, je n'ai pas encore acheté de Mac faute de budget :) Je souhaite vraiment que le libre puisse un jour fournir une alternative complète qui pourrait séduire un grand public, cependant les choix stupides de certains et les non-choix stupides des autres me font douter fortement de cela à moyen terme.

[1]:	https://wiki.mozilla.org/WebAPI
[2]:	http://haiku-os.org/
[3]:	http://www.enlightenment.org/ "enlightenment"