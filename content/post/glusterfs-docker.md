	---
tags: ["glusterfs","docker","arm"]
categories: ["devops"]
title: "Glusterfs Docker"
slug: "glusterfs-docker"
date: 2019-06-02T13:39:00+02:00
draft: false
---

Cet article présente l'installation du file system distribué [glusterfs](https://www.gluster.org/) sur des serveurs ARM64 chez [scaleway](https://www.scaleway.com).  
L'objectif est d'y stocker les volumes docker pour qu'ils soient synchronisé entre tous les serveurs, ainsi si le swarm déplace un conteneur sur une autre machine physique, il retrouvera ses données. Cette solution est plus robuste que celle que j'utilisais avec [syncthing](/2017/05/auto-hebergement-hybride/).

L'idéal serait de créer une partition XFS sur un disque dédié, cependant les ARM64v8 premier prix ne peuvent attacher plus de 50Go, le volume principal étant de 50go il faudrait passer à la [gamme ARM64-4G](https://www.scaleway.com/en/arm-instances/). A défaut je vais utiliser un répertoire dédié dans le volume système.


Sur CentOS la première étape est d'activer le dépôt yum de la version 6 sur tous les noeuds :

```sh
yum install centos-release-gluster6.noarch
```

Ensuite on installe le service et on l'active

```sh
yum install glusterfs-server
systemctl enable glusterd
systemctl start glusterd
```

On créé les répertoires suivants sur chaque noeuds

```sh
mkdir -p /glusterfs/data /swarm/volumes
```

Le premier sert de répertoire de travail pour gluster, les données y seront stockées mais elles ne doivent pas être accédé directement. Le deuxième servira de point de montage fuse, c'est via celui-ci qu'on écrira les données.

Chaque noeuds doit être déclaré dans le fichier /etc/hosts des serveurs, j'utilise l'interface réseau du vpncloud :

```
192.168.254.10  proxy
192.168.254.1   node1
192.168.254.2   node2
192.168.254.3   node3
```

On connecte chaque noeuds glusterfs, exemple depuis node1 

```sh
gluster peer probe node2
gluster peer probe node3
gluster peer probe proxy
```

Puis on vérifie la bonne connexion :

```sh
gluster peer status
Number of Peers: 3

Hostname: node2
Uuid: a8e03bc8-c830-499c-937f-72403c280d00
State: Peer in Cluster (Connected)

Hostname: node3
Uuid: 4a8715f5-370d-4781-8d0d-dfd10fb2a303
State: Peer in Cluster (Connected)

Hostname: proxy
Uuid: ce29e4de-2a70-437f-b3d4-59cfafe09692
State: Peer in Cluster (Connected)
```

On peut créer le volume en indiquant le nombre de replica et le répertoire de travail de chaque noeud 

```sh
gluster volume create swarm-vols replica 4 transport tcp 
node1:/glusterfs/data node2:/glusterfs/data 
node3:/glusterfs/data proxy:/glusterfs/data force
```

En cas d'erreur on peut supprimer une brick et en ajouter 

```sh
gluster volume remove-brick swarm-vols replica 3 proxy:/glusterfs/data force

gluster volume add-brick swarm-vols replica 4 proxy:/glusterfs/data force
```

on autorise le montage uniquement depuis localhost

```sh
gluster volume set swarm-vols auth.allow 127.0.0.1
```

puis on start le volume 

```sh
gluster volume start swarm-vols
```

sur chaque serveur on fait le montage fuse

```sh
mount.glusterfs localhost:/swarm-vols /swarm/volumes
```

On vérifie que le volume est ok sur chaque noeud

```sh
gluster volume status
Status of volume: swarm-vols
Gluster process                             TCP Port  RDMA Port  Online  Pid
------------------------------------------------------------------------------
Brick node1:/glusterfs/data                 49152     0          Y       9823 
Brick node2:/glusterfs/data                 49152     0          Y       22719
Brick node3:/glusterfs/data                 49152     0          Y       17137
Brick proxy:/glusterfs/data                 49152     0          Y       2003 
Self-heal Daemon on localhost               N/A       N/A        Y       10823
Self-heal Daemon on node3                   N/A       N/A        Y       17512
Self-heal Daemon on node2                   N/A       N/A        Y       23017
Self-heal Daemon on proxy                   N/A       N/A        Y       2024 
 
Task Status of Volume swarm-vols
------------------------------------------------------------------------------
There are no active volume tasks
```

```sh
gluster volume info swarm-vols
 
Volume Name: swarm-vols
Type: Replicate
Volume ID: a7528fcb-75ba-40ff-9ddf-c7742f35a3ef
Status: Started
Snapshot Count: 0
Number of Bricks: 1 x 4 = 4
Transport-type: tcp
Bricks:
Brick1: node1:/glusterfs/data
Brick2: node2:/glusterfs/data
Brick3: node3:/glusterfs/data
Brick4: proxy:/glusterfs/data
Options Reconfigured:
transport.address-family: inet
nfs.disable: on
performance.client-io-threads: off

```


Enfin on test la synchro en créant un fichier dans /swarm/volumes/ et en vérifiant qu'il est bien répliqué sur tous les noeuds.

On peut maintenant créer des services docker swarm, exemple

```sh
mkdir /swarm/volumes/znc

docker service create --name znc -p 6697:6697 
--constraint 'node.labels.location == cloud-arm64' 
--network traefik-net --label traefik.frontend.rule=Host:znc.fredix.xyz 
--label traefik.docker.network=traefik-net --label traefik.port=6697 
--label traefik.backend=znc 
--mount type=bind,source=/swarm/volumes/znc,target=/znc-data arm64v8/znc:1.7.3
```

Sources :

Pour une utlisation avancée de glusterfs : [Un système de fichiers Haute Disponibilité avec GlusterFS !](https://www.morot.fr/un-systeme-de-fichiers-haute-disponibilite-avec-glusterfs-paru-dans-glmf-144/)  
[http://embaby.com/blog/using-glusterfs-docker-swarm-cluster/](http://embaby.com/blog/using-glusterfs-docker-swarm-cluster/)  
[https://wiki.centos.org/HowTos/GlusterFSonCentOS](https://wiki.centos.org/HowTos/GlusterFSonCentOS)



