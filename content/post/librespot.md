---
date: 2018-06-25T20:14:18+02:00
tags: ["archlinux","arm","audio"]
categories: ["geek"]
title: "Librespot"
slug: "librespot"
draft: false
---

[librespot](https://github.com/librespot-org/librespot/) est un client opensource pour [spotify](https://www.spotify.com). Après une compilation assez longue sur mon raspberry pi 3 (librespot est codé en Rust), il suffit de lui indiquer son login/pass spotify en paramètre pour que le raspberry soit visible depuis n'importe quel client spotify. Ainsi on peut depuis son smartphone lancer sa musique dans une autre pièce.

	./target/release/librespot --name rasp -u user_spotify -p "pass"

ici l'appareil est nommé "rasp".

A savoir que le raspberry pi 3 bien qu'étant un processeur 64 bits, il n'est pas conseillé d'installer une archlinux 64 bits, faute de driver audio analogique, à moins d'utiliser la sortie audio HDMI, ce qui limite l'intérêt de la chose. Une archlinux 32 bits possède les drivers audio analogique ce qui permet d'utiliser la sortie son jack vers une enceinte, ou comme moi sur l'entrée auxilère d'un poste radio.

#### compilation

avant de lancer la compilation, il faudra installer les paquets suivants :
```sh
pacman -S portaudio libtool pkg-config gcc rust autoconf automake alsa-utils
```

puis lancer le build qui va télécharger les dépendances nécessaires :
```sh
git clone https://github.com/librespot-org/librespot
cd librespot
cargo build --release
```

Toutefois librespot est sujet à quelques plantages ponctuels.

#### configuration

lorsque l'on souhaite utiliser la sortie analogique il est obligatoire d'ajouter cette ligne dans le fichier /boot/config.txt 
```
dtparam=audio=on
```

et de configurer alsa pour qu'il utilise la bonne sortie
```sh
amixer cset numid=3 1
```
