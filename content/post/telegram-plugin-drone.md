+++
tags = ["drone","docker","telegram"]
categories      = ["devops"]
date = "2017-05-31T00:00:00Z"
title = "un plugin drone pour telegram"
slug = "telegram-plugin-drone"
draft = false
+++

Voici la suite de notre [déploiement continu avec drone](https://fredix.xyz/2017/05/deploiement-continu-drone/).

Maintenant que tout fonctionne, on souhaite pouvoir lancer un git push et aller boire son café pépère pendant que drone build et déploie en production.  
Cependant il serait bien d'être alerté de la réussite ou pas de l'opération. Drone propose divers plugins pour cela, dont un pour [Telegram](http://plugins.drone.io/appleboy/drone-telegram/). Les plugins Slack ou Hipchat sont sans doute plus connus pour informer une team via une notification, mais personnellement j'ai une préférence pour [Telegram](https://telegram.org)  qui propose un excellent client desktop en plus des smartphones.

En premier lieu il est nécessaire de créer un bot via le bot [BotFather](https://t.me/botfather). Il suffit de lui demander par un /newbot , donner un surnom et un nom unique au bot. On obtient une token à conserver dans son gestionnaire de mot de passe favori.

Une 2ème opération est nécessaire pour obtenir  l'id de son compte Telegram. En effet le plugin drone ne peut envoyer un message que vers un id Telegram et pas vers le nom de votre compte. Je ne sais pas si c'est une limitation du plugin ou de l'API Telegram, mais le bot [IDBot](https://t.me/myidbot) permet de l'obtenir via un */getid*  
Pour obtenir l'id d'un groupe, il faut ajouter le bot à son groupe Telegram (allez dans le profil du bot IDBot puis cliquer sur Add to group) et taper */getgroupid*

Une fois le bot créé il faudra lui envoyer un /start ce qui autorisera le plugin à vous envoyer des messages.  
On va modifier le fichier *.drone.yml* (toujours à la racine de votre dépôt git) que l'on a créé dans l'article précédent ([déploiement continu avec drone](https://fredix.xyz/2017/05/deploiement-continu-drone/)) pour lui ajouter dans le pipeline une section Telegram :

```yaml
	pipeline:
	  docker:
	    image: plugins/docker
	    repo: fredix/hugo
	    tags: latest
	    file: Dockerfile
	    secrets: [ docker_username, docker_password ]
	  ssh:
	    image: appleboy/drone-ssh
	    host: 192.168.254.1
	    user: drone
	    port: 22
	    secrets: [ssh_username, ssh_password]
	    script:
	      - "docker service update --image fredix/hugo hugo"
	    when:
	      status: success
	  telegram:
	    image: appleboy/drone-telegram
	    token: $PLUGIN_TOKEN
	    to: $PLUGIN_TO
	    secrets: [ plugin_token, plugin_to ]
	    message: >
	      {{#success build.status}}
		  build {{build.number}} succeeded. Good job.
	      {{else}}
		  build {{build.number}} failed. Fix me please.
	      {{/success}}
```

Enfin on ajoute dans via l'interface web de drone dans l'onglet secret du dépôt les secrets *plugin_token* et *plugin_to* qui contiennent la token du bot et l'id de votre compte ou de votre groupe Telegram.  Des variables permettent d'ajouter des infos comme l'auteur du commit une url vers le build drone et même d'envoyer du son/vidéo/document/sticker/...

Happy build.

![drone-telegram](/images/drone-telegram.png  "drone-telegram")


![drone-telegram-ios](/images/drone-telegram-ios.jpg  "drone-telegram-ios")
