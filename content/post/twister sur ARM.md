+++
date = "2014-10-15T23:47:57+02:00"
draft = false
title = "twister sur ARM"
tags            = ["twister", "opensource"]
+++

J'utilise depuis quelques mois [twister][1] un projet opensource basé sur le moteur de bitcoin, mais dans le but de proposer un microblogging alternatif. Pour éviter la redondance voici 2 articles :

- [https://linuxfr.org/news/twister-un-microblog-opensource-p2p][2]
- [http://www.bortzmeyer.org/twister.html][3]

Ici l'idée est de tester ce logiciel sur la nouvelle plateforme de [cloud physique de Online][4]. Cette offre propose des serveurs ARM conçus en interne (bravo), instanciables immédiatement via une [API][5]. Aucune idée du tarif, sauf que les serveurs sont disponible gratuitement pendant le beta test. Ce blog donne plus d'informations sur le matériel : [Benchs and specs of Online's cloud preview from inside][6]

Première chose, Golang n'est pas encore disponible sur ARM officiellement, même s'il le sera d'après cette [roadmap][7] pour la version 1.5. Cependant Ubuntu propose un port de la 1.2 sur ARM, certains outils comme [camlistore][8] nécessite la 1.3 mais [Hugo][9], le serveur de page web statiques en markdown, se compile et fonctionne très bien avec.

> **UPDATE** : finalement [Go Version Manager (gvm)][10] arrive très bien à compiler Golang 1.3.3 sur ARMv7. Il est donc fortement conseillé d'utiliser gvm pour être à jour des dernières releases de Go plutôt que d'utiliser les paquets Ubuntu.

[Pulse][11] en Golang (ex [syncthing][12]) propose un binaire pour ARMv7  qui fonctionne très bien.

Enfin twister. Voici la [doc de compilation sur Linux][13], mais attention pour ARM le configure nécessite quelques paramètres particuliers sous peine d'échouer :

```sh
./configure --enable-sse2=no --with-boost-libdir=/usr/lib/arm-linux-gnueabihf
```

puis

```sh
make -j2
```

malgré les 4 coeurs du processeur il vaut mieux compiler sur 2 coeurs uniquement, avec un -j4 on risque de saturer les 2Go de RAM du serveur :

```
virtual memory exhausted: Cannot allocate memory
```

Une fois la compilation terminée, qui est plutôt longue, on lance :

```sh
twisterd --debug -rpcuser=monuser -rpcpassword=monpass -rpcallowip=127.0.0.1
```

le `--debug` permet de consulter les logs via un `tail -f .twister/debug.log`

Il est important de mettre un vrai login/pass et surtout pas le user/pwd proposé dans la doc, car twister sera accessible à tout Internet contraitement à votre instance locale. Ensuite il écoute sur l'IP de loopback, on pourrait sans doute mettre celle publique, mais il me parait plus sérieux de mettre en frontal un reverse proxy. Voici ma config basique avec nginx :

/etc/nginx/conf.d/twister.conf

```
upstream twister {
server 127.0.0.1:28332; # Default twister port
}
	
server {
listen       80;
server_name  IP; # IP ou domaine
server_tokens off;     # don't show the version number, a security best practice
	
access_log  /var/log/nginx/twister-proxy-access;
error_log   /var/log/nginx/twister-proxy-error;
	
include proxy.include;
	
 location / {
proxy_redirect     off;
	
proxy_set_header   X-Forwarded-Proto $scheme;
proxy_set_header   Host              $http_host;
proxy_set_header   X-Real-IP         $remote_addr;
	
proxy_pass http://twister;
  }
}
``` 

/etc/nginx/proxy.include

```
proxy_set_header   Host $http_host;
proxy_set_header   X-Real-IP $remote_addr;
proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
proxy_set_header   X-Forwarded-Proto $scheme;
	
client_max_body_size       10m;
client_body_buffer_size    128k;
	
proxy_connect_timeout      90;
proxy_send_timeout         90;
proxy_read_timeout         90;
	
proxy_buffer_size          4k;
proxy_buffers              4 32k;
proxy_busy_buffers_size    64k;
proxy_temp_file_write_size 64k;
```

Une fois connecté à l'interface d'admin vous pouvez soit créer un compte soit vous connectez si vous avez bien sauvegardé la clé secrète du compte.

Pour finir j'ai lancé la génération de blocs pour stresser le matériel et comparer :

```
ARMv7              Current hash rate: 16189
VPS cloud1 OVH     Current hash rate: 38794
Core i5 sous OSX   Current hash rate: 82417
```

Malheureusement twister ne permet pas pour l'instant d'utiliser plusieurs core même si l'option est présente. La génération de bloc sur cet ARM sert un peu à rien, cependant l'interface web de twister et la machine reste très réactive.  
Subsiste un dernier bug, l'instance de twister sur cet ARM chez Online n'affiche aucun avatar, j'essaye de voir si cela proviendrait d'une éventuelle configuration réseau chez Online, le stockage des avatars étant stockés dans la DHT et récupéré de manière [particulière][14] :

> avatar requires fragmentation of IP packets. It is possible (although unlikely) that some router would have problem with that

Une doc un peu plus avancée sur l'hébergement de twister sur un serveur :

[http://wiki.twister.net.co/w/using:howto:twister\_on\_your\_self-host\_server][15]

Malgré sa jeunesse et ses bugs, twister est un excellent outil, amené à évoluer et à devenir une solution crédible de réseau social libre en P2P. Un développeur a publié un outil web qui permet de consulter les messages des utilisateurs stockés dans la DHT : [https://twisterio.com/timeline/fredix][16]

[1]:	http://twister.net.co
[2]:	https://linuxfr.org/news/twister-un-microblog-opensource-p2p
[3]:	http://www.bortzmeyer.org/twister.html
[4]:	http://labs.online.net/#/
[5]:	https://doc.cloud.online.net/advanced/api.html
[6]:	http://philippe.lewin.me/2014/10/14/benchs-specs-online-cloud-preview/
[7]:	http://dotgo.sourcegraph.com/post/99652962343/brad-fitzpatrick-on-the-future-of-the-go-programming
[8]:	https://camlistore.org
[9]:	http://fredix.xyz/2014/10/hugo-syncthing/
[10]:	https://github.com/moovweb/gvm
[11]:	https://ind.ie/pulse/
[12]:	/2014/10/hugo-syncthing/
[13]:	https://github.com/miguelfreitas/twister-core/blob/master/doc/building-on-ubuntu-debian.md
[14]:	https://groups.google.com/d/msg/twister-users/qUom2jL-L9o/ttjoFLGjQEwJ
[15]:	http://wiki.twister.net.co/w/using:howto:twister_on_your_self-host_server
[16]:	https://twisterio.com/timeline/fredix
