+++
date = "2014-11-13T01:03:55+01:00"
draft = false
title = "le libre et l'argent"
tags            = ["opensource"]
+++

Je viens de tomber sur cette publicité pour Firefox de Mozilla via cet article [L’arnaque de la fondation Mozilla][1] , le titre semble plutôt dur limite du troll, je visionne la vidéo et là les bras m'en tombent.

Ok sur le message qui explique que Firefox ne stocke pas les données de ses utilisateurs et donc respecte la  vie privée de ses utilisateurs. C'est cependant faire un raccourci un peu trop rapide puisque le tracage est aussi et surtout sur tous les sites  web infestés de Google Analytic, widget Facebook/Twitter et autres régies publicitaire..

Sur la forme c'est puant, et je suis plutôt d'accord avec l'article pour dire qu'il est "messianique", on y présente des jeunes beaux, les vieux banals dégagez, une bonne pub comparable à celle d'un Total ou Apple, pour un projet qui se veut différent c'est loupé.

Mais le plus énorme à 0:44 : "sans but lucratif". Nous on est propre on ne fait pas d'argent, sous entendu l'argent corrompt, c'est le Mal absolu. Par contre le fait que Mozilla soit financé depuis des années par Google, là ce n'est pas un problème... 

Ce discours est bien celui puant du Libre en général qui n'arrive pas à gérer ses rapports avec l'argent. En résumé soit on est un méchant capitaliste qui veut faire des profits soit on est un gentil bénévole anti-capitaliste. Pas de juste milieu, et pour payer quelques développeurs et ceux qui font tourner la boutique Mozilla, on perçoit les fonds de Google sans sourciller, quelle éthique !

Il serait temps que certains acteurs du Libre murissent un peu. Ce message est digne d'un ado prépubère. Il serait temps d'assumer que l'argent est nécessaire à faire vivre des projets tel que celui là et il serait donc temps pour Mozilla de se trouver un modèle économique respecteux de son éthique. 

Certains dans d'autres domaines l'ont trouvé, par exemple [La ruche qui dit oui][2] un projet qui propose de monter des coopératives de producteurs en direct vers les consommateurs et sous forme de franchise. Sont-ils sans but lucratif ? non, proposent-ils une autre forme de capitalisme en ne passant pas par les gros distributeurs et les dérives écologiques et économiques que cela implique ? oui.

Alors pourquoi dans la high-tech Libre est-on tellement en retard avec des modes de pensées dignes du communisme des années 70 ? Il faut arréter avec le "sans but lucratif", tout le monde a besoin de vivre, même Mozilla qui met bien en avant sa fondation histoire de cacher [Mozilla Corporation][3]. Mais il n'y a pas de honte !

La vraie honte est de ne pas l'assumer, de ne pas être capable de se trouver un modèle économique au point de vivre sous la coupe de Google, ou si ce dernier ne renouvelle pas le contrat de Microsoft bing, peu importe. La vraie honte est d'avoir cette pub, daubée, sur Youtube et même d'y avoir une chaine [firefoxchannel][4] ...  
Développez des services freemium, acceptez les paiements en bitcoin, reprenez en main Thunderbird avec la même énergie que Firefox, inspirez-vous de Google/Apple mais avec l'éthique du Libre, ce ne serait-ce pas mieux, plus logique et éthique, que de dépendre de ces corpos tout en les critiquant ? Stop à l'hypocrisie.

Le vrai problème est qu'il n'y a pas, encore, d'autre projet libre grand public et médiatisé de cette taille, pour essayer de faire autrement, de montrer qu'il est possible de faire autrement, de contruire un autre capitalisme, comme il se contruit actuellement dans d'autres domaines que l'informatique.  

Mais ce n'est qu'une question de temps, car Internet est bien plus que du Web.

[1]:	http://blog-libre.org/post/2014/11/11/larnaque-de-la-fondation-mozilla
[2]:	http://www.laruchequiditoui.fr
[3]:	http://fr.wikipedia.org/wiki/Mozilla_Corporation
[4]:	https://www.youtube.com/user/firefoxchannel