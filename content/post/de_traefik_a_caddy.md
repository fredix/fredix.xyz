---
title: "De traefik à caddy"
date: 2023-02-23T22:33:18+01:00
draft: false
slug: "de_traefik_a_caddy"
---


Ça faisait un moment que je repoussais la migration de mon serveur web [traefik](https://traefik.io/traefik/) 1 vers la version 2. La vieille flemme de reconfigurer le serveur ainsi que chacun de mes services selon la nouvelle nomenclature de la version 2. C'est alors que j'ai pensé à [caddy](https://caddyserver.com), un autre serveur web en Go, et je me suis demandé s'il ne pouvait pas s'interfacer avec mon docker swarm ? Et bien oui !  
Attention je ne dis pas que traefik n'est pas bien, mais ma nouvelle devise étant "pourquoi faire compliqué quand on peut faire simple", je n'ai pas besoin de la complexité de traefik pour mes besoins personnels, donc voici le plugin de caddy pour docker swarm : [caddy-docker-proxy](https://github.com/lucaslorentz/caddy-docker-proxy).


Tout d'abord je créé un network dédié à caddy:

```docker network create --attachable --scope=swarm --driver=overlay caddy-net```

Ensuite je créé un répertoire pour le volume (on peut aussi utiliser docker volume create):

```mkdir /swarm/volumes/caddy```


Le fichier caddy.yml pour lancer la stack caddy dans le swarm :

```yaml
version: "3.7"
services:
  caddy:
    image: lucaslorentz/caddy-docker-proxy:ci-alpine
    networks:
      - caddy-net
    ports:
      - 80:80
      - 443:443
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - /swarm/volumes/caddy:/data
    deploy:
      placement:
        constraints:
          - node.role == manager

      labels:
        caddy.email: user@domain.com

networks:
  caddy-net:
    external: true
```

Je lance le conteneur caddy:

```docker stack deploy --compose-file=caddy.yml caddy```


Le serveur web est prêt à exposer les services docker. Par exemple prenons ce fichier gotify.yml qui va exposer le service [gotify.fredix.xyz](https://gotify.fredix.xyz)

```yaml
version: "3.3"
services:
  gotify:
    image: gotify/server
    env_file: .env.sqlite
    networks:
      - caddy-net
    ports:
      - 80
    volumes:
      - /swarm/volumes/gotify:/app/data
    deploy:
      placement:
        constraints:
          - node.role == manager

      labels:
        caddy: gotify.fredix.xyz
        caddy.reverse_proxy: "{{upstreams 80}}"
#        caddy.tls.ca: https://acme-staging-v02.api.letsencrypt.org/directory
        caddy.tls.ca: https://acme-v02.api.letsencrypt.org/directory

networks:
  caddy-net:
    external: true
```

La configuration pour caddy se résume à 3 labels... \o/ On lance le service et terminé ; caddy va de lui même installer les certificats let's encrypt. A noter que pendant les tests il vaut mieux utiliser l'API staging de letsencrypt.

```docker stack deploy --compose-file=gotify.yml gotify```
