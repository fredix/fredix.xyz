+++
date = "2014-10-09T16:20:25+02:00"
draft = false
title = "J’ai switché sur Mac et j’en suis ravi"
slug = "J’ai switche sur Mac et j’en suis ravi"
tags            = ["mac", "opensource"]
+++

Ca doit faire 1 an que j’ai du utiliser un macbook pro pour le développement d’une application chez mon employeur.
Je n’avais jamais utilisé de Mac, et je regardais cet environnement avec la curiosité et la condescendance du libriste convaincu d’utiliser le seul OS techniquement valable et ouvert.

J’avoue avoir pris une grande claque, il faut en effet utiliser OSX un certain moment pour se rendre compte des années lumière de retard qu’ont nos environnement de bureaux sous Linux.
J’avoue avoir résisté aux sirènes de certains de mes contacts Linuxien/BSDiste qui ont switché depuis longtemps.

Je n’ai pas envie d’énumérer les avantages, bloguer est pour moi en ce moment une contrainte pénible, juste un défouloir personnel.
Cependant je tiens à dire que l’on y trouve des applications d’extrème qualité jamais vu ailleurs. J’écris ce texte depuis [Ulysses][1] un éditeur de texte markdown très bien pensé, présenté, rapide et synchronable via iCloud et Dropbox.
J’utilise également [1password][2] qui permet de gérer ses mots de passes, d’en générer à la volée et donc d’avoir un mot de passe différent pour chaque site web, et synchronisable avec la version iOS, soit via Dropbox/iCloud, soit en wifi local pour les craintifs. Ainsi que [Alfred][3] qui permet de tout piloter au clavier et même de définir des workflows pour  déclencher des tâches.

Ces applications sont chères, surtout quand on vient du monde Linux où tout est gratuit et l’idée de payer même 1 centime une blague. Cependant je suis développeur et j’ai conscience du travail nécessaire à développer une application et j’estime que ce travail doit être rémunéré. Car il fait vivre un développeur indépendant, une TPE ou une petite PME et ceci est pour moi autrement plus important que le libre.
Et oui, jusqu’à preuve du contraire, pratiquement aucun développeurs, sauf quelques exceptions, ne vie d’une application opensource. Et ce n’est pas les développeurs de firefox sponsorisé par Google qui diront le contraire.

Les développeurs qui travaillent chez RedHat, Suse, Canonical ou autres, ne vivent pas directement de leur travail puisque leurs applications n’est pas directement vendu, mais font partie d’un service vendu.

MacOS est à mes yeux le seul environnement qui permet à des développpeurs de vivre de leur passion, de pouvoir vendre directement leur produit tel un artisan, un agriculteur local.
D’une part ils ont le choix de vendre via une coopérative qui prendra un pourcentage (l’appstore) certe élevé, soit directement via leur site web.
Je serais curieux de connaitre les pourcentages de vente de licence de [sublimetext][4] par OS, mais je suis bizarrement prêt à parier que celles pour Linux sont les plus faible.
Rapidement on pourrait en déduire qu’il n’y a pas d’éditeur de texte digne de ce nom sur OSX ce qui expliquerait des ventes plus fortes pour cet OS, or justement par défaut il y a vim et emacs … personne n’oserait dire que ce sont de mauvais éditeur, mais peut etre sont-ils plus plébicité par les admins que les devs ou utilisateur ? mais je vais y revenir.

L’erreur du libre a été d’implémenter en son coeur l’idée que le propriétaire est Mal, malsain, que l’utilisateur est obligatoirement prisonnier. Or c’est souvent faux. Les grandes majorité des applications permettent d’exporter les données utilisateur dans un format ouvert.
La possibilité qu’une application travaille avec des formats ouverts et libres est de loin la meilleure car elle permet de pouvoir vivre de son développement tout en n’enfermant pas ses utilisateurs.

Le problème est là, du haut de leur montagne de morale bien pensante le libriste hacktiviste ne paye pas de logiciel, ca ne lui vient même pas à l’idée. Payer pour du libre, quelle idée, quelle horreur. A la rigueur il fera une obole de temps à temps à des associations qui prèchent la bonne parole sur des services centralisés et propriétaires comme twitter, histoire de se donner bonne conscience.

Au final le développeur d’application native a tout intéret de viser la plateforme Apple et c’est ce qui se passe depuis des années.
Le pendant de tout cela, ou cela mène le libre, c’est une webisation de son environnement, à part quelques outils de bases on est très loin de la richesse que l’on peut trouver sur Mac.

Or en fonçant sur la mode web, où Interet == le web, qui l’on trouve en face ? ChromeOS, du « petit » Google… Dommage les chromebook cartonnent aux US et commencent à avoir du succès en Europe.

Où je veux en venir, c’est très simple. Le monde des logiciels natifs sur desktop est chez Apple, que ce soit sur les devices et le desktop, le monde des logiciels web chez Google, le monde des services chez les 2. Le desktop Linux se trouve donc entre les deux, incapable de faire vivre une communauté de développeurs, il périclite, et au final ne se compose que d’administrateurs systèmes qui n’hésitent pas à [insulter][5] (voir menacer de mort) des devs comme Lennart Poettering développeur de systemd qui a osé coder un logiciel libre qui bouscule leurs petites habitudes.

Mais finalement quoi attendre d’autre d’une communauté composée toujours plus d’administrateurs système, l’utilisateur lambda étant sur windows,osx, chromeOS..

Alors entre la soit disant la cage dorée de Apple, qui me permet d’installer toutes les applications que je veux, libre ou pas, qui peut me permettre d’en vivre, et celle de Linux, pauvre, qui laisse le seul choix d’être soit administrateur soit développeur web, le choix est vite fait. Je regrette une chose c’est de ne pas l’avoir fait il y a des années.

Je ne renie pas l’importance du libre, notamment sur les serveurs et au coeur du réseau mais sur le desktop il ne pourra jamais être cette alternative que j’imaginais, car il est par design défecteux avec GNU/Linux. Un jour peut être cela arrivera avec [haiku OS][6] la phylosophie y étant radicalement différente, notamment par le but de pouvoir lancer des applications BeOS … propriétaire … Quelle horreur…. 




[1]:	http://www.ulyssesapp.com
[2]:	https://agilebits.com/onepassword
[3]:	http://www.alfredapp.com
[4]:	http://www.sublimetext.com/
[5]:	http://www.zdnet.fr/actualites/systemd-et-les-t-du-c-de-la-communaute-open-source-39807395.htm
[6]:	https://www.haiku-os.org