---
title: "Nostr"
date: 2024-03-06T19:00:27+01:00
draft: false
tags: ["nostr","social","bitcoin"]
---

## Présentation
Peut-être avez-vous entendu parler depuis quelques temps d'un nouveau "réseau social" opensource ? Hé non je ne parle pas de [Bluesky](https://bsky.app/) mais bien d'un "réseau social" opensource appelé Nostr, créé fin 2020, mais voici ce que nous dit la [page Wikipedia](https://en.wikipedia.org/wiki/Nostr) :

* Nostr est un protocole opensource décentralisé
* Pensé pour résister à la censure
* [Jack](https://nostr.com/npub1sg6plzptd64u62a878hep2kev88swjh3tw00gjsfl8f237lmu63q0uf63m) Dorsey, co-fondateur de Twitter, a financé le créateur de Nostr, [Fiatjaf](https://fiatjaf.com/nostr.html), pour [14 bitcoins](https://twitter.com/jack/status/1603535971114487816?lang=en). Il est d'ailleurs amusant de constater que Jack est aussi un fondateur de [Bluesky](https://en.wikipedia.org/wiki/Bluesky_(social_network)) qui se veut aussi décentralisé et ouvert avec son [AT Protocol](https://atproto.com)

Bien que récent, Nostr possède un écosystème très riche avec une multitude d'outils et de services basés dessus. Voici la page de description du projet par son créateur : [nostr - Notes and Other Stuff Transmitted by Relays](https://fiatjaf.com/nostr.html).  
On y lit au début : "it does not rely on P2P techniques, therefore it works." un petit pique sur le P2P, je ne sais pas si c'est fait exprès mais je me souviens il y a longtemps d'une tentative de réseau social opensource en P2P qui utilisait bittorrent et une blockchain, [Twister](http://twister.net.co). Je l'avais testé mais c'était assez complexe à mettre en place à l'époque, puis il a été abandonné par son auteur.

Nostr n'est donc pas un réseau social mais un protocole utilisé par des logiciels client (mobile, web, desktop) et des relais. Pour l'instant il est plus connu comme un usage de micro-blogging mais voyons ça en détail.


## Un compte unique

Il n'y a pas de site web officiel à utiliser comme Twitter/X ou une grosse instance avec un grand nombre d'utilisateurs comme [Mastodon.social](https://mastodon.social).  
Les clients web les plus connus actuellement sont [Snort](https://snort.social), [Primal](https://primal.net), [Iris](https://iris.to), [Coracle](https://coracle.social). Sur l'un d'entre eux vous pourrez créer un compte Nostr et vous pourrez par la suite utiliser ce compte sur n'importe lesquels de ces sites, cela fonctionnera.  
 **Attention** cependant à bien comprendre le fonctionnement. En effet nul besoin d'email ou numéro de téléphone à fournir, la création d'un compte est immédiate et génère une clé publique et une clé privée. La clé publique est sous ce format `npub1vwldp9k95xxe9mgnzakykpmhmazj5q2veldt6jvju2dvrfkxvlaq6dzfhz` et la clé privé lui ressemble mais commence par `nsec...`. Attention, il y a plusieurs choses à retenir :

* ne jamais fournir à qui que ce soit la clé privé, et ne jamais la diffuser.
* si quelqu'un accède à votre clé privée, votre compte est perdu car il est impossible de la changer. Il faudra forcément créer un nouveau compte.
* les clés publique et privé ne pouvant être mémorisé, il est fortement conseillé de les noter dans un logiciel de gestion de mot de passe, type [bitwarden](https://bitwarden.com) ou [1password](https://1password.com/fr). 
> Une appartée à ce sujet : L'utilisation d'un gestionnaire de mot de passe n'est plus de nos jours un outil de geek. C'est bien un outil indispensable pour tout un chacun, et avec les piratages récurrents des services web il n'est plus concevable d'utiliser un même mot de passe partout, qui plus est faible. Voici une courte présentation à ce sujet, [Gestionnaires de mots de passe](/2022/09/gestionnaires-de-mots-de-passe/)
> 	
* Sur certains site vous pouvez vous connecter en donnant uniquement une clé publique, la votre la mienne ou celle de n'importe qui. Dans ce cas vous aurez un accès en lecture seule au compte, donc sans pouvoir poster ni modifier le profil ou lire les messages privés qui sont chiffrés.
* Pour plus de sécurité, vous pouvez installer une extension dans votre navigateur, comme celle de [Alby](https://getalby.com), grâce à laquelle vous n'avez plus besoin de donner votre clé privée sur un site web Nostr. Vous lui donnerez alors les droits, via l'extension, de signer vos messages, donc de poster, et de modifier vos informations. Cela nécessite de créer un compte sur Alby.


## Les NIPs

Les [NIPs](https://github.com/nostr-protocol/nips) sont comme des RFC mais pour Nostr. Ils ajoutent des fonctionnalités facultatives, à implémenter côté relai ou client et seule la [01](https://github.com/nostr-protocol/nips/blob/master/01.md) est pour l'instant obligatoire car elle sert de base au protocole en transmettant les Notes.

## Fonctionnement

Nostr est composé de logiciels client (web ou mobile) et de relais sur des serveurs. Le client utilise votre clé publique et privé pour signer vos messages et les envoyer vers des relais. Les relais stockent simplement les messages reçus par les clients, et les retransmet aux autres clients connectés. De fait les clients connectés à un même relai peuvent recevoir les messages de tous les utilisateurs utilisant ce relai.  
Mais si 2 clients n'ont pas au moins un relai en commun, alors les 2 utilisateurs ne verront pas les messages de l'autre. C'est pourquoi les clients se connectent à plusieurs relais, afin d'une part d'éviter la censure qu'il pourrait y avoir sur un relai, d'autre part afin de transmettre au plus grand nombre ses messages, enfin d'augmenter la résilience en cas de panne.  
Par défaut un client va se connecter à quelques relais, cependant vous pouvez en ajouter en utilisant le site [nostr.watch](https://nostr.watch) qui en liste un grand nombre. Pour ma part je fais tourner un relai public ([strfry](https://codeberg.org/fredix/nomad/src/branch/main/strfry.hcl)) sur mon infra [Nomad](/tags/nomad/), que vous pouvez bien sûr utiliser : **wss://relay.nostromo.social**  
A savoir que des relais sont à accès payant, soit en écriture, soit lecture/écriture, afin de supprimer le spam et payer les frais de fonctionnement, exemple avec ce relai [Nostr.lc](https://nostr.lc) dont l'usage est payant en Satoshi ([Sat](https://www.cointribune.com/quest-ce-quun-satoshi-la-plus-petite-unite-de-mesure-du-bitcoin-btc-est-un-terme-de-plus-en-plus-utilise/)). A ce sujet j'ouvre une rapide parenthèse vers l'implémentation de Bitcoin dans Nostr.

## Bitcoin

Nostr commence à avoir un certain succès dans le monde de la crypto suite à l'implémentation de la [NIP-57](https://github.com/nostr-protocol/nips/blob/master/57.md) sur des clients et relais, appelée [Zaps](https://nostr.how/en/zaps). En résumé on peut envoyer des Tips (pourboires) à un autre utilisateur en Satoshi (Sat) qui est la plus petite unité de mesure du Bitcoin (1 bitcoin = 100 000 000 satoshis, 1 satoshi = 0.00000001 bitcoin). C'est donc une unité de mesure adaptée à de faibles montants, mais cela ne s'arrête pas là.  
Utiliser la blockchain est très lent, aussi la NIP-57 utilise une surcouche au réseau Bitcoin, appelé Lightning network, qui offre des transactions quasiment instantanées. Je ne vais pas décrire ici le fonctionnement mais je vous conseille cet excellent site qui explique le [Lightning Network](https://lightning.fr).  
Pour envoyer et recevoir des Sats il faut un portefeuille (wallet) qui permet de les recevoir via une adresse. [Alby](https://getalby.com) et certains clients web proposent cela, ou si vous êtes courageux vous pouvez installer votre propre noeud Lightning ([LND](https://github.com/lightningnetwork/lnd)).  
Enfin je vous conseille d'installer une application mobile connectée au noeud Lightning que vous utilisez pour consulter vos transactions et en faire, exemple [Wallet of Satoshi](https://www.walletofsatoshi.com) ou [Zeus](https://zeusln.com), mais cela devient un tout autre sujet.  
Grâce aux Zaps, Nostr permet de financer du contenu ce qui en fait un écosystème monétisable totalement autonome et décentralisé. Mais quel genre de contenu ?

## Les applications sur Nostr

Les applications les plus connues sont celles cités précédement permettant le micro-blogging à la Twitter, mais Nostr permet d'autres usages tel que :

* [Stacker News](https://stacker.news/) une alternative à Hacker news
* [Zap.Stream](https://zap.stream/) une alternative à Twitch
* [Habla.news](https://habla.news) une alternative à Medium
* [Nostrnests](https://nostrnests.com) un espace de chat et discussion audio

Elles sont toutes utilisable avec votre compte Nostr, si vous avez installé l'extension Alby vous vous connectez en quelques clics sans avoir besoin de créer un nouveau compte sur ces services.  
Voici une liste plus complète ici : [Nostr Apps](https://www.nostrapps.com)


## Les clients mobiles

quelques clients mobiles

* [Damus](https://damus.io) le plus connu pour Apple
* [Amethyst](https://play.google.com/store/apps/details?id=com.vitorpamplona.amethyst) pour Android
* [Primal](https://play.google.com/store/apps/details?id=net.primal.android) pour Android

## Conclusion

Nostr est un écosystème en plein développement et prometteur. Certes, comprendre Nostr peut sembler un peu plus compliqué que Twitter ou Facebook, mais c'est un faible prix à payer pour le respect de sa vie privée et sa liberté de communiquer.  
[Mon profil Nostr](https://nostr.com/npub1vwldp9k95xxe9mgnzakykpmhmazj5q2veldt6jvju2dvrfkxvlaq6dzfhz)

## Quelques liens

* [Nostr les bases en 5 minutes: habla.news](https://habla.news/a/naddr1qqxnzd3cxyunqvfhxy6rvwfjqyt8wumn8ghj7mn0wd68ytnfdehhxarp9e3kxtcpremhxue69uhkummnw3ez6ur4vgh8wetvd3hhyer9wghxuet59uq37amnwvaz7tmwdaehgu3wwdjhg6rxdae8qunfweskx7fwvdhk6tcpzdmhxue69uhk7enxvd5xz6tw9ec82c30qythwumn8ghj7mn0wd68ytnjv4kxz7t9wgh8xef0qgsf54mtm9sy0kvgf2f6hv4mq3rqj2u6r0rh8fzjzsl873j72wrhtaqrqsqqqa286k3et4)
* [Nostr.com](https://nostr.com)
* [Nostr.how](https://nostr.how)
* [Nostr.net](https://nostr.net)
* [Nostr.fr](https://nostr.fr)
* [Nostr.watch](https://nostr.watch)
* [Nostr : le futur des réseaux sociaux sera décentralisé](https://www.logiste.fr/nostr.html)
* [Nostr : cryptographie VS censure](https://hellotoken.io/nostr/)
* [nostr-protocol](https://github.com/nostr-protocol/nostr) le projet sur github


(Ce texte a été écrit avec [Ghostwriter2](https://ghostwriter.kde.org/fr/))