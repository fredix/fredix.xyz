---
title: "Asus Zenbook"
slug: "asus-zenbook"
date: 2024-03-12T20:47:36+01:00
draft: false
tags: ["linux","laptop","manjaro"]
---

Je suis l'heureux possesseur d'un laptop Asus Zenbook, heureux car depuis quelques heures le dernier point noir vient de disparaître, à savoir le son des haut parleur.  
L'été dernier je recherchais un ultra book full AMD, surtout pour avoir le support hors de la boite de la carte graphique (terminé pour moi les drivers proprio Nvidia 🖕).  
Je m'étais dirigé vers un lenovo [Thinkpad Z13 Gen 1](https://www.lenovo.com/fr/fr/p/laptops/thinkpad/thinkpadz/thinkpad-z13-(13-inch-amd)/21d2cto1wwfr1) qui sont plutôt bien réputé, et après l'avoir reconfiguré pour augmenter la RAM et le disque je passais commande. Quelle ne fût pas ma surprise de voir celle-ci annulée une semaine après et me voir remboursé sans plus de précision que :
>Malheureusement, nous ne sommes pas en mesure de traiter tout ou partie de votre commande. Les articles mentionnés ci-dessous ont été annulés et ils ne vous seront pas facturés.

Bref tant pis pour Lenovo et les Thinkpad car je voulais ce modèle. Aussi après moulte recherches et vidéos, j'ai choisi un Asus Zenbook 15 OLED, qui est dans leur haut de gamme. Il était uniquement disponible sur Amazon :( il n'est plus en vente mais si vous êtes intéressé ce modèle non OLED s'en rapproche [Asus Zenbook 15 UM3504DA-BN414W](https://www.amazon.fr/Asus-Zenbook-UM3504DA-BN414W-Portable-Processor/dp/B07MHFTTWP).  
J'ai donc payé ma dîme à Microsoft 🤮 et viré leur spyware "OS" dès reception de la bête. Avec Manjaro tout a fonctionné hormis la sortie son des haut parleur. Visiblement le matériel audio était trop récent et à part bidouiller le kernel il suffisait d'attendre un peu. Confirmé par un utilisateur de ArchLinux dans ce post sur Reddit [No Sound on new ASUS Zenbook 15 OLED Laptop](https://www.reddit.com/r/archlinux/comments/18izrrg/no_sound_on_new_asus_zenbook_15_oled_laptop/) : 

>After installing Linux kernel 6.7 - the sound out of the speakers actually works now!!!

Manjaro vient d'ajouter le kernel 6.7.7, il m'a suffit de lancer le [gestionnaire de paramètres de Manjaro](/2022/08/manjaro/), installer ce kernel et rebooter 🥳

Pour info voici le chipset audio en question

```sh
inxi -A
Audio:
  Device-1: AMD Rembrandt Radeon High Definition Audio driver: snd_hda_intel
  Device-2: AMD ACP/ACP3X/ACP6x Audio Coprocessor driver: snd_pci_acp6x
  Device-3: AMD Family 17h/19h HD Audio driver: snd_hda_intel
  API: ALSA v: k6.7.7-1-MANJARO status: kernel-api
  Server-1: PulseAudio v: 17.0 status: active
```

Concernant ce portable je suis conquis, le seul bémol étant l'écran brillant plutôt que mat. Précédemment j'avais un Asus Vivobook mais ce Zenbook tout en alu est de bien meilleur qualité. Ayant eu dans le passé un Macbook pro, son trackpad est tout à fait comparable ce qui est rare ! lisse et grand il n'accroche pas, je n'ai **pas besoin d'une souris** et GNOME gère jusqu'à 3 doigts en simultané pour se déplacer dans les bureaux 🤙.  
A configuration identique cet Asus est moins cher qu'un Dell XPS, un Thinkpad ou un Macbook pro. Certes n'ayant pas un processeur ARM il n'a pas l'autonomie des Mac, et il faudra sans doute attendre quelques années une alternative sérieuse et abordable avec Qualcomm à moins que les fondeurs ne se dirigent vers l'architecture RISC-V mais c'est une autre histoire.

```sh
neofetch
██████████████████  ████████   fred@zenbook
██████████████████  ████████   ------------
██████████████████  ████████   OS: Manjaro Linux x86_64
██████████████████  ████████   Host: Zenbook 15 UM3504DA_UM3504DA 1.0
████████            ████████   Kernel: 6.7.7-1-MANJARO
████████  ████████  ████████   Uptime: 45 mins
████████  ████████  ████████   Packages: 1653 (pacman)
████████  ████████  ████████   Shell: zsh 5.9
████████  ████████  ████████   Resolution: 2880x1620
████████  ████████  ████████   DE: GNOME 45.4
████████  ████████  ████████   WM: Mutter
████████  ████████  ████████   WM Theme: Custom-Accent-Colors
████████  ████████  ████████   Theme: Adw-dark [GTK2/3]
████████  ████████  ████████   Icons: Papirus-Dark-Maia [GTK2/3]
                               Terminal: WarpTerminal
                               CPU: AMD Ryzen 7 7735U with Radeon Graphics (16) @ 4.819GHz
                               GPU: AMD ATI Radeon 680M
                               Memory: 8033MiB / 31336MiB
```

![asus zenbook](/images/asus.png)

(Ce texte a été écrit avec [Ghostwriter](https://ghostwriter.kde.org/fr/))
