+++
date = "2014-01-10T15:27:57+00:00"
draft = false
slug = "le-revenu-de-base"
title = "le revenu de base"
tags            = ["autre"]
+++

Cela fait longtemps comme de nombreux français que je m'intéresse à l'emploi dans notre pays mais aussi aux développement des entreprises qui est lié à l'emploi. En effet on comprend vite lorsqu'on grandi et encore plus lorsqu'on a des enfants, que les 2 choses indispensable dans la vie d'un citoyen français sont une assurance chômage et un emploi (public ou privé).

Des pays comme les états unis se passent très bien de l'assurance chômage, cependant l'activité de leurs nombreuses entreprises n'a rien à voir avec celles de la France. De plus la facilité pour licencier n'apporte pas de crainte à l'embauche, mais je vais y revenir.



Pour commencer je vous conseille de lire cet article court [Comprendre le revenu de base][1] ou celui plus complet de wikipedia [http://fr.wikipedia.org/wiki/Revenu\_de\_base][2]


Pour résumer grossièrement, d'autant plus que cela diffère selon les partisans, le revenu de base permettrait de percevoir une allocation mensuelle sans contrepartie si l'on rempli les conditions suivantes :



 
  * être de nationalité française

	 
  * ne pas travailler

	 
  * travailler mais auquel cas le revenu de base serait diminué au prorata de ses revenus ou supprimé


Et surtout il n'y a pas d'exigence à la recherche d'un emploi pour la percevoir ... à vie. Ce sont à priori les principes de base de ce revenu, le montant de l'allocation peut changer en fonction des partisans, au minima cela reviendrait à une allocation permettant de payer un petit loyer dans une petite ville et à se nourrir.

C'est une idée tellement moderne ou extrême selon les points de vu, que ni la gauche, ni les partis encore plus à gauche n'ont osé la proposer dans leur programme aux présidentielles, hormis [Dominique de Villepin][3] (sic) (850€/mois):

"_La mesure sans doute la plus spectaculaire de son programme (et la plus décriée à l'intérieur même de son propre parti[30][4]) est la création d'un « revenu citoyen[31][5] », qui serait versé à chaque Français selon un principe proche de celui de l'[allocation universelle][6]_"

Mon point de vu est que cette allocation pourrait apporter un changement radical des rapports avec le travail. En effet nul besoin de prendre n'importe quel travail pour survivre, l'Entreprise en bénéficierait en ayant des candidats réellement intéressé par le poste et non plus de personnes souhaitant juste recotiser pour les ASSEDIC. Les emplois ingrats et pénibles seraient de fait mieux payé. La disparition des multiples aides remplacées par ce revenu supprimerait les couts administratif et la mobilisation de nombreux fonctionnaires pour les gérer.

Enfin et surtout, les citoyens désireux de créer leur entreprise pourraient enfin avoir le temps de la préparer, y réfléchir, faire les études de marchés, et les développements (code pour une startup informatique) pour la création du produit. L'Auvergne l'a d'ailleurs très bien compris puisqu'elle propose un SMIC pendant 6 mois à toute startup qui se lancerait dans cette région : [http://www.newdealnewideas.fr][7]

C'est là que j'y vois le plus grand intérêt. La seule chose qui peut réduire le chômage en France est la création de micro-entreprise/TPE. L'âge des grosses industries française qui font vivre des milliers de personnes est révolu depuis 30 ans. Les industries sont délocalisées dans des pays où la main d'oeuvre ne coute rien en salaire et en charges. Par contre faciliter la création de petites entreprises, que cela soit des salons de coiffure ou des startups dans un garage, oui le revenu de base peut faire exploser cela. **Il est faux de croire que les gens qui toucheraient ce revenu passerait leur journée devant la TV car il n'y a rien de plus plaisant de faire un travail qui plait, et encore mieux de créer son propre emploi.**

Concernant le financement de ce revenu il a bien entendu un cout. C'est bien sûr l'argument en sa défaveur. Face à une idée aussi extrême qu'est le revenu de base, posons en face une autre idée tout aussi extrême, afin que les deux ensemble apportent un équilibre à notre société. Simplifions drastiquement la création d'entreprise, supprimons d'office les charges pendant les 3 premières années d'une entreprise de moins de 5 personnes, et surtout **autorisons le licenciement sans justificatif à toutes les entreprises de moins de 50 personnes ** en conservant malgré tout un délai de préavis pourquoi pas fluctuant en fonction de l’ancienneté.

Le corolaire de ces 2 idées réunies, serait qu'il n'y aurait plus de touriste pour postuler à des emplois, il n'y aurait plus de crainte à embaucher puisqu'il n'y aurait pas de problème pour licencier, il n'y aurait pas de crainte à être licencié puisqu'il y aurait le revenu de base pour vivre, humblement certes mais vivre.

Et pour ceux souhaitant un revenu de base plus élevé sur une période, il pourrait dépendre d'une cotisation facultative prélevé sur son salaire comme actuellement.

A mon sens le revenu de base ne peut être crédible qu'avec cette idée en face. Dans une société les appliquant j'y vois une explosion de création d'entreprises, j'y vois une explosion des recrutements et donc une diminution du chômage, chose qui n'arrivera jamais dans le consensus actuel qu'il soit à Droite ou à Gauche.

Je crois malheureusement qu'aucun parti n'aura le courage de proposer des idées aussi opposées. Nos partis mettent toujours face à face les employés contre les employeurs, que cela soit les gentils employés contre les méchants patrons pour la Gauche, ou les gentils patrons et les méchants employés syndiqués pour la Droite.

Enfin tant que la culture du business ne sera pas intégré dans les programmes scolaire dès le collège, la France restera dans une culture anti patron, anti réussite mais aussi anti « échec », complètement dépassé du monde qui nous entoure.

[1]:	http://revenudebase.info/comprendre-le-revenu-de-base/
[2]:	http://fr.wikipedia.org/wiki/Revenu_de_base
[3]:	http://fr.wikipedia.org/wiki/Dominique_de_Villepin
[4]:	http://fr.wikipedia.org/wiki/Dominique_de_Villepin#cite_note-30
[5]:	http://fr.wikipedia.org/wiki/Dominique_de_Villepin#cite_note-31
[6]:	http://fr.wikipedia.org/wiki/Allocation_universelle
[7]:	http://www.newdealnewideas.fr "www.newdealnewideas.fr"