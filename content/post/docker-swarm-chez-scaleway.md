---
title: "Docker swarm chez Scaleway"
date: 2018-12-28T22:19:24+01:00
tags: ["docker","scaleway"]
slug: "docker-swarm-chez-scaleway"
draft: false
---

Cet article a pour but de présenter la préparation à la mise en place de docker swarm chez l'hébergeur Scaleway.


## Contexte

Pour diverses raisons j'apprécie beaucoup Docker et même si je suis loin d'en connaitre encore toutes les subtilités il me permet de gérer quelques services comme mon blog ou un proxy irc. Parmis les solutions d'hébergement j'ai fini par me rapprocher de Scaleway car ils étaient (sont?) les premiers fournisseur de serveur baremetal en architecture ARM puis ARM64. Concrètement pouvoir bénéficier d'un dédié quad core avec 2Go de RAM pour 3€/mois est un net avantage aux solutions virtualisé et le support des principales distributions Linux (Ubuntu, Centos, Debian) en est la touche finale.  
Bien sûr Docker fonctionne très bien sur ARM(64).


## Contraintes

### Docker

Docker propose des images toutes faites via son hub : https://hub.docker.com/ cependant par défaut les images sont contruites pour les architectures x86, elles ne fonctionneront pas sur un serveur ARM. Il faut se diriger vers ce dépôt dédié à l'ARM https://hub.docker.com/u/arm64v8, on y trouvera les principales distributions ainsi que les logiciels les plus polulaires, ils pourront servir de base à la construction de vos propres images.

### Scaleway

Scaleway a ceci de particulier c'est qu'un server baremetal ou VPS qui n'a pas d'IP public associé n'aura pas d'accès à Internet. La solution de coller à chaqu'un de ses serveurs une IP publique risque de couter cher et il n'y a aucune raison qu'un serveur de base de données en ait une.  
Bref les serveurs ne pouvant sortir cela empèchera les mises à jour de l'OS et le téléchargement d'image Docker. La solution indiquée dans les forums est d'utiliser un proxy http. Ainsi un serveur dédié possédant une IP publique aura une instance d'un proxy squid ce qui permettra aux autres de sortir en http/https ; de plus il fera office de machine de rebond ssh pour accéder à chaque serveur. 


## Infra
### proxy

le serveur proxy est basé sur un ARM64-2G, une Centos et une IP publique. le service squid est installé, sa configuration est la suivante :

cat /etc/squid/squid.conf
```
acl localnet src 10.0.0.0/8     # RFC1918 possible internal network
acl localnet src 172.16.0.0/12  # RFC1918 possible internal network
acl localnet src 192.168.0.0/16 # RFC1918 possible internal network
acl localnet src fc00::/7       # RFC 4193 local private network range
acl localnet src fe80::/10      # RFC 4291 link-local (directly plugged) machines
	
auth_param basic program /usr/lib/squid/basic_ncsa_auth /etc/squid/users
auth_param basic realm proxy
acl authenticated proxy_auth REQUIRED
http_access allow authenticated

# Example rule allowing access from your local networks.
# Adapt localnet in the ACL section to list your (internal) IP networks
# from where browsing should be allowed
http_access allow localnet
http_access allow localhost

# And finally deny all other access to this proxy
http_access deny all

# Choose the port you want. Below we set it to default 3128.
http_port 3128
```

le fichier /etc/squid/users est créé via la commande système : 

```sh
htpasswd -c /etc/squid/users fredix
```

### node

sur les autres serveurs ou node, les variables d'environnement suivantes sont définies dans le fichier /etc/environment :

```env
export http_proxy=http://fredix:PASS@192.168.254.10:3128
export https_proxy=http://fredix:PASS@192.168.254.10:3128
```

L'ip indiquée correspond à l'IP du VPN affectée au proxy. Comme décrit dans un précédent article [auto-hebergement-hybride](https://fredix.xyz/2017/05/auto-hebergement-hybride/), les serveurs communiquent entre eux via vpncloud, l'IP locale fournie par scaleway étant suceptible de changer en cas de reboot.

Cette configuration suffit pour mettre à jour le node ou faire un wget, mais Docker ne prendra pas en compte ces variables.

### docker serveur

Sur tous les noeuds docker il faut créer le fichier suivant /etc/systemd/system/docker.service.d/http-proxy.conf contenant :

```toml
[Service]
Environment="HTTP_PROXY=http://fredix:PASS@192.168.254.10:3128" 
Environment="HTTPS_PROXY=http://fredix:PASS@192.168.254.10:3128"
Environment="NO_PROXY=localhost,127.0.0.1,192.168.254.1,192.168.254.2,192.168.254.3"
```

Puis redémarer le service Docker. Cette configuration permettra au service de télécharger les images docker en passant par notre proxy.

### docker client

le client docker peut nécessiter des arguments afin de le forcer à utiliser le serveur squid. Exemple :

```sh
docker build -f Dockerfile.arm64v8 -t fredix/arm64v8-traefik:1.7.6 --build-arg http_proxy="http://fredix:PASS@192.168.254.10:3128" --build-arg https_proxy="http://fredix:PASS@192.168.254.10:3128" .
```

On build ici une image, or dans le dockerfile il y a une mise à jour de l'OS ; les arguments permettent au conteneur créé par le docker build de sortir par notre squid et autoriser la mise à jour.

### ssh

voici la config du client ssh qui permet de se connecter sur chaque node en passant par le serveur proxy :

cat /home/fredix/.ssh/config

``` 
Host node1
HostName node1
User root
ProxyCommand ssh root@IP_PUBLIC_PROXY nc %h %p

Host node2
HostName node2
User root
ProxyCommand ssh root@IP_PUBLIC_PROXY nc %h %p

Host node3
HostName node3
User root
ProxyCommand ssh root@IP_PUBLIC_PROXY nc %h %p

Host proxy
HostName IP_PUBLIC_PROXY
User root
```
