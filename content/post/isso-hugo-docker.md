---
tags: ["isso","docker","hugo"]
categories: ["devops"]
title: "Isso un gestionnaire de commentaire avec Hugo et Docker"
date: 2017-11-11T13:58:49+01:00
slug: "isso-hugo-docker"
draft: false
---

Suite à cette présentation de [isso](https://posativ.org/isso/) ( [Installation d'une alternative open-source à Disqus](https://blog.ston3o.me/installation-alternative-open-source-disqus/)) j'ai décidé de le mettre en place sur Hugo et docker swarm.

J'ai pour cela créé un dockerfile de isso afin qu'il tourne sur mon infra ARM64 : https://github.com/fredix/dockerfile/tree/master/isso
. Le fichier entrypoint.sh est directement pris de ce dépôt git : https://github.com/SISheogorath/isso-docker il faut ensuite le modifier afin de préciser les variables host et name.

Dockerfile.arm64
```dockerfile
FROM arm64v8/python:3.6
RUN pip install isso && mkdir /data
COPY entrypoint.sh /entrypoint.sh
HEALTHCHECK CMD ["wget", "-qO", "/dev/null", "http://127.0.0.1/js/embed.min.js"]
ENTRYPOINT ["/entrypoint.sh"]
```

entrypoint.sh
```sh
#!/bin/sh
    
cat >/isso.conf <<EOF
[general]
dbpath = /data/isso.db
name = ${ISSO_NAME:-comments.fredix.xyz}
host = ${ISSO_HOST:-https://fredix.xyz}
max-age = ${ISSO_MAX_AGE:-30m}
    
[server]
listen = http://0.0.0.0:80
    
[guard]
enabled = true
ratelimit = 2
direct-reply = 3
reply-to-self = ${ISSO_REPLY_TO_SELF:-false}
require-email = ${ISSO_REQUIRE_EMAIL:-false}
require-author = ${ISSO_REQUIRE_AUTHOR:-false}
    
EOF
    
exec isso -c /isso.conf "$@"
```

Sur votre serveur ARM64 :
```sh
docker build -f Dockerfile.arm64 -t fredix/arm64v8-isso .
docker push fredix/arm64v8-isso
```

Voici le yaml nécessaire à lancer la stack docker et le launcher : https://github.com/fredix/swarm/tree/master/hugo.


hugo-arm64v8.yml
```yaml
    version: '3'
    services:
      hugo:
        image: fredix/arm64v8-blog
        restart: always
        ports:
          - 1313
        networks:
          - traefik-net
        deploy:
          placement:
            constraints:
    #          - node.labels.location == cloud-arm64
              - node.role == manager
          labels:
            - "traefik.port=1313"
            - "traefik.docker.network=traefik-net"
            - "traefik.frontend.rule=Host:fredix.xyz,www.fredix.xyz"
    
      isso:
        image: fredix/arm64v8-isso
        restart: always
        ports:
          - 80
        volumes:
          - /docker_volumes/isso:/data
        networks:
          - traefik-net
        deploy:
          placement:
            constraints:
    #          - node.labels.location == cloud-arm64
              - node.role == manager
          labels:
            - "traefik.port=80"
            - "traefik.docker.network=traefik-net"
            - "traefik.frontend.rule=Host:isso.fredix.xyz"
    
    networks:
      traefik-net:
        external: true
```


Il faut penser à créer le répertoire /docker_volumes/isso ou le modifier pour que isso puisse stocker sa base sqlite.
    
Pour finir il est nécessaire de modifier le thème de votre blog hugo afin qu'il ajoute les scripts de gestion des commentaires. Dans le répertoire de votre thème il faut ajouter le fichier suivant :

votretheme/layouts/partials/isso.html
```html
<script data-isso="//isso.fredix.xyz/" src="//isso.fredix.xyz/js/embed.min.js"></script>
<section id="isso-thread"></section>
```

puis ajouter ce nouveau partial dans le fichier votretheme/layouts/post/single.html 

```html
    {{ partial "header.html" . }}
    
    <div class="header">
      <h1>{{ .Title }}</h1>
      <h2>{{ .Description }}</h2>
    </div>
    <div class="content">
    
      {{ partial "post_meta.html" . }}
    
      {{ .Content }}
    
      {{ partial "prev_next_post.html" . }}
    
      {{ partial "isso.html" . }}
    
    
    </div>
    
    {{ partial "footer.html" . }}
```

 On peut maintenant lancer la stack :

```sh
docker stack deploy --compose-file=hugo-arm64v8.yml hugo-arm64
```

En cas de mise à mise à jour de votre image il suffit de relancer docker stack deploy pour qu'il la télécharge et mette à jour le conteneur. Si vous utiliser drone.io voici le pipeline dans lequel j'ai remplacé docker service update par docker stack deploy :

```yaml
platform: linux/arm64
    
clone:
  default:
    image: plugins/git:linux-arm64
    depth: 50
    
pipeline:
  publish:
    image: plugins/docker:linux-arm64
    repo: fredix/arm64v8-blog
    tags: latest
    dockerfile: Dockerfile.arm64
    secrets: [ docker_username, docker_password ]
  ssh:
    image: fredix/arm64v8-alpine-drone-ssh
    host: 192.168.254.1
    port: 22
    username: drone
    volumes:
      - /docker_volumes/drone_agent/drone.key:/root/ssh/drone.key
    key_path: /root/ssh/drone.key
    script:
#      - "sudo docker service update --image fredix/arm64v8-blog hugo-arm64"
      - "sudo docker stack deploy --compose-file=/root/scripts/swarm/hugo/hugo-arm64v8.yml hugo-arm64"
    when:
      status: success
  telegram:
    image: fredix/arm64v8-alpine-drone-telegram
    token: $PLUGIN_TOKEN
    to: $PLUGIN_TO
    secrets: [ plugin_token, plugin_to ]
    message: >
      {{#success build.status}}
        build {{build.number}} succeeded on {{repo.name}}. Good job {{build.author}}  {{build.link}}
      {{else}}
        build {{build.number}} failed on {{repo.name}}. Fix me please {{build.author}}  {{build.link}}
      {{/success}}
    when:
      status: [ success, failure ]
```
