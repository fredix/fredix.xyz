---
tags: ["drone","docker","arm"]
categories: ["devops"]
title: "Drone 1"
slug: "drone-1"
date: 2019-05-17T23:32:52+02:00
draft: false
---

Cet article présente succintement la nouvelle version de [Drone 1](https://blog.drone.io/drone-1/) et les changements de configuration. Pour rappel Drone est un outil d'intégration et de déploiement continu.

Tout d'abord l'interface web a bien évoluée, elle est très clair et agréable.

![drone1_1.png](/images/drone1_1.png)

Un pipeline peut maintenant être relancé depuis l'interface et le lien view source permet de pointer vers le commit qui l'a déclenché.

![drone1_2.png](/images/drone1_2.png)


la syntaxe du fichier pipeline .drone.yml a changé, elle se présente maintenant sous cette forme :


```yaml
  kind: pipeline
  name: default

  platform:
    os: linux
    arch: arm64

  clone:
    depth: 50

  steps:
  - name: docker
    image: plugins/docker:linux-arm64
    settings:
      repo: fredix/arm64v8-blog
      tags: latest
      dockerfile: Dockerfile.arm64
      username:
        from_secret: docker_username
      password:
        from_secret: docker_password

  - name: ssh
    image: fredix/arm64v8-alpine-drone-ssh
    volumes:
    - name: key
      path: /root/ssh/drone.key
    settings:
      host: 192.168.254.1
      port: 22
      username: drone
      key_path: /root/ssh/drone.key
      script:
  #      - "sudo docker service update --image fredix/arm64v8-blog hugo-arm64"
        - "sudo docker stack deploy --compose-file=/root/scripts/swarm/hugo/hugo-arm64v8.yml hugo-arm64"
    when:
      status: success

  - name: telegram
    image: fredix/arm64v8-alpine-drone-telegram
    settings:
      token:
        from_secret: plugin_token
      to:
        from_secret: plugin_to
      message: >
        {{#success build.status}}
          build {{build.number}} succeeded on {{repo.name}}. Good job {{build.author}}  {{build.link}}
        {{else}}
          build {{build.number}} failed on {{repo.name}}. Fix me please {{build.author}}  {{build.link}}
        {{/success}}
    when:
      status: [ success, failure ]

  volumes:
  - name: key
    host:
      path: /docker_volumes/drone_agent/drone.key

```
Le docker compose a également changé car Drone supporte maintenant officiellement [l'architecture ARM](https://blog.drone.io/drone-announces-official-support-for-arm/)

```yaml
version: '3'
services:
  drone-server:
    image: drone/drone:linux-arm64
    restart: always
    env_file: .env.production-server
    ports:
      - 80
      - 443
    volumes:
      - /docker_volumes/drone_server:/data
    networks:
      - drone-infra
      - traefik-net
    deploy:
      placement:
        constraints:
          - node.labels.location == cloud-arm64
      labels:
        - "traefik.port=80"
        - "traefik.docker.network=traefik-net"
        - "traefik.frontend.rule=Host:drone.fredix.xyz"

  drone-agent:
    image: drone/agent:linux-arm64
    restart: always
    env_file: .env.production-agent
    command: agent
    depends_on:
      - drone-server
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - /docker_volumes/drone_agent/drone.key:/drone.key
    networks:
      - drone-infra
    deploy:
      placement:
        constraints:
          - node.labels.location == cloud-arm64

networks:
  traefik-net:
    external: true
  drone-infra:
    external: true
```
J'ai du changer également les variables d'environnement

cat .env.production-server
```env
DRONE_SERVER_HOST=drone.fredix.xyz
DRONE_SERVER_PROTO=https
DRONE_GITLAB_CLIENT_ID=TOKEN
DRONE_GITLAB_CLIENT_SECRET=TOKEN
DRONE_GITLAB_SERVER=https://framagit.org
DRONE_RPC_SERVER=https://drone.fredix.xyz
DRONE_RPC_SECRET=SECRET
DRONE_LOGS_DEBUG=true
DRONE_LOGS_PRETTY=false
DRONE_USER_CREATE=username:fredix,admin:true
DRONE_AGENTS_ENABLED=true
```

cat .env.production-agent
```env
DRONE_TIMEOUT=10m
DRONE_MAX_PROCS=3
DRONE_RPC_SERVER=https://drone.fredix.xyz
DRONE_RPC_SECRET=SECRET
DOCKER_OS=linux
DOCKER_ARCH=arm64
DRONE_PLATFORM=linux/arm64
DRONE_DEBUG=true
DRONE_LOGS_DEBUG=true
DRONE_LOGS_PRETTY=false
DRONE_RUNNER_ARCH=arm64
```

Cette version corrige enfin le bug de communication entre le serveur et ses agents. Pour ceux ne souhaitant pas s'héberger, Drone propose une [offre gratuite en cloud](https://blog.drone.io/drone-cloud/) liée à github uniquement.
