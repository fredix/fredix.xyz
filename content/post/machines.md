---
title: "Machines"
date: 2024-01-21T15:38:55+01:00
draft: false
tags: ["linux","app"]
---

[Machines](https://apps.gnome.org/fr/Boxes/) est une application GNOME qui permet de créer des machines virtuelles, elle est aussi connu sous le nom de Boxes.  
Très basique elle met en place un réseau NATé pour permettre à la VM de sortir sur Internet, cependant il n'est pas possible de s'y connecter depuis le host, en ssh per exemple.  
Voici la méthode que j'ai utilisé sur Manjaro pour mettre en place une interface bridge et l'utiliser dans les VMs de Machines, cela doit être plus ou moins pareil avec d'autres distributions.

* Tout d'abord il faut installer le paquet virt-manager  
`sudo pacman -S virt-manager`
* Ensuite lancer le service libvirtd  
`sudo systemctl start libvirtd`
* Puis lancer virt-manager et éditer la connexion QEMU/KVM (clic droit puis détails)
![](/images/virt-manager1.png)

* cocher Démarrage automatique [] au démarrage
* cliquer sur le bouton play en bas pour activer l'interface

![](/images/virt-manager2.png)

* Quitter virt-manager puis vérifier qu'il y a bien dans ce fichier `/etc/qemu/bridge.conf` :  
`allow virbr0`

* Ensuite dans un terminal il faut lister les VMs et éditer pour chacune leur fichier de configuration avec virsh

```sh
virsh -c qemu:///session list --all
 ID   Nom               État
----------------------------------------------
 3    boxes-unknown-2   en cours d’exécution
 -    archlinux         fermé
 -    boxes-unknown     fermé
 -    haikur1beta4      fermé
 -    ubuntu23.10       fermé
 ```

`virsh edit ubuntu23.10`

chercher avec l'éditeur de texte `interface` et remplacer `user` par `bridge` puis ajouter `<source bridge='virbr0'/>`  
Exemple :

```xml
    <interface type='bridge'>
      <mac address='52:54:00:59:d2:52'/>
      <source bridge='virbr0'/>
      <model type='virtio'/>
      <address type='pci' domain='0x0000' bus='0x01' slot='0x00' function='0x0'/>
    </interface>
```

* Sortir de l'éditeur puis lancer la VM modifiée dans Machines, elle devrait avoir maintenant une IP de votre réseau local. Si cela fonctionne pas vérifier qu'il y a bien une interface virbr0 dans un terminal sur le host et si non tenter un reboot  
```sh
ip a|grep -i virbr0
64: virbr0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    inet 192.168.122.1/24 brd 192.168.122.255 scope global virbr0
66: tap0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel master virbr0 state UNKNOWN group default qlen 1000
```

(Ce texte a été écrit avec [Ghostwriter](https://ghostwriter.kde.org/fr/))
