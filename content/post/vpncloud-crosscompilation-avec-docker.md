---
tags: ["vpncloud","docker","rust"]
categories: ["devops"]
title: "vpncloud cross-compilation avec Docker"
slug: "vpncloud-crosscompilation-avec-docker"
date: 2019-08-12T00:50:04+02:00
draft: false
---

J'utilise le logiciel [vpncloud](https://vpncloud.ddswd.de/) pour connecter des serveurs ARM64 avec un raspberry pi 3. Il s'avère que sa compilation sur un raspberry 3 (ou 2) en plus d'être longue peut planter car elle prend trop de mémoire et le kernel fini par tuer la tâche.  
L'idéal est d'utiliser son PC de bureau car Rust peut être compilé en crossplateforme. Il faut néanmoins installer divers paquets comme le compilateur gcc de la cible (armv7, arm64,..) et [rustup](https://rustup.rs/) ce qui peut être pénible selon sa distribution et sa version. Docker peut alors faciliter cette tâche, d'autant plus que Mozilla fournie une image [Docker Rust](https://hub.docker.com/_/rust).  
Voici quelques scripts pour générer un binaire pour 3 architectures.

## ARM32v7 (raspberry pi 2 / 3)

```sh
git clone https://github.com/dswd/vpncloud.git
cd vpncloud

docker run --rm --user "$(id -u)":"$(id -g)" -v "$PWD":/usr/src/vpncloud 
-w /usr/src/vpncloud -u root -it rust:1.36 /bin/bash -c "apt-get update && 
apt-get install -qq gcc-arm-linux-gnueabihf && 
rustup target add armv7-unknown-linux-gnueabihf && 
cargo build --release --target=armv7-unknown-linux-gnueabihf"
```
Le binaire généré se trouve dans `target/armv7-unknown-linux-gnueabihf/release/vpncloud`

## ARM64v8 (raspberry pi 3 / 4)

(le raspberry3 peut etre installé en Linux 64 bit avec par exemple [archlinuxarm](https://archlinuxarm.org/platforms/armv8/broadcom/raspberry-pi-3) : *AArch64 Installation* )

```sh
git clone https://github.com/dswd/vpncloud.git
cd vpncloud

docker run --rm --user "$(id -u)":"$(id -g)" -v "$PWD":/usr/src/vpncloud 
-w /usr/src/vpncloud -u root -it rust:1.36 /bin/bash -c 'apt-get update && 
apt-get install -qq gcc-aarch64-linux-gnu && 
echo "[target.aarch64-unknown-linux-gnu]" >> .cargo/config && 
echo "linker = \"aarch64-linux-gnu-gcc\"" >> .cargo/config && 
rustup target add aarch64-unknown-linux-gnu && 
cargo build --release --target=aarch64-unknown-linux-gnu'
```
ls `target/aarch64-unknown-linux-gnu/release/vpncloud`


## X86_64

```sh
git clone https://github.com/dswd/vpncloud.git
cd vpncloud

docker run --rm --user "$(id -u)":"$(id -g)" -v "$PWD":/usr/src/vpncloud 
-w /usr/src/vpncloud -u root -it rust:1.36 /bin/bash -c 
'echo "[target.x86_64-unknown-linux-gnu]" >> .cargo/config && 
echo "linker = \"x86_64-linux-gnu-gcc\"" >> .cargo/config && 
rustup target add x86_64-unknown-linux-gnu && 
cargo build --release --target=x86_64-unknown-linux-gnu'
```
ls `target/x86_64-unknown-linux-gnu/release/vpncloud`

